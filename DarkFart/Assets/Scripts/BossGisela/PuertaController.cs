using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaController : MonoBehaviour
{
    [SerializeField]
    private Llave.TipoLlave tipoLlaveNecesario;
    // Start is called before the first frame update
    public void AbrirPuerta(Llave.TipoLlave tipo)
    {
        if (tipo == tipoLlaveNecesario)
        {
            Debug.Log("Abielto");
            gameObject.SetActive(false);
        }
        
    }
}

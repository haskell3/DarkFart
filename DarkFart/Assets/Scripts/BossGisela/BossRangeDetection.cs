using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRangeDetection : MonoBehaviour
{
    [SerializeField]
    private Sack boss;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 7)
        {
            //Debug.Log("ola player");
            boss.PlayerInRange = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 7)
        {
            //Debug.Log("adios player");
            boss.PlayerInRange = false;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilSack : MonoBehaviour
{
    public void Return()
    {
        GetComponent<Rigidbody2D>().velocity = -GetComponent<Rigidbody2D>().velocity;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterBossBattle : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Door;
    [SerializeField]
    private GameObject m_Boss;
    [SerializeField]
    private AudioSource m_Music;
    [SerializeField]
    private AudioClip m_BossMusic;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_Music.clip = m_BossMusic;
        m_Music.Play();
        m_Door.SetActive(true);
        m_Boss.SetActive(true);
        gameObject.SetActive(false);
    }


}

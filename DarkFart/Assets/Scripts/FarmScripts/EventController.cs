using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventController : MonoBehaviour
{
    [SerializeField] private Enemy[] enemies;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private float lapse = 1;
    [SerializeField] private float probability = 0.01f;
    [SerializeField] private float multiplicadorCantidadEnemigos = 0.001f;
    [SerializeField] private bool isAble = true;
    [SerializeField] private int diasParaMejorar = 50;
    [SerializeField] private Vector3[] spawns;

    private void Start()
    {
        StartCoroutine(EventControllers());
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Insert))
        {
            Event();
        }
    }
    private IEnumerator EventControllers()
    {
        while (true)
        {
            yield return new WaitForSeconds(lapse);
            if (Random.Range(0f, 101f) < probability * TimeManager.Instance.Day)
                Event();
                
        }
    }

    private void Event()
    {
        if (!isAble) return;
        isAble = false;

        int actualEnemie = TimeManager.Instance.Day / diasParaMejorar;
        if (actualEnemie >= enemies.Length)
            actualEnemie = enemies.Length - 1;

        int cantidadEnemigos = (int) (TimeManager.Instance.Day * Random.Range(1, 100) * multiplicadorCantidadEnemigos);
        if (cantidadEnemigos <= 0)
            cantidadEnemigos = 1;

        for (int i = 0; i < cantidadEnemigos; i++)
        {
            GameObject g = Instantiate(enemyPrefab);

            g.GetComponent<EnemyController>().EnemySO = enemies[actualEnemie];

            Vector3 pos = spawns[Random.Range(0, spawns.Length)];
            pos.x += Random.Range(-(cantidadEnemigos/2), cantidadEnemigos/2);
            pos.y += Random.Range(-(cantidadEnemigos/2), cantidadEnemigos/2);
            g.transform.position = pos;
        }
    }

    public void Enable()
    {
        isAble = true;
    }
}

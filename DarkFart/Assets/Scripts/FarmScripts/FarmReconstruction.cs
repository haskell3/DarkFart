using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using NavMeshPlus.Components;

[RequireComponent(typeof(FarmSimulation))]
public class FarmReconstruction : MonoBehaviour
{
    private Tilemap t;
    [SerializeField] private GranjaInfo farm;
    [SerializeField] private GameObject m_Animal;
    [SerializeField] private GameObject m_Enemy;
    [SerializeField] private GameObject m_Item;
    [SerializeField] private GameObject m_ArbolRoca;
    [SerializeField] private GameObject m_Plant;
    [SerializeField] private GameObject m_Barrera;
    [SerializeField] private GameObject m_Estructura;
    [SerializeField] private GameObject m_NPC;
    [SerializeField] private NavMeshSurface m_NavMesh;
    [SerializeField] private GameEvent m_WantToSleep;

    [SerializeField] private Estructura[] estructureComponent;
    private FarmSimulation m_Simulation;

    private void Awake()
    {
        m_Simulation = GetComponent<FarmSimulation>();
    }

    private void Start()
    {
        TimeStamp stamp = TimeManager.Instance.TimeStamped;
        t = TilemapController.PlantasTilemap;
        SetTiles();
        BuildNavMesh();
        RecreateGameObjects(stamp);
        BuildNavMesh();
        m_Simulation.Simulate(stamp);
    }

    public void BuildNavMesh()
    {
        m_NavMesh.BuildNavMesh();
    }

    public void SetTiles()
    {
        for (int i = 0; i < farm.tiles.Count(); i++)
        {
            t.SetTile(t.WorldToCell(farm.tiles.GetPos(i)), farm.tiles.GetTile(i));
        }
    }
    public void RecreateEnemies()
    {
        foreach (EnemyInfo info in farm.enemies)
        {
            GameObject g = Instantiate(m_Enemy);

            g.GetComponent<EnemyController>().EnemySO = info.enemy;
            g.GetComponent<EntitieDie>().Drops = info.items;
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;
            g.GetComponent<SpriteFrontBackSideAgent>().Sprites = info.sprites;

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }
    }

    public void RecreateEnemies(EnemyInfo[] enemies)
    {
        foreach (EnemyInfo info in enemies)
        {
            GameObject g = Instantiate(m_Enemy);

            g.GetComponent<EnemyController>().EnemySO = info.enemy;
            g.GetComponent<EntitieDie>().Drops = info.items;
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;
            g.GetComponent<SpriteFrontBackSideAgent>().Sprites = info.sprites;

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }
    }

    public void RecreateGameObjects(TimeStamp stamp)
    {
        foreach (EstructuraInfo info in farm.estructures)
        {
            GameObject g = Instantiate(m_Estructura);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;

            switch (info.typeEstructure)
            {

                case EnumForEstructures.CAMA:
                    CamaBehaviour ca = g.AddComponent<CamaBehaviour>();
                    ca.WantToSleep = m_WantToSleep;
                    ca.Estructura = estructureComponent[1];
                    break;
                case EnumForEstructures.MESACRAFTEO:
                    MesaBehaviour m = g.AddComponent<MesaBehaviour>();
                    m.Estructura = estructureComponent[3];
                    break;
            }

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }
        foreach (BarreraInfo info in farm.barriers)
        {
            GameObject g = Instantiate(m_Barrera);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;

            g.GetComponent<BarreraBehaviour>().Estructura = estructureComponent[4];

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }

        foreach (CalderoInfo info in farm.caldiers)
        {
            GameObject g = Instantiate(m_Estructura);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;


            CalderoBehaviour m = g.AddComponent<CalderoBehaviour>();
            m.Estructura = estructureComponent[0];
            m.setMaxWater(info.maxWater);
            m.setWater(info.water);
            m.Loadwood(info.wood);

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }

        foreach (CofreInfo info in farm.chests)
        {
            GameObject g = Instantiate(m_Estructura);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;


            CofreBehaviour m = g.AddComponent<CofreBehaviour>();
            m.Estructura = estructureComponent[2];
            m.ItemSlots = info.items;


            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }
        BuildNavMesh();
        foreach (ItemInfo info in farm.items)
        {
            GameObject g = Instantiate(m_Item);

            g.GetComponent<ItemInWorld>().SetItem(info.item, info.amount);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }
        foreach (AnimalInfo info in farm.animals)
        {
            GameObject g = Instantiate(m_Animal);

            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;
            g.GetComponent<AnimalController>().animal = info.animal;
            g.GetComponent<EntitieDie>().Drops = info.items;
            g.GetComponent<SpriteFrontBackSideAgent>().Sprites = info.sprites;

            Debug.Log("Antes " + g.transform.position);

            g.transform.position = info.pos;

            Debug.Log("Despues " + g.transform.position);
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }
        RecreateEnemies();
        foreach (DropeableInfo info in farm.resources)
        {
            GameObject g = Instantiate(m_ArbolRoca);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;
            g.GetComponent<EntitieDie>().Drops = new List<Item>(info.items);

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }
        foreach (PlantInfo info in farm.plants)
        {
            GameObject g = Instantiate(m_Plant);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;

            PlantsGrowths p = g.GetComponent<PlantsGrowths>();

            p.IsReady = info.isReady;
            p.IsDead = info.isDead;
            p.CurrentDays = info.currentDays;
            p.Days = info.days;
            p.Sprites = info.sprites;
            p.Item = info.item;

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;

            for(int i = 0; i < stamp.Day; i++)
            {
                p.NewDay();
            }
        }
        foreach (NPCInfo info in farm.NPCS)
        {
            GameObject g = Instantiate(m_NPC);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<IdInfo>().Recreated = true;
            g.GetComponent<SpriteFrontBackSideAgent>().Sprites = info.sprites;

            switch (info.script)
            {

                case NPCInfo.typeOfScript.DefenderGranja:
                    //g.AddComponent<>();
                    break;
                case NPCInfo.typeOfScript.RepararBarrera:
                    g.AddComponent<TascaReparar>();
                    break;
                default:

                    break;
            }

            NPCController npc = g.GetComponent<NPCController>();
            npc.Personality = info.personalidad;
            npc.Friendship = info.nivelAmistad;
            npc.Name = info.name;
            npc.Speed = info.speed;
            npc.IsInGranja = true;
            VidaController vida = g.GetComponent<VidaController>();
            vida.Hp = info.hp;
            vida.MaxHP = info.MaxHP;

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
        }
    }
}

public enum EnumForEstructures { MESACRAFTEO, CAMA }




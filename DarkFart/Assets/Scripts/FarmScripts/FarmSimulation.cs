using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(FarmReconstruction))]
public class FarmSimulation : MonoBehaviour
{
    [SerializeField] private GranjaInfo _granjaInfo;
    [SerializeField] private float probability;
    [SerializeField] private float enemyDamage;
    [SerializeField] private float radiusFarm;
    [SerializeField] private float m_RadiusForBarriers = 4f;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject PrefabAgent;
    [SerializeField] private Vector3 agentPos;
    [SerializeField] private Vector2[] init;
    [SerializeField] private Vector2[] finish;
    [SerializeField] private LayerMask AnimalsPlants;
    [SerializeField] private LayerMask Estructuras;
    private FarmReconstruction _reconstruction;
    private List<Transform> pathsComplete = new List<Transform>();
    private PathVector3 pathPartial;
    private NavMeshAgent _navMeshAgent;
    private int cantidadEnemigos = 0;
    private int cantidadEnemigosAux;
    private bool[][] matrix;
    private int diffX;
    private int diffY;
    private void Awake()
    {
        _reconstruction = GetComponent<FarmReconstruction>();
        PrefabAgent = Instantiate(PrefabAgent);
        PrefabAgent.transform.position = agentPos;
        _navMeshAgent = PrefabAgent.GetComponent<NavMeshAgent>();
    }

    public void Simulate(TimeStamp stamp)
    {
        float time = stamp.Time + 24 * stamp.Day;

        for (int i = 0; i < time; i++)
        {
            if (UnityEngine.Random.Range(0f, 101f) < probability)
            {
                cantidadEnemigos++;
            }

            if (cantidadEnemigos == 0) continue;

            NavMeshPath path = new NavMeshPath();

            Collider2D[] colliders = Physics2D.OverlapCircleAll(PrefabAgent.transform.position, radiusFarm, AnimalsPlants, -1, 1);
            pathsComplete.Clear();
            foreach (Collider2D collider in colliders)
            {
                if (_navMeshAgent.CalculatePath(collider.transform.position, path))
                {
                    if (path.status == NavMeshPathStatus.PathComplete)
                        pathsComplete.Add(collider.transform);
                    else
                        pathPartial = new PathVector3(collider.transform, path);
                }
            }
            cantidadEnemigosAux = cantidadEnemigos;
            DamageThings();
        }
        EnemyInfo[] enemyInfos = new EnemyInfo[cantidadEnemigos];
        for (int i = 0; i < cantidadEnemigos; i++)
        {
            int r = UnityEngine.Random.Range(0, init.Length);
            EnemyInfo e = new EnemyInfo(enemyPrefab.layer, new Vector3(UnityEngine.Random.Range(init[r].x, finish[r].x), UnityEngine.Random.Range(init[r].y, finish[r].y), 0), enemyPrefab.GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Enemy, IdInfo.NewId());
            e.enemy = enemyPrefab.GetComponent<EnemyController>().EnemySO;
            e.items = enemyPrefab.GetComponent<EntitieDie>().Drops;
            e.vida = enemyPrefab.GetComponent<VidaController>().MaxHP;
            enemyInfos[i] = e;
        }
        _reconstruction.RecreateEnemies(enemyInfos);

    }

    private void DamageThings()
    {
        if (pathsComplete.Count > 0)
        {
            if (pathsComplete[0].gameObject.TryGetComponent<PlantsGrowths>(out PlantsGrowths p))
            {
                Destroy(p.gameObject);
                cantidadEnemigosAux--;
                pathsComplete.RemoveAt(0);
                if (cantidadEnemigosAux > 0)
                    DamageThings();
            }
            else if (pathsComplete[0].gameObject.TryGetComponent<VidaController>(out VidaController v))
            {
                if (v.Hp < cantidadEnemigosAux * enemyDamage)
                {
                    while (v.Hp > 0 && cantidadEnemigos > 0)
                    {
                        v.Hp -= enemyDamage;
                        cantidadEnemigosAux--;
                    }
                    pathsComplete.RemoveAt(0);
                    if (cantidadEnemigosAux > 0)
                        DamageThings();
                }
                else
                    v.Hp -= (cantidadEnemigosAux * enemyDamage);
            }
        }
        else if (pathPartial.pos != null)
        {
            Vector3 lastCorner = pathPartial.path.corners[pathPartial.path.corners.Length - 1];
            lastCorner.z = 0;
            Vector3 direction = pathPartial.pos.position - lastCorner;
            Collider2D[] c = Physics2D.OverlapCircleAll(lastCorner, m_RadiusForBarriers, Estructuras);

            Array.Sort(c, new CollidersComparators(lastCorner));

            if (c[0].transform == null)
            {
                Debug.Log("ESTO ES MALOOOO. NO DEBERIA PASAR. (Preguntar Ari)");
                return;
            }

            if (c[0].transform.gameObject.TryGetComponent<VidaController>(out VidaController v))
            {
                if (v.Hp < cantidadEnemigosAux * enemyDamage)
                {
                    while (v.Hp > 0 && cantidadEnemigos > 0)
                    {
                        v.LessHP(enemyDamage);
                        cantidadEnemigosAux--;
                    }
                    _reconstruction.BuildNavMesh();
                    if (cantidadEnemigosAux <= 0) return;
                    Collider2D[] colliders = Physics2D.OverlapCircleAll(PrefabAgent.transform.position, radiusFarm, AnimalsPlants, -1, 1);
                    NavMeshPath path = new NavMeshPath();
                    pathsComplete.Clear();
                    //The line below should be commented if we want the enemies to destroy more barriers than necessary. Useful to the game but not very clever from the enemies.
                    pathPartial = new PathVector3(null, null);
                    foreach (Collider2D collider in colliders)
                    {
                        if (_navMeshAgent.CalculatePath(collider.transform.position, path))
                        {
                            if (path.status == NavMeshPathStatus.PathComplete)
                                pathsComplete.Add(collider.transform);
                            else
                                pathPartial = new PathVector3(collider.transform, path);
                        }
                    }
                    DamageThings();
                }
                else
                    v.LessHP(cantidadEnemigosAux * enemyDamage);
            }
            else
                Debug.Log("No tiene vida? RARO. (Preguntar Ari)");

        }
    }

}

class CollidersComparators : IComparer<Collider2D>
{
    Vector3 t;
    public int Compare(Collider2D x, Collider2D y)
    {
        return (int)(((x.transform.position - t).magnitude) - ((y.transform.position - t).magnitude));
    }
    public CollidersComparators(Vector3 t)
    {
        this.t = t;
    }
}

public struct PathVector3
{
    public Transform pos;
    public NavMeshPath path;

    public PathVector3(Transform pos, NavMeshPath path)
    {
        this.pos = pos;
        this.path = path;
    }
    public string ToString()
    {
        return "Position: " + pos.position + ", Path: " + path.ToString();
    }
}

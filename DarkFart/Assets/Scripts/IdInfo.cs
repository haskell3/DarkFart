using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class IdInfo : MonoBehaviour
{
    private int id = -1;
    public int Id { get { return id; } set { id = value; } }
    private static int cont;
    [SerializeField] private MapInfo map;
    [SerializeField] private GranjaInfo farm;
    private bool recreated = false;
    public bool Recreated { get { return recreated; } set { recreated = value; } }

    private void Awake()
    {
        if (id != -1) return;
        id = cont;
        cont++;
    }

    private bool isId(int idToCompare)
    {
        return id == idToCompare;
    }

    public void SaveInfo()
    {
        if (gameObject.layer == 3)
        {
            EnemyInfo e = new EnemyInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Enemy, GetComponent<IdInfo>().Id);
            e.enemy = GetComponent<EnemyController>().EnemySO;
            e.items = GetComponent<EntitieDie>().Drops;
            e.vida = GetComponent<VidaController>().Hp;
            e.maxVida = GetComponent<VidaController>().MaxHP;
            e.sprites = GetComponent<SpriteFrontBackSideAgent>().Sprites;
            if (SceneController.Instance.ActualScene == SceneController.SCENES.Granja)
                farm.enemies.Add(e);
            else
                map.enemies.Add(e);
        }
        else if (gameObject.layer == 8)
        {
            AnimalInfo e = new AnimalInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Animal, GetComponent<IdInfo>().Id);
            e.animal = GetComponent<AnimalController>().animal;
            e.items = GetComponent<EntitieDie>().Drops;
            e.vida = GetComponent<VidaController>().Hp;
            e.maxVida = GetComponent<VidaController>().MaxHP;
            e.sprites = GetComponent<SpriteFrontBackSideAgent>().Sprites;
            if (SceneController.Instance.ActualScene == SceneController.SCENES.Granja)
                farm.animals.Add(e);
            else
                map.animals.Add(e);
        }
        else if (gameObject.layer == 16 || gameObject.layer == 17)
        {
            DropeableInfo e = new DropeableInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.ArbolRoca, GetComponent<IdInfo>().Id);
            e.items = GetComponent<EntitieDie>().Drops;
            e.vida = GetComponent<VidaController>().Hp;
            e.maxVida = GetComponent<VidaController>().MaxHP;
            if (SceneController.Instance.ActualScene == SceneController.SCENES.Granja)
                farm.resources.Add(e);
            else
                map.resources.Add(e);
        }
        else if (gameObject.layer == 11)
        {
            ItemInfo e = new ItemInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Item, GetComponent<IdInfo>().Id);
            e.item = GetComponent<ItemInWorld>().returnItem();
            e.amount = GetComponent<ItemInWorld>().returnAmount();
            if (SceneController.Instance.ActualScene == SceneController.SCENES.Granja)
                farm.items.Add(e);
            else
                map.items.Add(e);
        }
        else if (gameObject.layer == 13)
        {
            PlantInfo plant = new PlantInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Item, GetComponent<IdInfo>().Id);
            PlantsGrowths p = GetComponent<PlantsGrowths>();
            plant.isReady = p.IsReady;
            plant.isDead = p.IsDead;
            plant.currentDays = p.CurrentDays;
            plant.days = p.Days;
            plant.sprites = p.Sprites;
            plant.item = p.Item;
            farm.plants.Add(plant);
        }
        else if (gameObject.layer == 19)
        {
            NPCInfo npc = new NPCInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Item, GetComponent<IdInfo>().Id);
            NPCController info = GetComponent<NPCController>();
            VidaController vida = GetComponent<VidaController>();

            npc.nivelAmistad = info.Friendship;
            npc.personalidad = info.Personality;
            npc.name = info.Name;
            npc.speed = info.Speed;
            npc.hp = vida.Hp;
            npc.MaxHP = vida.MaxHP;
            npc.sprites = GetComponent<SpriteFrontBackSideAgent>().Sprites;

            if(TryGetComponent<TascaReparar>(out TascaReparar t))
            {
                npc.script = NPCInfo.typeOfScript.RepararBarrera;
            }
            else
            {
                npc.script = NPCInfo.typeOfScript.None;
            }
            if(SceneController.Instance.ActualScene == SceneController.SCENES.Granja || GetComponent<NPCController>().IsInGranja)
                farm.NPCS.Add(npc);

        }
        else if (gameObject.layer == 12)
        {
            if(gameObject.TryGetComponent<BarreraBehaviour>(out BarreraBehaviour b))
            {
                BarreraInfo e = new BarreraInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Item, GetComponent<IdInfo>().Id);
                e.estructura = b.Estructura;
                e.vida = GetComponent<VidaController>().Hp;
                e.maxVida = GetComponent<VidaController>().MaxHP;
                farm.barriers.Add(e);
            }
            else if (gameObject.TryGetComponent<CofreBehaviour>(out CofreBehaviour co))
            {
                CofreInfo e = new CofreInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Item, GetComponent<IdInfo>().Id);
                e.estructura = co.Estructura;
                e.items = co.ItemSlots;
                farm.chests.Add(e);
            }
            else if (gameObject.TryGetComponent<CalderoBehaviour>(out CalderoBehaviour c))
            {
                CalderoInfo e = new CalderoInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Item, GetComponent<IdInfo>().Id);
                e.estructura = c.Estructura;
                e.water = c.getWater();
                e.maxWater = c.getMaxWater();
                e.wood = c.getWood();
                farm.caldiers.Add(e);
            }
            else
            {
                EstructuraInfo e = new EstructuraInfo(gameObject.layer, transform.position, GetComponent<SpriteRenderer>().sprite, GameObjectsInfo.typeOfGameObject.Item, GetComponent<IdInfo>().Id);
                e.estructura = GetComponent<EstructuraBehaviour>().Estructura;
                if (gameObject.TryGetComponent<CamaBehaviour>(out CamaBehaviour ca))
                    e.typeEstructure = EnumForEstructures.CAMA;
                else if (gameObject.TryGetComponent<MesaBehaviour>(out MesaBehaviour m))
                    e.typeEstructure = EnumForEstructures.MESACRAFTEO;

                farm.estructures.Add(e);
            }
        }
    }

    public static int NewId()
    {
        int aux = cont;
        cont++;
        return aux;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractNPC : MonoBehaviour,Interactuable
{
    [SerializeField]
    private NPCController controller;
    [SerializeField]
    private PersonalidadInfo personalidad;
    public PersonalidadInfo Personalidad { set { personalidad = value; } }
    [SerializeField] private bool canTalk = true;

    private void Awake()
    {
        controller = GetComponent<NPCController>();
    }
    public void Interact()
    {
        if (!canTalk) return;
        if (!controller.IsInGranja)
        {
            DialogueManager.Instance.EnterDialogueMode(personalidad.RecruitmentDialogue.dialogueTextAsset, controller);
        } else
        {
            for (int i = 0; i < personalidad.Levels.Count; i++)
            {
                if (controller.Friendship <= personalidad.Levels[i].maxFriendship)
                {
                    int r = Random.Range(0, personalidad.Levels[i].dialogues.Count);
                    DialogueManager.Instance.EnterDialogueMode(personalidad.Levels[i].dialogues[r].dialogueTextAsset, controller);
                    break;
                }
            }
            canTalk = false;
        }
        
    }

    public void ResetTalk()
    {
        canTalk = true;
    }
}

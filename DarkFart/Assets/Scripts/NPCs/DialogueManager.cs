using Ink.Runtime;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    [Header("Dialogue UI")]
    [SerializeField] private GameObject dialoguePanel;
    [SerializeField] private TextMeshProUGUI dialogueText;

    [Header("Choices UI")]
    [SerializeField] private GameObject[] choices;
    [SerializeField] private GameObject continueButton;
    private TextMeshProUGUI[] choicesText;

    private Story currentStory;
    private NPCController controller;
    private string name;
    private float finaleFriendship=0;
    private bool tpToGranja = false;
    private GameObject player;
    [SerializeField]
    private bool dialogueIsPlaying;

    public static DialogueManager Instance { get; internal set; }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {
        player = FindObjectOfType<MovimentPlayer>().gameObject;
        dialogueIsPlaying = false;
        dialoguePanel.SetActive(false);

        //get all of the choices text
        choicesText = new TextMeshProUGUI[choices.Length];
        int index = 0;
        foreach (GameObject choice in choices)
        {
            choicesText[index] = choice.GetComponentInChildren<TextMeshProUGUI>();
            index++;
        }

    }
    private void Update()
    {
        if (!dialogueIsPlaying)
        {
            return;
        }
        /*
        if (Input.GetKeyDown(KeyCode.E))
        {
            EnterDialogueMode(dialogueTextAsset);
        }*/
 
        /*
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ContinueStory();
        }*/
    }

    public void EnterDialogueMode(TextAsset inkJSON, NPCController c)
    {
        player.GetComponent<InputController>().DisableMoveAndInventory();
        player.GetComponent<InputController>().DisableHand();
        Time.timeScale = 0;
        currentStory = new Story(inkJSON.text);
        dialogueIsPlaying = true;
        dialoguePanel.SetActive(true);
        GameController.Instance.StartDialogue(c);
        controller = c;
        name = controller.Name;
        ContinueStory();
    }
    public void EnterDialogueMode(TextAsset inkJSON, string n)
    {
        player.GetComponent<InputController>().DisableMoveAndInventory();
        player.GetComponent<InputController>().DisableHand();
        Time.timeScale = 0;
        currentStory = new Story(inkJSON.text);
        dialogueIsPlaying = true;
        dialoguePanel.SetActive(true);
        name = n;
        ContinueStory();
    }

    private void ExitDialogueMode()
    {
        player.GetComponent<InputController>().EnableMoveAndInventory();
        player.GetComponent<InputController>().EnableHand();
        Time.timeScale = 1;
        dialogueIsPlaying = false;
        dialoguePanel.SetActive(false);
        if (controller != null) GameController.Instance.StopDialogue(controller);
        dialogueText.text = "";
        if (controller != null) controller.AddFriendship(finaleFriendship);
        finaleFriendship = 0;
        
        if (tpToGranja)
        {
            controller.TpToGranja();
        }
        controller = null;
        tpToGranja = false;
    }

    public void ContinueStory()
    {
        if (currentStory.canContinue)
        {
            dialogueText.text = name + ": "+currentStory.Continue();
            DisplayChoices();
        }
        else
        {
            ExitDialogueMode();
        }
    }

    private void DisplayChoices()
    {
        List<Choice> curretChoices = currentStory.currentChoices;
        if (curretChoices.Count > 0)
        {
            continueButton.SetActive(false);
        } else
        {
            continueButton.SetActive(true);
        }

            //Mirar si la UI puede suportear tantas opciones 
            //TODO: Que esto nunca pase
            if (curretChoices.Count > choices.Length)
        {
            Debug.LogError("Se han dado m�s opciones de las que la UI puede displayear. Numero de opciones dadas: " + curretChoices.Count + " Numero de opciones maximas: " + curretChoices.Count);
        }

        int index = 0;
        //enable las choices e inicializarlas a la cantidad de opciones para esta linea de dialogo
        foreach (Choice choice in curretChoices)
        {
            choices[index].gameObject.SetActive(true);
            choicesText[index].text = choice.text;
            index++;
        }
        // checkiar si el resto de choices suporteadas estan hidden
        for (int i = index; i < choices.Length; i++)
        {
            choices[i].gameObject.SetActive(false);
        }

        
    }

    public void MakeChoice(int choiceIndex)
    {
        currentStory.ChooseChoiceIndex(choiceIndex);
        ContinueStory();
        //Debug.Log((int) currentStory.variablesState["friendship"]);
        finaleFriendship = (int)currentStory.variablesState["friendship"];
        tpToGranja = (bool)currentStory.variablesState["tpToGranja"];
    }
}

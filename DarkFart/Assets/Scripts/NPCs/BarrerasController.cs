using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrerasController : MonoBehaviour, IComparer<VidaController>
{
    public static BarrerasController Instance { get; internal set; }
    [SerializeField]
    private List<VidaController> barreras = new List<VidaController>();
    public List<VidaController> Barreras {
        get 
        {
            barreras.Sort(this);
            return barreras; } 
        set { barreras = value; }
    }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public int Compare(VidaController x, VidaController y)
    {
        return (int) (x.Hp - y.Hp);
    }
}

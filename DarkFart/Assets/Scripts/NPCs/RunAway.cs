using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem.XR;

public class RunAway : MonoBehaviour
{
    [SerializeField] private NavMeshAgent m_NavMeshAgent;
    private float displacementDist = 5f;
    [SerializeField] private Transform m_EnemyTransform;
    [SerializeField] private NPCController m_NPCController;
    private IEnumerator RunAwayFromEnemy(Transform destination)
    {
        while (true)
        {
            Vector3 normDir = (m_EnemyTransform.position - transform.position).normalized;
            m_NavMeshAgent.SetDestination(transform.position - (normDir * displacementDist));

            yield return new WaitForSeconds(0.5f);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 3)
        {
            m_NPCController.StopWalkingRandomly();
            m_EnemyTransform = collision.transform;
            StartCoroutine(RunAwayFromEnemy(m_EnemyTransform));
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 3)
        {
            m_NPCController.ChangeToRandomWalk();
            StopAllCoroutines();
        }
    }

}

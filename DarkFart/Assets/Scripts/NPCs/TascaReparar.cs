using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;

public class TascaReparar : MonoBehaviour
{
    private NavMeshAgent m_NavMeshAgent;
    private VidaController m_BarrerasVida;
    private Transform m_BarrerasTransform;
    private NPCController m_NPCController;
    [SerializeField] private float waitBuscar = 2;
    [SerializeField] private float repararTime = 1;
    [SerializeField] private float repararBonus = 10;
    private void Start()
    {
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_NPCController = GetComponent<NPCController>();
        if (m_NPCController.IsInGranja) StartCoroutine(Buscar());
    }

    IEnumerator Buscar()
    {
        while (true)
        {
            List<VidaController> barreras = BarrerasController.Instance.Barreras;
            List<Vector3> posibles = new List<Vector3>();
            if (barreras.Count > 0)
            {
                bool paralo = false;
                for (int b = 0; b < barreras.Count && barreras[b].Hp < barreras[b].MaxHP; b++)
                {
                    Debug.Log("barreras sin vida");
                    m_BarrerasTransform = barreras[b].transform;
                    m_BarrerasVida = barreras[b];
                    Vector3 posarriba = new Vector3(m_BarrerasTransform.position.x, m_BarrerasTransform.position.y - 1f, m_BarrerasTransform.position.z);
                    Vector3 posabajo = new Vector3(m_BarrerasTransform.position.x, m_BarrerasTransform.position.y + 1f, m_BarrerasTransform.position.z);
                    Vector3 posderecha = new Vector3(m_BarrerasTransform.position.x + 1f, m_BarrerasTransform.position.y, m_BarrerasTransform.position.z);
                    Vector3 posizquierda = new Vector3(m_BarrerasTransform.position.x - 1f, m_BarrerasTransform.position.y, m_BarrerasTransform.position.z);

                    posibles.Add(posarriba);
                    posibles.Add(posabajo);
                    posibles.Add(posderecha);
                    posibles.Add(posizquierda);

                    List<PathPos> posiblesPaths = new List<PathPos>();

                    for (int i = 0; i < posibles.Count; ++i)
                    {
                        NavMeshPath path = new NavMeshPath();
                        if (m_NavMeshAgent.CalculatePath(posibles[i], path))
                        {
                            m_NavMeshAgent.SetDestination(posibles[i]);
                            if (path.status == NavMeshPathStatus.PathComplete)
                            {
                                PathPos pathpos = new PathPos(path, posibles[i]);
                                posiblesPaths.Add(pathpos);
                            }
                        }                        
                    }

                    if (posiblesPaths.Count > 0)
                    {
                        Vector3 finalPath = posiblesPaths[0].position;
                        float minDistance = CalculatePathLength(posiblesPaths[0].path);
                        for (int i = 1; i < posiblesPaths.Count; ++i)
                        {
                            float dist = CalculatePathLength(posiblesPaths[i].path);
                            if (dist < minDistance)
                            {
                                minDistance = dist;
                                finalPath = posiblesPaths[i].position;
                            }
                        }
                        m_NPCController.StopWalkingRandomly();
                        Debug.Log(m_BarrerasTransform.position);
                        Debug.Log(finalPath);
                        m_NavMeshAgent.SetDestination(finalPath);
                        StartCoroutine(CaminarHastaBarrera());
                        paralo = true;
                        break;
                    } else
                    {
                        m_NavMeshAgent.SetDestination(transform.position);
                    }


                }
                if (paralo) break;
            }


            yield return new WaitForSeconds(waitBuscar);
        }
    }
    IEnumerator CaminarHastaBarrera()
    {
        while (true)
        {
            Debug.Log((m_NavMeshAgent.destination - transform.position).magnitude);
            if ((m_NavMeshAgent.destination - transform.position).magnitude <= 0.6f)
            {
                StartCoroutine(Reparar());
                break;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
    IEnumerator Reparar()
    {
        while (m_BarrerasVida.Hp < m_BarrerasVida.MaxHP)
        {
            m_BarrerasVida.Hp += repararBonus;
            yield return new WaitForSeconds(repararTime);
        }
        m_BarrerasVida.Hp = m_BarrerasVida.MaxHP;
        m_NPCController.ChangeToRandomWalk();
        StartCoroutine(Buscar());
    }
    private float CalculatePathLength(NavMeshPath path)
    {
        float length = 0f;
        if (path != null)
        {
            Vector3[] corners = path.corners;
            for (int i = 1; i < corners.Length; i++)
            {
                length += Vector3.Distance(corners[i - 1], corners[i]);
            }
        }
        return length;
    }

    private class PathPos
    {
        public NavMeshPath path;
        public Vector3 position;

        public PathPos(NavMeshPath path, Vector3 position)
        {
            this.path = path;
            this.position = position;
        }
    }

}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeManager : MonoBehaviour
{
    [Header("Variables")]
    [SerializeField]
    private float increment;
    public float Increment
    {
        get { return increment; }
    }
    [SerializeField]
    private float secsIncrement;
    public float SecsIncrement
    {
        get { return secsIncrement; }
    }
    [SerializeField]
    private int day = 0;
    public int Day { get { return day; } }

    private bool isDay = false;
    [SerializeField]
    private float time;

    [Header("GameEvents")]
    [SerializeField]
    private GameEventFloat ChangeTime;
    [SerializeField]
    private GameEvent ChangeToDay;
    [SerializeField]
    private GameEvent ChangeToNight;
    [SerializeField]
    private GameEvent AddDay;
    [SerializeField]
    private GameObject m_GUI;
    private static GameObject GUI;
    private Coroutine m_timeChangeCoroutine;
    [SerializeField]
    private TimeStamp timeStamp;
    public TimeStamp TimeStamped 
    { 
        get 
        {
            if (timeStamp == null) return new TimeStamp(0, 0);
            float timeS;
            int dayS;
            if (timeStamp.Time > time)
            {
                timeS = (24 - timeStamp.Time) + time;
            }
            else
            {
                timeS = time - timeStamp.Time;
            }
            dayS = day - timeStamp.Day;
            return new TimeStamp(dayS, timeS); 
        } 
    }
    [SerializeField] private MapInfo m_MapInfo;
    [SerializeField] private GranjaInfo m_GranjaInfo;
    public static TimeManager Instance { get; internal set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void Reset()
    {
        day = 0;
        time = 0;
    }

    IEnumerator TimeChange()
    {
        while (true)
        {

            time += increment;
            CheckTime();

            yield return new WaitForSeconds(secsIncrement);
        }
    }

    private void CheckTime()
    {
        while (time >= 24)
        {
            time -= 24;
            day++;
            GUI.GetComponent<TextMeshProUGUI>().text = "Dia: " + Day.ToString();
            AddDay.Raise();
        }

        if (time >= 7 && !isDay)
        {
            ChangeToDay.Raise();
            isDay = true;
        }
        if (time >= 21 && isDay)
        {
            ChangeToNight.Raise();
            isDay = false;
        }
        ChangeTime.Raise(time);
    }

    public void PlayerDied()
    {
        day += 3;
        GUI.GetComponent<TextMeshProUGUI>().text = "Dia: " + Day.ToString();
        if(SceneController.Instance.ActualScene == SceneController.SCENES.Map)
            m_MapInfo.Save();
        else if(SceneController.Instance.ActualScene == SceneController.SCENES.Granja)
            m_GranjaInfo.Save();
        SceneController.Instance.ChangeScene(SceneController.SCENES.Muerte);
    }

    public void SleepXHours(int hours)
    {
        time += hours;
        CheckTime();
    }

    private void SearchGUI()
    {
        GUI = FindObjectOfType<TagGUIDay>()?.gameObject;
        if (GUI == null) return;
        m_GUI = GUI;
        GUI.GetComponent<TextMeshProUGUI>().text = "Dia: " + Day.ToString();
        CheckTime();
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SearchGUI();
    }

    public void startAll()
    {
        if (m_timeChangeCoroutine == null)
            m_timeChangeCoroutine = StartCoroutine(TimeChange());
    }

    public void stopCoroutine()
    {
        if (m_timeChangeCoroutine != null)
            StopCoroutine(m_timeChangeCoroutine);
        m_timeChangeCoroutine = null;
    }

    public void TimeStamp()
    {
        timeStamp = new TimeStamp(day, time);
    }
}

[Serializable]
public class TimeStamp
{
    [SerializeField] private int day;
    public int Day { get { return day; } }
    [SerializeField] private float time;
    public float Time { get { return time; } }

    public TimeStamp(int day, float time)
    {
        this.day = day;
        this.time = time;
    }
}

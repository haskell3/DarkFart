using TMPro;
using UnityEngine;

public class ChangeTime : MonoBehaviour
{
    private float actualtime = 0;

    private void Start()
    {
        int hor = ((int)(actualtime / 3600));
        int min = ((int)((actualtime - hor * 3600) / 60));
        string hora = "0";
        string minuto = "0";
        if (hor.ToString().Length == 1)
            hora = "0" + hor.ToString();
        else
            hora = hor.ToString();

        if (min.ToString().Length == 1)
            minuto = "0" + min.ToString();
        else
            minuto = min.ToString();

        gameObject.GetComponent<TextMeshProUGUI>().text = "Hora: " + hora + ":" + minuto;
    }

    public void UpdateTime(float time)
    {
        actualtime = toSeconds(time);
        int hor = ((int)(actualtime / 3600));
        int min = ((int)((actualtime - hor * 3600) / 60));
        string hora = "0";
        string minuto = "0";
        if (hor.ToString().Length == 1)
            hora = "0" + hor.ToString();
        else
            hora = hor.ToString();

        if (min.ToString().Length == 1)
            minuto = "0" + min.ToString();
        else
            minuto = min.ToString();

        gameObject.GetComponent<TextMeshProUGUI>().text = "Hora: " + hora + ":" + minuto;
    }

    private float toSeconds(float time)
    {
        return time *= 3600;
    }

}

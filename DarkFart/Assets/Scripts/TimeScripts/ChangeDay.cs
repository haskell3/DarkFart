using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangeDay : MonoBehaviour
{
    void Start()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = "Day: "+TimeManager.Instance.Day.ToString();
    }

    public void AddDay()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = "Day: " + TimeManager.Instance.Day.ToString();
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangeDayNight : MonoBehaviour
{
    private string dayTime = "Noche";
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = dayTime.ToString();
    }

    public void ChangeToDay()
    {
        dayTime = "D�a";
        gameObject.GetComponent<TextMeshProUGUI>().text = dayTime.ToString();
    }

    public void ChangeToNight()
    {
        dayTime = "Noche";
        gameObject.GetComponent<TextMeshProUGUI>().text = dayTime.ToString();
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(VidaController))]
[RequireComponent(typeof(GameEventFloatListener))]
public class EnergiaController : MonoBehaviour
{
    [SerializeField] private float energy;
    public float Energy { get { return energy; } set { energy = value; } }
    [SerializeField] private float Maxenergy;
    [SerializeField] private EnergiaBarra GUI;

    private void Awake()
    {
        energy = Maxenergy;
    }

    public void RecoverEnergy(float e)
    {
        energy += e;
        if(energy > Maxenergy) 
            energy = Maxenergy;
        if(energy > 0) 
            gameObject.GetComponent<VidaController>().WithEnergy(); 
        RefreshGUI();
    }

    public void RecoverFromSleep(int hours)
    {
        if(hours > 24) hours = 24;

        float par = Maxenergy / 24;

        energy += (par * hours);
        if (energy > Maxenergy)
            energy = Maxenergy;
        if (energy > 0)
            gameObject.GetComponent<VidaController>().WithEnergy();
        RefreshGUI();
    }

    public float getEnergia()
    {
        return energy;
    }

    public float getMaxEnergia()
    {
        return Maxenergy;
    }

    public void SetMaxEnergy(float maxEnergy)
    {
        Maxenergy = maxEnergy;
    }

    public void WasteEnergy(float e)
    {
        energy -= e;
        if (energy <= 0)
        {
            energy = 0;
            if(!gameObject.GetComponent<VidaController>().withoutEnergy)
                gameObject.GetComponent<VidaController>().WithoutEnergy();
        }
        RefreshGUI();
    }
    private void RefreshGUI()
    {
        if (GUI != null)
            GUI.mostrar();
    }

    public void startAll()
    {
        GUI = FindFirstObjectByType<EnergiaBarra>();
    }

    public void resetEnergia()
    {
        energy = Maxenergy;
        gameObject.GetComponent<VidaController>().WithEnergy();
        GUI.mostrar();
    }
}

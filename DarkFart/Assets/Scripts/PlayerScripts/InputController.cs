using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

[RequireComponent(typeof(MovimentPlayer))]
[RequireComponent(typeof(InteractPlayer))]
[RequireComponent(typeof(InventarioController))]
[RequireComponent(typeof(PlayerHachaPicoArma))]
[RequireComponent(typeof(TakeItems))]
public class InputController : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    public InputAction MovementAction { get { return m_MovementAction; } }
    private InputAction m_Delta;
    public InputAction Delta { get { return m_Delta; } }
    private MovimentPlayer m_MovimentPlayer;
    private InventarioController m_InventoryController;
    private InteractPlayer m_InteractPlayer;
    private PlayerHachaPicoArma m_PlayerHachaPicoArma;
    private TakeItems m_TakeItems;
    [SerializeField]
    private FishingMinigame m_FishingMinigame;
    [SerializeField]
    private MenuGameController m_Menu;
    private List<InputAction> inventoryActions;


    private void Awake()
    {
        m_MovimentPlayer = GetComponent<MovimentPlayer>();
        m_InteractPlayer = GetComponent<InteractPlayer>();
        m_InventoryController = GetComponent<InventarioController>();
        m_PlayerHachaPicoArma = GetComponent<PlayerHachaPicoArma>();
        m_TakeItems = GetComponent<TakeItems>();
        m_Menu.setController(this);
        Assert.IsNotNull(m_InputAsset);
        m_Input = Instantiate(m_InputAsset);
        m_MovementAction = m_Input.FindActionMap("Default").FindAction("Movement");
        m_Input.FindActionMap("Default").FindAction("Sprint").started += m_MovimentPlayer.ChangeStateSprint;
        m_Input.FindActionMap("Default").FindAction("Sprint").canceled += m_MovimentPlayer.ChangeStateWalk;
        m_Input.FindActionMap("Default").FindAction("Roll").performed += m_MovimentPlayer.ChangeStateRoll;
        m_Input.FindActionMap("Default").FindAction("Interact").performed += m_InteractPlayer.Interact;
        m_Input.FindActionMap("Default").FindAction("Hand").performed += m_InteractPlayer.Hand;
        m_Input.FindActionMap("Default").FindAction("HandSecondary").performed += m_InteractPlayer.HandSecond;
        inventoryActions = new List<InputAction>
        {
             m_Input.FindActionMap("Default").FindAction("Inventory1"),
             m_Input.FindActionMap("Default").FindAction("Inventory2"),
             m_Input.FindActionMap("Default").FindAction("Inventory3"),
             m_Input.FindActionMap("Default").FindAction("Inventory4"),
             m_Input.FindActionMap("Default").FindAction("Inventory5"),
             m_Input.FindActionMap("Default").FindAction("Inventory6"),
             m_Input.FindActionMap("Default").FindAction("Inventory7"),
             m_Input.FindActionMap("Default").FindAction("Inventory8"),
             m_Input.FindActionMap("Default").FindAction("Inventory9"),
             m_Input.FindActionMap("Default").FindAction("Inventory"),
             m_Input.FindActionMap("Default").FindAction("QuickChange"),
             m_Input.FindActionMap("Default").FindAction("ChangeHandSecondary")
        };
        m_Input.FindActionMap("Default").FindAction("Inventory1").performed += m_InventoryController.ChangeToOne;
        m_Input.FindActionMap("Default").FindAction("Inventory2").performed += m_InventoryController.ChangeToTwo;
        m_Input.FindActionMap("Default").FindAction("Inventory3").performed += m_InventoryController.ChangeToThree;
        m_Input.FindActionMap("Default").FindAction("Inventory4").performed += m_InventoryController.ChangeToFour;
        m_Input.FindActionMap("Default").FindAction("Inventory5").performed += m_InventoryController.ChangeToFive;
        m_Input.FindActionMap("Default").FindAction("Inventory6").performed += m_InventoryController.ChangeToSix;
        m_Input.FindActionMap("Default").FindAction("Inventory7").performed += m_InventoryController.ChangeToSeven;
        m_Input.FindActionMap("Default").FindAction("Inventory8").performed += m_InventoryController.ChangeToEight;
        m_Input.FindActionMap("Default").FindAction("Inventory9").performed += m_InventoryController.ChangeToNine;
        m_Input.FindActionMap("Default").FindAction("Inventory").performed += m_InventoryController.backpackVisible;
        m_Input.FindActionMap("Default").FindAction("QuickChange").performed += m_InventoryController.QuickChange;
        m_Input.FindActionMap("Default").FindAction("ChangeHandSecondary").performed += m_InventoryController.ChangeHandSecondary;
        m_Input.FindActionMap("Default").FindAction("FixEnemy").performed += m_PlayerHachaPicoArma.FixEnemy;
        m_Input.FindActionMap("Default").FindAction("QuitEnemy").performed += m_PlayerHachaPicoArma.QuitEnemy;
        m_Input.FindActionMap("Default").FindAction("Recolocar").performed += m_TakeItems.RecolocarEstructura;
        m_Input.FindActionMap("Default").FindAction("Menu").performed += m_Menu.OpenMenu;
        m_Input.FindActionMap("Pesca").FindAction("Pescar").performed += m_FishingMinigame.FishingMiniGame;
        //m_Input.FindActionMap("Pesca").FindAction("Hand").performed += m_InteractPlayer.Hand;
        //m_Input.FindActionMap("Pesca").FindAction("HandSecondary").performed += m_InteractPlayer.HandSecond;
        m_Delta = m_Input.FindActionMap("Default").FindAction("ScrollInventory");
        m_Input.FindActionMap("Default").Enable();
    }

    public void DisableDefault()
    {
        m_Input.FindActionMap("Default").Disable();
    }

    public void EnableDefault()
    {
        m_Input.FindActionMap("Default").Enable();
    }

    public void DisableMoveAndInventory()
    {
        m_MovementAction.Disable();
        m_Delta.Disable();
        foreach (var action in inventoryActions)
        {
            action?.Disable();
        }
    }

    public void EnableMoveAndInventory()
    {
        m_MovementAction.Enable();
        m_Delta.Enable();
        foreach (var action in inventoryActions)
        {
            action?.Enable();
        }
    }

    public void DisableMove()
    {
        m_MovementAction.Disable();
    }

    public void EnableMove()
    {
        m_MovementAction.Enable();
    }

    public void DisableHand()
    {
        m_Input.FindActionMap("Default").FindAction("Hand").Disable();
        m_Input.FindActionMap("Default").FindAction("HandSecondary").Disable();
    }

    public void EnableHand()
    {
        m_Input.FindActionMap("Default").FindAction("Hand").Enable();
        m_Input.FindActionMap("Default").FindAction("HandSecondary").Enable();
    }

    public void ChangeToPesca()
    {
        //Debug.Log("Pesca");
        m_Input.FindActionMap("Default").Disable();
        m_Input.FindActionMap("Pesca").Enable();
    }
    public void ChangeToDefault()
    {
        //Debug.Log("Default");
        m_Input.FindActionMap("Pesca").Disable();
        m_Input.FindActionMap("Default").Enable();
    }
}

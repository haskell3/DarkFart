using System.Collections;
using UnityEngine;

public class PescaPlayer : MonoBehaviour
{
    public bool exclamacion = false;
    public bool started = false;
    private TagGUIExclamation exclamacionGUI;

    public static PescaPlayer Instance { get; internal set; }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void StartFishing(float waitTime)
    {
        started = true;
        StartCoroutine(WaitingFish(waitTime));
        GetComponent<InputController>().DisableMoveAndInventory();
    }

    private IEnumerator WaitingFish(float waitTime)
    {
        while (true)
        {
            float r = Random.Range(waitTime, waitTime + 5);
            yield return new WaitForSeconds(r);
            exclamacion = true;
            if (exclamacionGUI == null)
                exclamacionGUI = FindAnyObjectByType<TagGUIExclamation>();
            exclamacionGUI.transform.GetChild(0).gameObject.SetActive(true);
            if(started)
                StartCoroutine(ExclamacionTime());

        }
    }

    private IEnumerator ExclamacionTime()
    {
        yield return new WaitForSeconds(5);
        exclamacion = false;
        exclamacionGUI.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void StopAll()
    {
        GetComponent<InputController>().EnableMoveAndInventory();
        started = false;
        exclamacion = false;
        exclamacionGUI.transform.GetChild(0).gameObject.SetActive(false);
        StopAllCoroutines();
    }
}

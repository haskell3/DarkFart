using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastPositionPlayer : MonoBehaviour
{
    [SerializeField] private Vector3 lastPosition;
    public Vector3 LastPosition { get { return lastPosition; } set { lastPosition = value; } }
}

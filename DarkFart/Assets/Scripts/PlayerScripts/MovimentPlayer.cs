using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;

[RequireComponent(typeof(PlayerHachaPicoArma))]
[RequireComponent(typeof(EnergiaController))]
[RequireComponent(typeof(HambreController))]
[RequireComponent(typeof(TakeItems))]
[RequireComponent(typeof(InteractPlayer))]
[RequireComponent(typeof(InputController))]
[RequireComponent(typeof(InventarioController))]
[RequireComponent(typeof(ObjectPool))]
public class MovimentPlayer : MonoBehaviour
{
    private InputController inputController;
    private InventarioController inventarioController;
    private EnergiaController energia;
    private HambreController hambre;
    private VidaController vida;
    private MenuController menu;
    private Rigidbody2D m_Rigidbody;
    private Vector2 m_Movement;
    private Vector2 m_LastInput;
    public Vector2 LastInput { get { return m_LastInput; } }

    private bool isInvulnerable;
    public bool IsInvulnerable { get { return isInvulnerable; } }
    [Header("Character Values")]
    [SerializeField]
    private float m_Speed = 2;
    public float Speed { get { return m_Speed; } }
    [SerializeField]
    private float m_Drag;
    [SerializeField]
    private float m_Acceleration;
    [SerializeField]
    private float m_Sprint;
    public float Sprint { get { return m_Sprint; } }
    [SerializeField]
    private float m_RollTime;
    [SerializeField]
    private float m_RollImpulse;
    [SerializeField]private Tilemap tilemap;
    public Tilemap Tilemap { get { return tilemap; } }
    private enum SwitchMachineStates { NONE, IDLE, WALK, SPRINT, ROLL };
    private SwitchMachineStates m_CurrentState;
    private SwitchMachineStates m_LastState;
    [SerializeField] private GameObject WhatInteract;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_LastState = m_CurrentState;
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.SPRINT:

                break;

            case SwitchMachineStates.ROLL:
                isInvulnerable = true;
                StartCoroutine(StopRolling());
                m_Rigidbody.AddForce( m_Rigidbody.velocity.normalized * m_RollImpulse, ForceMode2D.Impulse);
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.SPRINT:

                break;

            case SwitchMachineStates.ROLL:
                isInvulnerable = false;
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Movement = inputController.MovementAction.ReadValue<Vector2>();
                if (m_Movement != Vector2.zero)
                    ChangeState(SwitchMachineStates.WALK);
                break;
            case SwitchMachineStates.WALK:
                m_Movement = inputController.MovementAction.ReadValue<Vector2>();
                if (m_Movement == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                else
                    m_LastInput = m_Movement;
                break;

            case SwitchMachineStates.SPRINT:
                m_Movement = inputController.MovementAction.ReadValue<Vector2>() * m_Sprint;
                if (m_Movement == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                else
                    m_LastInput = m_Movement;
                break;

            case SwitchMachineStates.ROLL:

                break;

            default:
                break;
        }

        if(tilemap != null)
            WhatInteract.transform.position = tilemap.GetCellCenterWorld(tilemap.WorldToCell(new Vector2(transform.position.x, transform.position.y) + m_LastInput));
    }

    private void FixedUpdate()
    {
        Vector2 groundSpeed = Vector2.right * m_Rigidbody.velocity.x + Vector2.up * m_Rigidbody.velocity.y;

        if (groundSpeed.magnitude < m_Speed)
        {
            m_Rigidbody.AddForce((m_Movement * m_Speed) * m_Acceleration, ForceMode2D.Force);   
        }
        

        groundSpeed = Vector2.right * m_Rigidbody.velocity.x + Vector2.up * m_Rigidbody.velocity.y;
        //Drag
        m_Rigidbody.AddForce(-groundSpeed * m_Drag, ForceMode2D.Force);


    }


    void Awake()
    {
        inputController = GetComponent<InputController>();
        inventarioController = GetComponent<InventarioController>();
        hambre = GetComponent<HambreController>();
        energia = GetComponent<EnergiaController>();
        vida = GetComponent<VidaController>();
        m_Rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
        tilemap = TilemapController.PlantasTilemap;
    }
    
    void Update()
    {
        UpdateState();
    }

    private void OnDestroy()
    {
        inputController.DisableDefault();
    }

    public void ChangeStateSprint(InputAction.CallbackContext context)
    {
        if (m_CurrentState == SwitchMachineStates.ROLL) return;
        ChangeState(SwitchMachineStates.SPRINT);
    }

    public void ChangeStateWalk(InputAction.CallbackContext context)
    {
        if(m_CurrentState != SwitchMachineStates.SPRINT) return;
        ChangeState(SwitchMachineStates.WALK);
    }

    public void ChangeStateRoll(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.ROLL);
    }

    private IEnumerator StopRolling()
    {
        yield return new WaitForSeconds(m_RollTime);
        ChangeState(m_LastState);
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        tilemap = TilemapController.PlantasTilemap;
        menu = FindFirstObjectByType<MenuController>();
        inventarioController.SceneLoaded();
    }

    public void startAll()
    {
        hambre.startAll();
        energia.startAll();
        inventarioController.selectedItem(1);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpriteFrontBackSideAgent : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private NavMeshAgent rigidbody;
    [SerializeField] private Sprite[] sprites;
    public Sprite[] Sprites { get { return sprites; } set { this.sprites = value; } }
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rigidbody = GetComponent<NavMeshAgent>();

        StartCoroutine(WhichSprite());
    }

    private IEnumerator WhichSprite()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);

            Vector3 vel = rigidbody.velocity;

            if (Mathf.Abs(vel.x) > Mathf.Abs(vel.y))
            {
                spriteRenderer.sprite = sprites[2];
                if (vel.x < 0)
                    spriteRenderer.flipX = true;
                else
                    spriteRenderer.flipX = false;
            }
            else if (vel.y < 0)
            {
                spriteRenderer.sprite = sprites[0];
            }
            else if (vel.y > 0)
            {
                spriteRenderer.sprite = sprites[1];
            }
        }
    }
}

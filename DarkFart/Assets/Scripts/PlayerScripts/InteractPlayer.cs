using System;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(MovimentPlayer))]
[RequireComponent(typeof(InventarioController))]
[RequireComponent(typeof(InputController))]
public class InteractPlayer : MonoBehaviour
{
    private MovimentPlayer m_MovimentPlayer;
    [Header("Interactuable Layers")]
    [SerializeField]
    private LayerMask m_InteractuableMask;
    [SerializeField]
    private InventarioController m_InventoryController;
    [SerializeField]
    private AnimalesChangeScene m_animalChangeScene;
    [SerializeField]
    public GameObject lastAnimal;

    private bool cuerda;
    public bool Cuerda { get { return cuerda; } set { cuerda = value; } }

    private void Awake()
    {
        m_MovimentPlayer = GetComponent<MovimentPlayer>();
        m_InventoryController = GetComponent<InventarioController>();
    }

    public void Hand(InputAction.CallbackContext context)
    {
        int i = m_InventoryController.CurrentInventory;
        Item item = m_InventoryController.selectedItem(i);
        //throw new NotImplementedException();
        if (item == null)
        {
            return;
        }
        //Debug.Log(item.name);
        try
        {
            if (item is Cuerda)
            {
                GameObject animal = lastAnimal;
                lastAnimal = ((Cuerda)item).Action(transform.position, m_MovimentPlayer.LastInput, m_MovimentPlayer.Tilemap, m_MovimentPlayer.GetComponent<Rigidbody2D>(), animal);
                if (lastAnimal == null)
                    cuerda = false;
                else
                {
                    cuerda = true;
                    m_animalChangeScene.setAnimal(lastAnimal.GetComponent<AnimalController>().animal);
                }
            }
            else
            {
                ((IActionable)item).Action(transform.position, m_MovimentPlayer.LastInput, m_MovimentPlayer.Tilemap);
                if (item is Estructura && ((Estructura)item).isAble(transform.position, m_MovimentPlayer.LastInput, m_MovimentPlayer.Tilemap))
                    m_InventoryController.RemoveItemByPos(i);
                if (item is Semilla)
                    m_InventoryController.RemoveItemByPos(i);
            }
        }
        catch (Exception e)
        {
        }
        try
        {
            ((IConsume)item).Consume();
            if (item is Consumible || item is Manual || item is ManualRecetas)
                m_InventoryController.RemoveItemByPos(i);
        }
        catch (Exception e)
        {
        }
    }
    public void HandSecond(InputAction.CallbackContext context)
    {
        Item item = m_InventoryController.getSecondHand().Item;
        //throw new NotImplementedException();

        if (item == null)
        {
            return;
        }
        //Debug.Log(item.name);
        try
        {
            if (item is Cuerda)
            {
                GameObject animal = lastAnimal;
                lastAnimal = ((Cuerda)item).Action(transform.position, m_MovimentPlayer.LastInput, m_MovimentPlayer.Tilemap, m_MovimentPlayer.GetComponent<Rigidbody2D>(), animal);
                if (lastAnimal == null)
                    cuerda = false;
                else
                {
                    cuerda = true;
                    m_animalChangeScene.setAnimal(lastAnimal.GetComponent<AnimalController>().animal);
                }
            }
            else
            {
                ((IActionable)item).Action(transform.position, m_MovimentPlayer.LastInput, m_MovimentPlayer.Tilemap);
                if (item is Estructura && ((Estructura)item).isAble(transform.position, m_MovimentPlayer.LastInput, m_MovimentPlayer.Tilemap))
                    m_InventoryController.RemoveItemByPos(m_InventoryController.getBackpack().GetPosSlotSecondaryHand());
            }
        }
        catch (Exception e)
        {
        }
        try
        {
            ((IConsume)item).Consume();
            if (item is Consumible || item is Manual || item is ManualRecetas)
                m_InventoryController.RemoveItemByPos(m_InventoryController.getBackpack().GetPosSlotSecondaryHand());
        }
        catch (Exception e)
        {
        }
    }

    public void Interact(InputAction.CallbackContext context)
    {
        Debug.Log("Interact");
        Vector3 origin = transform.position;
        origin.z = -10;
        origin.x += m_MovimentPlayer.LastInput.x;
        origin.y += m_MovimentPlayer.LastInput.y;

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward, 20, m_InteractuableMask);

        if (hit.transform == null) return;
        if (!hit.transform.gameObject.TryGetComponent<Interactuable>(out Interactuable interactuable)) return;

        interactuable.Interact();
    }
}

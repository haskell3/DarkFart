using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    [SerializeField]
    private float z = -10;
    [SerializeField]
    private Transform player;
    private Vector3 pos;

    private void Start()
    {
        player = FindFirstObjectByType<MovimentPlayer>().transform;
    }

    void Update()
    {
        pos = player.position;
        pos.z = z;
        transform.position = pos;    
    }
}

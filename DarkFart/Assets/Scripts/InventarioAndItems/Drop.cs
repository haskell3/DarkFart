using UnityEngine;

public class Drop : MonoBehaviour
{
    [SerializeField]
    private GameObject m_itemDropped;
    public void Interact(Backpack.ItemSlot itemDropped)
    {
        Debug.Log($"Interact with DROP : {gameObject.name}");
        GameObject g = Instantiate(m_itemDropped);
        g.GetComponent<ItemInWorld>().Inicia(itemDropped.Item, itemDropped.Amount, InventarioController.Instance.whereDrop());
    }
}

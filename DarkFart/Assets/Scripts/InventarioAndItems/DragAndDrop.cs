using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragAndDrop : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [Header("Functionality")]
    [SerializeField]
    private LayerMask layersUI;

    [Header("Controller")]
    [SerializeField]
    private InventarioController m_InventarioController;

    private int m_clico;
    private int m_desclico;
    private GameObject m_gameObject;

    private void Start()
    {
        m_InventarioController = InventarioController.Instance;
    }

    public void setController(InventarioController controller)
    {
        m_InventarioController = controller;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (gameObject.GetComponent<DisplayItem>().GetItem() != null)
        {
            m_InventarioController.disableHand();
            m_clico = gameObject.GetComponent<DisplayItem>().getPosBackpack();
            //Debug.Log($"Clicando en : {gameObject.GetComponent<DisplayItem>().getPosBackpack()}");
            string text = gameObject.GetComponent<DisplayItem>().getText();
            string amount = gameObject.GetComponent<DisplayItem>().getAmount();
            Sprite img = gameObject.GetComponent<DisplayItem>().getImage();
            gameObject.GetComponent<DisplayItem>().getPacking().SetActive(false);
            Vector3 screenPoint = Input.mousePosition;
            screenPoint.z = 10.0f; //distance of the plane from the camera
            m_InventarioController.getObjectMove().transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
            m_InventarioController.getObjectMove().GetComponent<LoadItemMove>().loadItem(text, amount, img);
            m_InventarioController.getObjectMove().SetActive(true);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f; //distance of the plane from the camera
        m_InventarioController.getObjectMove().transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (gameObject.GetComponent<DisplayItem>().GetItem() != null)
            ScreenMouseRay();
        Refresh();
        //m_InventarioController.enableHand();
    }

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    public void ScreenMouseRay()
    {
        m_EventSystem = FindFirstObjectByType<TagEventSystem>().gameObject.GetComponent<EventSystem>();
        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        m_Raycaster = FindFirstObjectByType<TagCanvas>().gameObject.GetComponent<GraphicRaycaster>();
        m_Raycaster.Raycast(m_PointerEventData, results);
        if (results[0].gameObject != null && gameObject.GetComponent<DisplayItem>().GetItem() != null)
        {
            GameObject dondeClico = null;
            foreach (RaycastResult result in results)
            {
                if (result.gameObject.tag == "ItemBackpack")
                {
                    dondeClico = result.gameObject;
                    break;
                }
                else if (result.gameObject.tag == "ItemCofre")
                {
                    dondeClico = result.gameObject;
                    break;
                }
                else if (result.gameObject.tag == "ItemCaldero")
                {
                    dondeClico = result.gameObject;
                    break;
                }
                else if (result.gameObject.tag == "Drop")
                {
                    dondeClico = result.gameObject;
                    break;
                }
            }
            if (dondeClico != null)
            {
                if (dondeClico.GetComponent<DropAnotherInventory>())
                {
                    m_desclico = dondeClico.GetComponent<DisplayItem>().getPosBackpack();
                    m_InventarioController.getBackpack().changeToAnotherBackpack(m_desclico, m_clico, dondeClico.GetComponent<DropAnotherInventory>().getController().getInventory());
                }
                else if (dondeClico.GetComponent<DragAndDrop>())
                {
                    m_desclico = dondeClico.GetComponent<DisplayItem>().getPosBackpack();
                    m_InventarioController.getBackpack().changeBackpackItem(m_desclico, m_clico);
                    dondeClico.GetComponent<DisplayItem>().getPacking().SetActive(true);
                }
                else if (dondeClico.GetComponent<Drop>())
                {
                    dondeClico.GetComponent<Drop>().Interact(m_InventarioController.getBackpack().GetItemSlot(m_clico));
                    m_InventarioController.getBackpack().RemoveItemByPos(m_clico);
                }
                else if (dondeClico.GetComponent<DropInCaldero>())
                {
                    int amount = dondeClico.GetComponent<DropInCaldero>().Drop(m_InventarioController.getBackpack().GetItemSlot(m_clico));
                    if (amount <= 0)
                    {
                        m_InventarioController.getBackpack().RemoveItemByPos(m_clico);
                    }
                    else
                        m_InventarioController.getBackpack().GetItemSlot(m_clico).Amount = amount;
                }
            }
        }
        else
        {
            m_InventarioController.Refresh();
        }
    }

    private void Refresh()
    {
        m_InventarioController.Refresh();

        CofreController.Instance.Refresh();
        foreach (Object item in m_InventarioController.getObjectInPos(m_clico))
        {
            item.GetComponent<DisplayItem>().getPacking().SetActive(true);
        }
        if(m_InventarioController == null)
            m_InventarioController = InventarioController.Instance;
        m_InventarioController.getObjectMove().GetComponent<LoadItemMove>().desactivate();
    }
}

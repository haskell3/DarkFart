using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropAnotherInventory : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    CofreController m_controller;

    public void setController(CofreController controller)
    {
        m_controller = controller;
    }

    public CofreController getController()
    {
        return m_controller;
    }

    [Header("Functionality")]
    [SerializeField]
    private LayerMask layersUI;

    [Header("Controller")]
    [SerializeField]
    private InventarioController m_InventarioController;

    private int m_clico;
    private int m_desclico;
    private GameObject m_gameObject;

    private void Start()
    {
        m_InventarioController = InventarioController.Instance;

    }

    public void setController(InventarioController controller)
    {
        m_InventarioController = controller;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (gameObject.GetComponent<DisplayItem>().GetItem() != null)
        {
            m_InventarioController.disableHand();
            m_clico = gameObject.GetComponent<DisplayItem>().getPosBackpack();
            //Debug.Log($"Clicando en : {gameObject.GetComponent<DisplayItem>().getPosBackpack()}");
            string text = gameObject.GetComponent<DisplayItem>().getText();
            string amount = gameObject.GetComponent<DisplayItem>().getAmount();
            Sprite img = gameObject.GetComponent<DisplayItem>().getImage();
            gameObject.GetComponent<DisplayItem>().getPacking().SetActive(false);
            Vector3 screenPoint = Input.mousePosition;
            screenPoint.z = 10.0f; //distance of the plane from the camera
            m_InventarioController.getObjectMove().transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
            m_InventarioController.getObjectMove().GetComponent<LoadItemMove>().loadItem(text, amount, img);
            m_InventarioController.getObjectMove().SetActive(true);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f; //distance of the plane from the camera
        m_InventarioController.getObjectMove().transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (gameObject.GetComponent<DisplayItem>().GetItem() != null)
            ScreenMouseRay();
        Refresh();
    }

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    public void ScreenMouseRay()
    {
        m_EventSystem = FindFirstObjectByType<TagEventSystem>().gameObject.GetComponent<EventSystem>();

        //Set up the new Pointer Event
        m_PointerEventData = new PointerEventData(m_EventSystem);
        //Set the Pointer Event Position to that of the game object
        m_PointerEventData.position = Input.mousePosition;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        m_Raycaster = FindFirstObjectByType<TagCanvas>().gameObject.GetComponent<GraphicRaycaster>();
        //Raycast using the Graphics Raycaster and mouse click position
        m_Raycaster.Raycast(m_PointerEventData, results);
        /*
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = Mathf.Infinity;
        //RaycastHit2D hit = Physics2D.Raycast(Input.mousePosition ,Vector2.zero,Mathf.Infinity); //Hit object that contains gameobject Information
        RaycastHit2D hit = Physics2D.Raycast(mousePosition, mousePosition - Camera.main.ScreenToWorldPoint(mousePosition), Mathf.Infinity, layersUI);
        //Debug.DrawRay(mousePosition, mousePosition - Camera.main.ScreenToWorldPoint(mousePosition), Color.blue);
        */

        if (results[0].gameObject != null && gameObject.GetComponent<DisplayItem>().GetItem() != null)
        {
            GameObject dondeClico = null;
            //Debug.Log("Ray has been Cast and hit an Object");
            foreach (RaycastResult result in results)
            {
                if (result.gameObject.tag == "ItemCofre")
                {
                    dondeClico = result.gameObject; //Save the position of the object mouse was over
                    break;
                }
                else if (result.gameObject.tag == "ItemBackpack")
                {
                    dondeClico = result.gameObject;
                    break;
                }
                else if (result.gameObject.tag == "Drop")
                {
                    dondeClico = result.gameObject;
                    break;
                }
                //Debug.Log($"Result: {result}");
            }
            if (dondeClico != null)
            {
                if (dondeClico.GetComponent<DropAnotherInventory>())
                {
                    //Debug.Log("Another 1");
                    m_desclico = dondeClico.GetComponent<DisplayItem>().getPosBackpack();
                    Debug.Log($"Desclicando en : {m_desclico}");
                    //dondeClico.GetComponent<Drop>().Interact(m_InventarioController.getBackpack().GetItemSlot(m_clico));

                    dondeClico.GetComponent<DropAnotherInventory>().getController().changeCofreItem(m_desclico, m_clico);
                    //m_InventarioController.getBackpack().RemoveItemByPos(m_clico);
                }
                else if (dondeClico.GetComponent<DragAndDrop>())
                {
                    //Debug.Log("Another 2");
                    m_desclico = dondeClico.GetComponent<DisplayItem>().getPosBackpack();
                    Debug.Log($"Desclicando en : {m_desclico}");
                    m_InventarioController.getBackpack().changeToAnotherBackpack(m_clico, m_desclico, m_controller.getInventory());
                    dondeClico.GetComponent<DisplayItem>().getPacking().SetActive(true);
                }
            }
        }
        else
        {
            m_InventarioController.Refresh();
        }
    }

    private void Refresh()
    {
        m_InventarioController.Refresh();
        CofreController.Instance.Refresh();
        foreach (Object item in m_InventarioController.getObjectInPos(m_clico))
        {
            item.GetComponent<DisplayItem>().getPacking().SetActive(true);
        }
        m_InventarioController.getObjectMove().GetComponent<LoadItemMove>().desactivate();
    }
}

using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static Backpack;

[RequireComponent(typeof(InputController))]
public class InventarioController : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private Backpack m_Backpack;
    [Header("GUI")]
    [SerializeField]
    private DisplayBackpack m_DisplayBackpack;
    [SerializeField]
    private DisplayHotbar m_DisplayHotbar;
    [SerializeField]
    private GameObject m_ObjectMove;
    [SerializeField]
    private GameObject m_DescripcionItem;
    [SerializeField]
    private Object[] m_AllObjects;
    [SerializeField]
    private LayerMask layersDrop;
    [Header("Input")]
    private InputController _controller;
    private Vector2 delta;
    [SerializeField]
    private int m_CurrentInventory = 1;
    private int m_LastInventory;
    public int CurrentInventory { get { return m_CurrentInventory; } }
    [SerializeField]
    private float m_Sensivity = 120f;
    [SerializeField]
    private int rangeDrop;

    public static InventarioController Instance { get; internal set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        _controller = GetComponent<InputController>();
    }

    private void Update()
    {
        delta = _controller.Delta.ReadValue<Vector2>();
        if (delta.y > 0)
        {
            while (delta.y > 0)
            {
                delta.y -= m_Sensivity;
                m_CurrentInventory--;
                if (m_CurrentInventory <= 0)
                    m_CurrentInventory = 9;
                selectedItem(m_CurrentInventory);
            }

        }
        else if (delta.y < 0)
        {
            while (delta.y < 0)
            {
                delta.y += m_Sensivity;
                m_CurrentInventory++;
                if (m_CurrentInventory >= 10)
                    m_CurrentInventory = 1;
                selectedItem(m_CurrentInventory);
            }
        }
        /*
        if (Input.GetKeyDown(KeyCode.N))
        {
            int randomX = Random.Range(-1, 2);
            int randomY = Random.Range(-1, 2);
            Debug.Log($"Transform X Random: {transform.position.x + randomX} --- Transform Y Random: {transform.position.y + randomY}");
        }
        */
    }

    public Vector3 whereDrop()
    {
        if (rangeDrop <= 0)
            rangeDrop = 2;
        int randomX = Random.Range(-rangeDrop, rangeDrop + 1);
        int randomY = Random.Range(-rangeDrop, rangeDrop + 1);
        RaycastHit2D hit = Physics2D.Raycast(new Vector3(transform.position.x + randomX, transform.position.y + randomY, transform.position.z), transform.forward, Mathf.Infinity, layersDrop);
        if (hit.collider != null)
        {
            Debug.Log("Hay algo: " + hit.collider.name);
        }
        while (((Mathf.Abs(randomX) < 0.9) || (Mathf.Abs(randomY) < 0.9)) && hit.collider != null)
        {
            randomX = Random.Range(-rangeDrop, rangeDrop + 1);
            randomY = Random.Range(-rangeDrop, rangeDrop + 1);
            hit = Physics2D.Raycast(new Vector3(transform.position.x + randomX, transform.position.y + randomY, transform.position.z), transform.forward, Mathf.Infinity, layersDrop);
            if (hit.collider != null)
            {
                Debug.Log("Hay algo: " + hit.collider.name);
            }
        }
        //Debug.Log($"Transform X Random: {transform.position.x + randomX} --- Transform Y Random: {transform.position.y + randomY}");
        Refresh();
        return new Vector3(transform.position.x + randomX, transform.position.y + randomY, transform.position.z);
    }

    public void AddItem(Item item)
    {
        m_Backpack.AddItem(item);
        m_DisplayBackpack.RefreshBackpack();
        m_DisplayHotbar.RefreshBackpack();
    }

    public int AddItem(Item item, int amount)
    {
        int saveAmount = m_Backpack.AddItem(item, amount);
        m_DisplayBackpack.RefreshBackpack();
        m_DisplayHotbar.RefreshBackpack();
        return saveAmount;
    }

    public void RemoveItemByPos(int pos)
    {
        m_Backpack.RemoveByPos(pos);
        Refresh();
    }

    public void Refresh()
    {
        if (m_DisplayBackpack.gameObject.activeSelf == true)
            m_DisplayBackpack.RefreshBackpack();
        if (m_DisplayHotbar.gameObject.activeSelf == true)
            m_DisplayHotbar.RefreshBackpack();
    }

    public Backpack getBackpack()
    {
        return m_Backpack;
    }

    public void LoadArmadura(Item armadura)
    {
        if (armadura is Armadura && armadura != null)
            gameObject.GetComponent<VidaController>().ExtraHp((Armadura)armadura);
        else
            gameObject.GetComponent<VidaController>().ExtraHp(null);
    }

    public void setArmadura(Item armadura)
    {
        if (armadura is Armadura && armadura != null)
            m_Backpack.setArmadura((Armadura)armadura);
    }

    public ItemSlot getSecondHand()
    {
        return m_Backpack.GetSecondaryHand();
    }

    public DisplayBackpack getDisplayBackpack()
    {
        return m_DisplayBackpack;
    }

    public void setDisplayBackpack(DisplayBackpack displayBackpack)
    {
        m_DisplayBackpack = displayBackpack;
    }

    public DisplayHotbar getDisplayHotbar()
    {
        return m_DisplayHotbar;
    }

    public void setDisplayHotbar(DisplayHotbar displayHotbar)
    {
        m_DisplayHotbar = displayHotbar;
    }

    public GameObject getObjectMove()
    {
        return m_ObjectMove;
    }

    public GameObject getDescripcionItem()
    {
        return m_DescripcionItem;
    }

    public GameObject getHotbartMove()
    {
        return m_ObjectMove;
    }

    public void getAll()
    {
        m_AllObjects = FindObjectsOfType(typeof(DisplayItem));
    }

    public List<Object> getObjectInPos(int pos)
    {
        List<Object> displayItems = new List<Object>();
        foreach (Object item in FindObjectsOfType(typeof(DragAndDrop)))
        {
            if (item.GetComponent<DisplayItem>().getPosBackpack() == pos)
            {
                displayItems.Add(item);
            }
        }
        return displayItems;
    }

    public Object getHotbarInPos(int pos)
    {
        pos -= 1;
        foreach (Object item in m_AllObjects)
        {
            if (item.GetComponent<DisplayItem>().getPosBackpack() == pos)
            {
                return item;
            }
        }
        return null;
    }

    public Item selectedItem(int pos)
    {
        pos -= 1;
        if (m_DisplayHotbar == null)
            return null;
        DisplayItem[] hotbar = m_DisplayHotbar.GetComponentsInChildren<DisplayItem>();
        Item itemSelected = null;
        foreach (DisplayItem item in hotbar)
        {
            if (pos == item.getPosBackpack())
            {
                item.getSelected().gameObject.SetActive(true);
                itemSelected = item.GetItem();
            }
            else
                item.getSelected().gameObject.SetActive(false);
        }
        return itemSelected;
    }

    public void backpackVisible(InputAction.CallbackContext context)
    {
        guiController.Instance.disableAll();
        m_DisplayBackpack.gameObject.SetActive(!m_DisplayBackpack.gameObject.activeInHierarchy);
        if (m_DisplayBackpack.gameObject.activeSelf == true)
        {
            Refresh();
            _controller.DisableHand();
            guiController.Instance.disableAll();
        }
        else
        {
            _controller.EnableHand();
            guiController.Instance.enableAll();
            getDescripcionItem().GetComponent<DescripcionItem>().desactivate();
        }

    }

    public void disableHand()
    {
        _controller.DisableHand();
    }

    public void enableHand()
    {
        _controller.EnableHand();
    }

    public void QuickChange(InputAction.CallbackContext context)
    {
        m_CurrentInventory = m_LastInventory;
        selectedItem(m_CurrentInventory);
    }

    public void ChangeHandSecondary(InputAction.CallbackContext context)
    {
        m_Backpack.SetSecondaryHand(m_CurrentInventory);
        Refresh();
    }

    public void ChangeToOne(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 1) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 1;
        selectedItem(m_CurrentInventory);
    }
    public void ChangeToTwo(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 2) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 2;
        selectedItem(m_CurrentInventory);
    }
    public void ChangeToThree(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 3) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 3;
        selectedItem(m_CurrentInventory);
    }
    public void ChangeToFour(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 4) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 4;
        selectedItem(m_CurrentInventory);
    }
    public void ChangeToFive(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 5) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 5;
        selectedItem(m_CurrentInventory);
    }
    public void ChangeToSix(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 6) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 6;
        selectedItem(m_CurrentInventory);
    }
    public void ChangeToSeven(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 7) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 7;
        selectedItem(m_CurrentInventory);
    }
    public void ChangeToEight(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 8) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 8;
        selectedItem(m_CurrentInventory);
    }
    public void ChangeToNine(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 9) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 9;
        selectedItem(m_CurrentInventory);
    }

    public void SceneLoaded()
    {
        m_DisplayBackpack = FindFirstObjectByType<TagGUIBackPack>(FindObjectsInactive.Include)?.gameObject?.GetComponent<DisplayBackpack>();
        m_ObjectMove = FindFirstObjectByType<TagGUIObject>()?.gameObject;
        m_DescripcionItem = FindFirstObjectByType<TagDesc>()?.gameObject;
    }
}

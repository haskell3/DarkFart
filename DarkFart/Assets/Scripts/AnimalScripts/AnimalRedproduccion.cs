using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AnimalController))]
public class AnimalRedproduccion : MonoBehaviour
{
    private float probabilidad;
    [SerializeField] private GameObject prefab;

    private void Awake()
    {
        probabilidad = GetComponent<AnimalController>().animal.ProbabilidadReproduccion;
    }

    public void AddDay()
    {
        if(Random.Range(0, 101) < probabilidad)
        {
            Instantiate(prefab).transform.position = transform.position;
        }
    }
}

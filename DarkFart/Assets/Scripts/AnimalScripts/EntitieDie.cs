using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitieDie : MonoBehaviour, IDieable
{
    [SerializeField]
    private List<Item> drops;
    [SerializeField]
    private int quantityDrop;
    public List<Item> Drops
    {
        get { return drops; }
        set { drops = value; }
    }
    [SerializeField]
    private GameObject itemInWorld;
    public void Die()
    {
        Item i = drops[Random.Range(0, drops.Count)];
        if (quantityDrop == 0) quantityDrop = 1;
        int quantity = Random.Range(1, quantityDrop);
        GameObject g = Instantiate(itemInWorld);
        g.GetComponent<ItemInWorld>().Inicia(i, quantity, transform.position);

        Destroy(this.gameObject);
    }
}

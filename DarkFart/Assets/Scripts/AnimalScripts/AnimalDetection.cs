using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AnimalDetection : MonoBehaviour
{
    [SerializeField]
    private AnimalController controller;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 3)
        {
            controller.ChangeToRunAway(collision.transform);
        }
        if (collision.gameObject.layer == 7)
        {
            int i = InventarioController.Instance.CurrentInventory;
            Item item = InventarioController.Instance.selectedItem(i);
            if (item is Alimento)
            {
                controller.ChangeToChasing();
                StartCoroutine(WatchPlayer());
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 3)
        {
            controller.ChangeToRandomWalk();
        }
        if (collision.gameObject.layer == 7)
        {
            if (controller.m_CurrentState == AnimalController.SwitchMachineStates.CHASE)
            {
                controller.ChangeToRandomWalk();
            }
            
        }
    }

    IEnumerator WatchPlayer()
    {
        while(true)
        {
            int i = InventarioController.Instance.CurrentInventory;
            Item item = InventarioController.Instance.selectedItem(i);
            if (item is not Alimento)
            {
                controller.ChangeToRandomWalk();
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
}

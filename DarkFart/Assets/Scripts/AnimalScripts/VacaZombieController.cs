using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VacaZombieController : MonoBehaviour
{
    [SerializeField]
    private VacaZombie vacaZombieSO;
    [SerializeField]
    private TimeManager timeManager;
    [SerializeField]
    private bool isAble = false;
    public bool IsAble
    {
       get { return isAble; }
    }

    private float increment;
    //private float secsIncrement;

    private float timeForEggLeft;

    public void Start()
    {
        vacaZombieSO = (VacaZombie) GetComponent<AnimalController>().animal;
        timeForEggLeft = vacaZombieSO.MeatTime;
        if (timeManager == null)
            timeManager = TimeManager.Instance;
        increment = timeManager.Increment;
        //secsIncrement = timeManager.SecsIncrement;
    }

    public void CheckTime()
    {
        timeForEggLeft -= increment;
        if (timeForEggLeft == 0)
        {
            isAble = true;
        }
    }

    public Alimento GetCarne()
    {
        isAble = false;
        timeForEggLeft = vacaZombieSO.MeatTime;
        return vacaZombieSO.Carne;
    }
}

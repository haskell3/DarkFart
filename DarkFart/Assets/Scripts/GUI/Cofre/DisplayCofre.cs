using UnityEngine;

public class DisplayCofre : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private GameObject m_DisplayBackpackParent;

    [SerializeField]
    private GameObject m_DisplayItemPrefab;

    [Header("Controller")]
    [SerializeField]
    private CofreController m_CofreController;
    private void Start()
    {
        m_CofreController = CofreController.Instance;
        RefreshAllBackpack();
    }

    private void ClearDisplay()
    {
        foreach (Transform child in m_DisplayBackpackParent.transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay()
    {
        for (int i = 0; i < m_CofreController.getInventory().Length; i++)
        {
            GameObject displayedItem = Instantiate(m_DisplayItemPrefab, m_DisplayBackpackParent.transform);
            displayedItem.GetComponent<DisplayItem>().Load(m_CofreController.getInventory()[i]);
            displayedItem.GetComponent<DisplayItem>().setPos(i);
            //displayedItem.GetComponent<DisplayItem>().setController(m_InventarioController);
            displayedItem.GetComponent<DropAnotherInventory>().setController(m_CofreController);
            //m_InventarioController.getAll();
        }
    }

    private void LoadEverything()
    {
        DisplayItem[] displayeds = m_DisplayBackpackParent.GetComponentsInChildren<DisplayItem>();
        for (int i = 0; i < displayeds.Length; i++)
        {
            displayeds[i].GetComponent<DisplayItem>().Load(m_CofreController.getInventory()[i]);
        }
    }

    public void RefreshBackpack()
    {
        LoadEverything();
    }

    public void RefreshAllBackpack()
    {
        ClearDisplay();
        FillDisplay();
    }
}

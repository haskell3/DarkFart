using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class DisplayItemCofre : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private GameEventItem m_Event;

    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private TextMeshProUGUI m_AmountText;
    [SerializeField]
    private Image m_Image;
    [SerializeField]
    private Image m_Selected;

    [SerializeField]
    private Sprite m_SpriteDefault;

    [SerializeField]
    private int posBackpack;

    [SerializeField]
    private GameObject m_Packing;

    [SerializeField]
    private Item m_Item;

    [Header("Controller")]
    [SerializeField]
    private InventarioController m_InventarioController;

    public void setController(InventarioController controller)
    {
        m_InventarioController = controller;
    }
    public GameObject getPacking()
    {
        return m_Packing;
    }

    public Item GetItem()
    {
        return m_Item;
    }

    public void setItem(Item item)
    {
        m_Item = item;
    }

    public string getText() { return m_Text.text; }
    public string getAmount() { return m_AmountText.text; }
    public Sprite getImage() { return m_Image.sprite; }
    public GameObject getGameObject() { return gameObject; }

    public Image getSelected() { return m_Selected; }

    public void Load(Item item)
    {
        m_Packing.SetActive(true);
        if (item != null && item.Name != null)
        {
            m_Item = item;
            m_Text.gameObject.SetActive(true);
            m_AmountText.gameObject.SetActive(true);
            m_Image.gameObject.SetActive(true);
            m_Text.text = item.Name;
            if (item.Sprite != null)
                m_Image.sprite = item.Sprite;
            else
                m_Image.sprite = m_SpriteDefault;

            GetComponent<Button>().onClick.RemoveAllListeners();
            GetComponent<Button>().onClick.AddListener(() => RaiseEvent(item));
        }
        else
        {
            m_Item = null;
            m_Text.gameObject.SetActive(false);
            m_AmountText.gameObject.SetActive(false);
            m_Image.gameObject.SetActive(false);
        }
    }

    public void Load(Backpack.ItemSlot itemSlot)
    {
        Load(itemSlot.Item);
        m_AmountText.text = itemSlot.Amount.ToString();
    }

    public void setPos(int pos)
    {
        posBackpack = pos;
    }

    public int getPosBackpack()
    {
        return posBackpack;
    }

    private void RaiseEvent(Item item)
    {
        //m_Event?.Raise(item);
    }
    /*
    public void OnDrop(PointerEventData eventData)
    {
        GameObject dropped = eventData.pointerDrag;
        Debug.Log(dropped.gameObject.name);
        Debug.Log(gameObject.name);
        Debug.Log(dropped.GetComponent<Drag>().itemDragged() + ": NewPos: " + posBackpack + "/ OldPos: " + dropped.GetComponentInParent<DisplayItem>().getPosBackpack());
        m_Event2?.Raise(dropped.GetComponent<Drag>().itemDragged(), dropped.GetComponentInParent<DisplayItem>().getAmountItem(), posBackpack, dropped.GetComponentInParent<DisplayItem>().getPosBackpack());
    }
    */
}


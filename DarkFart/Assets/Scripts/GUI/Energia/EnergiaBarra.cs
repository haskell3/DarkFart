using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergiaBarra : MonoBehaviour
{
    private Image barraEnergia;
    private GameObject Player;

    void Start()
    {
        barraEnergia = GetComponent<Image>();
        Player = FindFirstObjectByType<MovimentPlayer>()?.gameObject;
        mostrar();
    }

    public void mostrar()
    {
        if (barraEnergia != null && Player != null)
            barraEnergia.fillAmount = Player.GetComponent<EnergiaController>().getEnergia() / Player.GetComponent<EnergiaController>().getMaxEnergia();
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SleepGUI : MonoBehaviour
{
    private TextMeshProUGUI m_TextMeshProUGUI;
    [SerializeField] private GameObject m_TimeManager;
    [SerializeField] private GameObject m_Player;
    private TimeManager timeManager;
    private EnergiaController energiaController;
    private VidaController vidaController;
    private InputController inputController;
    private TagDrop drop;
    private void Awake()
    {
        m_Player = FindFirstObjectByType<MovimentPlayer>().gameObject;
        m_TextMeshProUGUI = GetComponentInChildren<TextMeshProUGUI>(true);
        //timeManager = m_TimeManager.GetComponent<TimeManager>();
        timeManager = FindObjectOfType<TimeManager>();
        energiaController = m_Player.GetComponent<EnergiaController>();
        inputController = m_Player.GetComponent<InputController>();
        vidaController = m_Player.GetComponent<VidaController>();
        drop = FindAnyObjectByType<TagDrop>(FindObjectsInactive.Include);
    }
    public void IWantToSleep()
    {
        drop.gameObject.SetActive(false);
        inputController.DisableDefault();
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void IDontWantToSleep()
    {
        drop.gameObject.SetActive(true);
        inputController.EnableDefault();
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void HoursSelected()
    {
        int hours = Int32.Parse(m_TextMeshProUGUI.text);
        timeManager.SleepXHours(hours);
        energiaController.RecoverFromSleep(hours);
        vidaController.RecoverFromSleep(hours);
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        inputController.EnableDefault();
    }

    public void SumarHora()
    {
        int hora = Int32.Parse(m_TextMeshProUGUI.text);
        hora++;
        m_TextMeshProUGUI.text = hora + "";
    }

    public void RestarHora()
    {
        int hora = Int32.Parse(m_TextMeshProUGUI.text);
        hora--;
        if (hora < 0) hora = 0;
        m_TextMeshProUGUI.text = hora + "";
    }
}

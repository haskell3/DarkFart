using UnityEngine;
using UnityEngine.EventSystems;
using static Backpack;

public class DisplayHotbar : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private GameObject m_DisplayHotbarParent;

    [SerializeField]
    private GameObject m_DisplaySecondHandParent;

    [SerializeField]
    private GameObject m_DisplayItemPrefab;

    [Header("Controller")]
    [SerializeField]
    private InventarioController m_InventarioController;

    private void Start()
    {
        m_InventarioController = InventarioController.Instance;
        m_InventarioController.setDisplayHotbar(this);
        RefreshAllBackpack();
    }

    private void ClearDisplay()
    {
        foreach (Transform child in m_DisplayHotbarParent.transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay()
    {
        for (int i = 0; i < 9; i++)
        {
            GameObject displayedItem = Instantiate(m_DisplayItemPrefab, m_DisplayHotbarParent.transform);
            displayedItem.GetComponent<DisplayItem>().Load(m_InventarioController.getBackpack().ItemSlots2[i]);
            displayedItem.GetComponent<DisplayItem>().setPos(i);
            displayedItem.GetComponent<DisplayItem>().setController(m_InventarioController);
            displayedItem.GetComponent<DragAndDrop>().setController(m_InventarioController);
            m_InventarioController.getAll();
        }
        if (m_DisplaySecondHandParent != null)
        {
            GameObject displayedItem = Instantiate(m_DisplayItemPrefab, m_DisplaySecondHandParent.transform);
            displayedItem.GetComponent<DisplayItem>().Load(m_InventarioController.getSecondHand());
            displayedItem.GetComponent<DisplayItem>().setPos(m_InventarioController.getBackpack().GetPosSlotSecondaryHand());
            displayedItem.GetComponent<DisplayItem>().setController(m_InventarioController);
            displayedItem.GetComponent<DragAndDrop>().setController(m_InventarioController);
        }
        /*
        foreach (Backpack.ItemSlot itemSlot in m_Backpack.ItemSlots2)
        {
            GameObject displayedItem = Instantiate(m_DisplayItemPrefab, m_DisplayBackpackParent.transform);
            displayedItem.GetComponent<DisplayItem>().Load(itemSlot.Item);
        }
        */
    }
    private void LoadEverything()
    {
        DisplayItem[] displayeds = m_DisplayHotbarParent.GetComponentsInChildren<DisplayItem>();
        //Debug.Log(displayeds[1].name);
        for (int i = 0; i < displayeds.Length; i++)
        {
            displayeds[i].GetComponent<DisplayItem>().Load(m_InventarioController.getBackpack().ItemSlots2[i]);
        }
        if (m_DisplaySecondHandParent != null)
        {
            m_DisplaySecondHandParent.GetComponentInChildren<DisplayItem>().Load(m_InventarioController.getSecondHand());
        }
    }

    public void RefreshBackpack()
    {
        LoadEverything();
    }

    public void RefreshAllBackpack()
    {
        ClearDisplay();
        FillDisplay();
    }
}

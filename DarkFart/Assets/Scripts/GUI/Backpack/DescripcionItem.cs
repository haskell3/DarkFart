using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DescripcionItem : MonoBehaviour
{
    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private TextMeshProUGUI m_ExtraText;
    [SerializeField]
    private TextMeshProUGUI m_ExtraText2;
    [SerializeField]
    private Image m_Background;
    [SerializeField]
    private Image m_Background2;
    private int sizeMin = 17;
    private int sizeMax = 34;

    public void loadItem(string text)
    {
        loadItem(text, "", "");
        size(sizeMin);
        activate();
    }

    public void loadItem(string text, string extraText)
    {
        loadItem(text, extraText, "");
        size(sizeMax);
        activate();
    }
    public void loadItem(string text, string extraText, string extraText2)
    {
        m_Text.text = text;
        m_ExtraText.text = extraText;
        m_ExtraText2.text = extraText2;
        size(sizeMax);
        activate();
    }

    private void size(int size)
    {
        RectTransform rt = m_Background.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(rt.rect.width, size - 2);
        rt = m_Background2.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(rt.rect.width, size);
    }

    private void activate()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void desactivate()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VidaBarra : MonoBehaviour
{
    private Image barraVida;
    [SerializeField]
    private TextMeshProUGUI textVida;
    private GameObject Player;
    private VidaController controller;

    void Awake()
    {
        barraVida = GetComponent<Image>();
        Player = FindFirstObjectByType<MovimentPlayer>()?.gameObject;
        if (Player != null)
        {
            controller = Player.GetComponent<VidaController>();
            controller.Gui = this;
        }
        mostrar();
    }
    private void Start()
    {
        mostrar();
    }

    public void mostrar()
    {
        if (barraVida != null && controller != null)
            barraVida.fillAmount = controller.mirarHp() / controller.mirarHpMax();
        if (textVida != null && controller != null)
            textVida.text = $"{Mathf.Round(controller.mirarHp())}/{Mathf.Round(controller.mirarHpMax())}";
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;

public class guiController : MonoBehaviour
{
    public static guiController Instance { get; internal set; }
    [Header("GUI")]
    [SerializeField]
    private GameObject[] m_guiObjects;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void disableAll()
    {
        if (m_guiObjects != null)
        {
            foreach (GameObject obj in m_guiObjects)
            {
                if (obj != null)
                {
                    obj.SetActive(false);
                }
            }
        }
    }

    public void enableAll()
    {
        if (m_guiObjects != null)
        {
            foreach (GameObject obj in m_guiObjects)
            {
                if (obj != null)
                {
                    obj.SetActive(true);
                }
            }
        }
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        getCamera();
    }

    private void getCamera()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
    }

}

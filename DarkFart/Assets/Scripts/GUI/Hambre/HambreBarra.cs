using UnityEngine;
using UnityEngine.UI;

public class HambreBarra : MonoBehaviour
{
    private Image barraHambre;
    private GameObject Player;

    void Start()
    {
        barraHambre = GetComponent<Image>();
        Player = FindFirstObjectByType<MovimentPlayer>()?.gameObject;
        mostrar();
    }

    public void mostrar()
    {
        if (barraHambre != null && Player != null)
            barraHambre.fillAmount = Player.GetComponent<HambreController>().getHambre() / Player.GetComponent<HambreController>().getMaxHambre();
    }
}

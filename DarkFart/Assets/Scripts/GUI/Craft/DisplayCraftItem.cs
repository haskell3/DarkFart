using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class DisplayCraftItem : MonoBehaviour, IPointerDownHandler
{
    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private TextMeshProUGUI m_NewCraft;
    [SerializeField]
    private Image m_Image;
    [SerializeField]
    private Image m_Locked;
    [SerializeField]
    private bool m_IsNewCraft;

    [SerializeField]
    private Sprite m_SpriteDefault;

    [SerializeField]
    private int posCrafteo;
    [SerializeField]
    private bool unlocked;

    [SerializeField]
    private Item m_Result;

    [Header("Controller")]
    [SerializeField]
    private CraftController m_CraftController;

    public void setController(CraftController controller)
    {
        m_CraftController = controller;
    }

    public Item GetItem()
    {
        return m_Result;
    }

    public string getText() { return m_Text.text; }
    public Sprite getImage() { return m_Image.sprite; }
    public GameObject getGameObject() { return gameObject; }

    public Image getLockedIMG() { return m_Locked; }

    public void Load(Craft craft)
    {
        if (craft != null && craft.Result.Name != null)
        {
            m_Result = craft.Result;
            m_Text.text = craft.Result.Name;
            if (craft.Result.Sprite != null)
            {
                m_Image.sprite = craft.Result.Sprite;
                m_Locked.sprite = craft.Result.Sprite;
            }
            else
            {
                m_Image.sprite = m_SpriteDefault;
                m_Locked.sprite = m_SpriteDefault;
            }
            //m_IsNewCraft = true;
            //GetComponent<Button>().onClick.RemoveAllListeners();
            //GetComponent<Button>().onClick.AddListener(() => RaiseEvent(craft));
        }
        if (unlocked)
        {
            m_Text.gameObject.SetActive(true);
            m_Image.gameObject.SetActive(true);
            m_Locked.gameObject.SetActive(false);
            if (m_IsNewCraft)
            {
                m_Locked.gameObject.SetActive(true);
                m_NewCraft.gameObject.SetActive(true);
            }
            else
            {
                m_NewCraft.gameObject.SetActive(false);
            }

        }
        else
        {
            m_Text.gameObject.SetActive(false);
            m_Image.gameObject.SetActive(false);
            m_Locked.gameObject.SetActive(true);
            if (m_IsNewCraft)
            {
                m_Locked.gameObject.SetActive(true);
                m_NewCraft.gameObject.SetActive(true);
            }
            else
            {
                m_NewCraft.gameObject.SetActive(false);
            }
        }
    }

    public void Load(CraftBook.CraftRecipe craftRecipe)
    {
        unlocked = craftRecipe.unlocked;
        m_IsNewCraft = craftRecipe.newCraft;
        Load(craftRecipe.recipe);
    }

    public void setPos(int pos)
    {
        posCrafteo = pos;
    }

    public int getPos()
    {
        return posCrafteo;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (unlocked)
        {
            if (m_IsNewCraft)
            {
                m_Locked.gameObject.SetActive(false);
                m_NewCraft.gameObject.SetActive(false);
                m_IsNewCraft = false;
            }
        }
        m_CraftController.loadDisplayCraftItem(posCrafteo, m_IsNewCraft);
    }
}


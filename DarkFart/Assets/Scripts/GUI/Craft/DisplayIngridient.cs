using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayIngridient : MonoBehaviour
{
    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private TextMeshProUGUI m_Quantity;
    [SerializeField]
    private Image m_Image;

    [SerializeField]
    private Sprite m_SpriteDefault;

    public void Load(Craft.Ingredients ingredient, int quantityBackpack)
    {
        if (ingredient != null && ingredient.ingredient.Name != null)
        {
            m_Text.gameObject.SetActive(true);
            m_Quantity.gameObject.SetActive(true);
            m_Image.gameObject.SetActive(true);
            m_Text.text = ingredient.ingredient.Name;
            if (ingredient.ingredient.Sprite != null)
                m_Image.sprite = ingredient.ingredient.Sprite;
            else
                m_Image.sprite = m_SpriteDefault;
            if (quantityBackpack < ingredient.quantity)
                m_Quantity.color = Color.red;
            m_Quantity.text = ingredient.quantity.ToString() + '(' + quantityBackpack + ')'; //Quiero poner la cantidad que tienes en tu inventario
        }
        else
        {
            m_Text.gameObject.SetActive(false);
            m_Quantity.gameObject.SetActive(false);
            m_Image.gameObject.SetActive(false);
        }
    }

    public void Load(Receta.Ingredients ingredient, int quantityBackpack)
    {
        if (ingredient != null && ingredient.ingredient.Name != null)
        {
            m_Text.gameObject.SetActive(true);
            m_Quantity.gameObject.SetActive(true);
            m_Image.gameObject.SetActive(true);
            m_Text.text = ingredient.ingredient.Name;
            if (ingredient.ingredient.Sprite != null)
                m_Image.sprite = ingredient.ingredient.Sprite;
            else
                m_Image.sprite = m_SpriteDefault;
            if (quantityBackpack < ingredient.quantity)
                m_Quantity.color = Color.red;
            m_Quantity.text = ingredient.quantity.ToString() + '(' + quantityBackpack + ')'; //Quiero poner la cantidad que tienes en tu inventario
        }
        else
        {
            m_Text.gameObject.SetActive(false);
            m_Quantity.gameObject.SetActive(false);
            m_Image.gameObject.SetActive(false);
        }
    }

}

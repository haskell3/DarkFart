using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DisplayCook : MonoBehaviour, IPointerDownHandler
{
    [Header("Display")]
    [SerializeField]
    private Image m_Image;
    [SerializeField]
    private Image m_Locked;

    [SerializeField]
    private Sprite m_SpriteDefault;

    [SerializeField]
    private int posCook;
    [SerializeField]
    private bool unlocked;

    [SerializeField]
    private Item m_Result;

    [Header("Controller")]
    [SerializeField]
    private CookController m_CookController;

    public void setController(CookController controller)
    {
        m_CookController = controller;
    }

    public Item GetItem()
    {
        return m_Result;
    }

    public Sprite getImage() { return m_Image.sprite; }
    public GameObject getGameObject() { return gameObject; }

    public Image getLockedIMG() { return m_Locked; }

    public void Load(Receta cook)
    {
        if (cook != null && cook.Result.Name != null)
        {
            m_Result = cook.Result;
            if (cook.Result.Sprite != null)
            {
                m_Image.sprite = cook.Result.Sprite;
                m_Locked.sprite = cook.Result.Sprite;
            }
            else
            {
                m_Image.sprite = m_SpriteDefault;
                m_Locked.sprite = m_SpriteDefault;
            }
        }
        if (unlocked)
        {
            m_Image.gameObject.SetActive(true);
            m_Locked.gameObject.SetActive(false);
        }
        else
        {
            m_Image.gameObject.SetActive(false);
            m_Locked.gameObject.SetActive(true);
        }
    }

    public void Load(CookBook.CookRecipe cookRecipe)
    {
        unlocked = cookRecipe.unlocked;
        Load(cookRecipe.recipe);
    }

    public void setPos(int pos)
    {
        posCook = pos;
    }

    public int getPos()
    {
        return posCook;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        m_CookController.loadDisplayCookRecipe(posCook);
    }
}



using UnityEngine;

public class DisplayCookBook : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private GameObject m_DisplayRecipesParent;
    [SerializeField]
    private GameObject m_DisplayRecipePrefab;

    private void OnEnable()
    {
        RefreshCookBookMenu();
    }

    private void ClearDisplay()
    {
        foreach (Transform child in m_DisplayRecipesParent.transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay()
    {
        for (int i = 0; i < CookController.Instance.getCookBook().recipesSlot.Count; i++)
        {
            if (CookController.Instance.getCookBook().recipesSlot[i] != null)
            {
                GameObject displayedItem = Instantiate(m_DisplayRecipePrefab, m_DisplayRecipesParent.transform);
                displayedItem.GetComponent<DisplayCook>().Load(CookController.Instance.getCookBook().recipesSlot[i]);
                displayedItem.GetComponent<DisplayCook>().setPos(i);
                displayedItem.GetComponent<DisplayCook>().setController(CookController.Instance);
            }
        }
    }

    public void RefreshCookBookMenu()
    {
        ClearDisplay();
        FillDisplay();
    }

    public void cookBookVisible()
    {
        CookController.Instance.cookBookVisible();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitFarm : MonoBehaviour
{
    [SerializeField] private GranjaInfo farm;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        farm.Save();
        collision.transform.position = new Vector3(2, 2, 0);
        TimeManager.Instance.TimeStamp();
        SceneController.Instance.ChangeScene(SceneController.SCENES.Map);
    }
}

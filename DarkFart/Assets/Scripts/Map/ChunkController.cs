using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkController : MonoBehaviour
{
    public static ChunkController Instance { get; internal set; }
    [SerializeField] private List<Chunk> chunks = new List<Chunk>();
    [SerializeField] private Dictionary<Chunk, DictionaryValue> neighbours = new Dictionary<Chunk, DictionaryValue>();
    [SerializeField] private MovimentPlayer player;
    private float seconds;
    private int ChunkWidth;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }

        player = FindObjectOfType<MovimentPlayer>();
    }

    public void ChunksCreated()
    {
        ChunkWidth = Chunk.width;
        seconds = (ChunkWidth / (player.Speed * player.Sprint)) * 0.85f;
        DetermineNeighbours();
    }
    public void ChunksFilled()
    {
        StartCoroutine(CheckChunks());
    }
    private void DetermineNeighbours()
    {
        foreach (Chunk chunk in chunks) 
        {
            DictionaryValue value = new DictionaryValue();
            foreach(Chunk chunkNeigh in chunks)
            {
                if (chunk == chunkNeigh)
                    continue;
                else if (Math.Abs(chunk.fila - chunkNeigh.fila) <= 1 && Math.Abs(chunk.col - chunkNeigh.col) <= 1)
                    value.neighbours.Add(chunkNeigh);
                else
                    value.notNeighbours.Add(chunkNeigh);
            }
            neighbours.Add(chunk, value);
        }
    }

    private IEnumerator CheckChunks()
    {
        while (true)
        {
            Vector3 pos = player.transform.position;

            foreach (Chunk chunk in chunks)
            {
                if(chunk.pos.x < pos.x && chunk.pos.y < pos.y && chunk.pos.x + ChunkWidth > pos.x && chunk.pos.y + ChunkWidth > pos.y)
                {
                    if(neighbours.TryGetValue(chunk, out DictionaryValue value))
                    {
                        EnableChunks(value.neighbours);
                        DisableChunks(value.notNeighbours);
                    }
                    break;
                }
            }

            yield return new WaitForSeconds(seconds);
        }
    }

    private void DisableChunks(List<Chunk> notNeighbours)
    {
        foreach (Chunk chunk in notNeighbours)
        {
            if (!chunk.active) continue;

            List<GameObject> toDestroy = new List<GameObject>();

            foreach (GameObject g in chunk.objects)
            {
                if(g != null)
                {
                    if (g.transform.position.x >= chunk.pos.x && g.transform.position.y >= chunk.pos.y && g.transform.position.x <= chunk.pos.x + ChunkWidth && g.transform.position.y <= chunk.pos.y + ChunkWidth)
                        g.SetActive(false);
                    else
                    {
                        toDestroy.Add(g);
                        foreach ( Chunk c in chunks)
                        {
                            if(g.transform.position.x >= c.pos.x && g.transform.position.y >= c.pos.y && g.transform.position.x <= c.pos.x + ChunkWidth && g.transform.position.y <= c.pos.y + ChunkWidth)
                            {
                                c.objects.Add(g);
                                g.SetActive(c.active);
                                break;
                            }

                        }
                    }

                }
            }
            
            foreach (GameObject g in toDestroy)
            {
                chunk.objects.Remove(g);
            }
            toDestroy.Clear();

            chunk.active = false;
        }
    }

    private void EnableChunks(List<Chunk> neighbours)
    {
        foreach (Chunk chunk in neighbours) 
        {
            if (chunk.active) continue;

            foreach (GameObject g in chunk.objects)
            {
                if (g != null)
                    g.SetActive(true);
            }
            chunk.active = true;
        }
    }

    public void Clear()
    {
        chunks.Clear();
    }
    public void Add(Chunk c)
    {
        chunks.Add(c);
    }

    public void InsertIntoChunk(GameObject g)
    {
        foreach (Chunk chunk in chunks)
        {
            if (g.transform.position.x >= chunk.pos.x && g.transform.position.y >= chunk.pos.y && g.transform.position.x < chunk.pos.x + ChunkWidth && g.transform.position.y < chunk.pos.y + ChunkWidth)
            {
                chunk.objects.Add(g);
                break;
            }
        }
    }
}

[Serializable]
public class Chunk
{
    public int id;
    public int fila;
    public int col;
    public static int width;
    public Vector3 pos;
    public List<GameObject> objects = new List<GameObject>();
    public bool active;

    public Chunk(int i, Vector3 p, int f, int c)
    {
        id = i;
        pos = p;
        fila = f;
        col = c;
        active = true;
    }
}

class DictionaryValue
{
    public List<Chunk> neighbours = new List<Chunk>();
    public List<Chunk> notNeighbours = new List<Chunk>();

    public DictionaryValue() { }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimalesChangeScene : MonoBehaviour
{
    [SerializeField]
    private InteractPlayer player;

    [SerializeField]
    private Animal animal;

    [SerializeField] private GameObject[] prefab;

    public void setAnimal(Animal a)
    {
        animal = a;
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (player.Cuerda && animal != null)
        {
            Debug.Log("Animal contigo mamahuevo");
            GameObject g = null;
            if (animal is NarizCosa)
                g = Instantiate(prefab[1]);
            else if (animal is VacaZombie)
                g = Instantiate(prefab[2]);
            else
                g = Instantiate(prefab[0]);

            g.GetComponent<AnimalController>().Inicia(new Vector3(0, 0), animal);
        }
    }
}
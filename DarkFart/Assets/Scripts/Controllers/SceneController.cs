using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static SceneController Instance { get; internal set; }
    public enum SCENES { Map, Granja, Initial, City1, Menu, Muerte }
    private SCENES actualScene;
    public SCENES ActualScene { get { return actualScene; } }
    [SerializeField]
    private TimeManager timeManager;
    [SerializeField]
    private TilemapController tilemapController;
    [SerializeField]
    private guiController canvas;
    [SerializeField]
    private GameObject pantallaCarga;
    [SerializeField] private MovimentPlayer player;
    [SerializeField]
    private AnimalesChangeScene animalChange;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }

        SceneManager.sceneLoaded += timeManager.OnSceneLoaded;
        SceneManager.sceneLoaded += tilemapController.OnSceneLoaded;
        SceneManager.sceneLoaded += player.OnSceneLoaded;
        SceneManager.sceneLoaded += canvas.OnSceneLoaded;
        SceneManager.sceneLoaded += animalChange.OnSceneLoaded;
        SceneManager.sceneLoaded += OnSceneLoaded;

        //SaveData.Instance.Load();

        ChangeScene(SCENES.Menu);
    }

    public void startAll()
    {
        canvas.gameObject.SetActive(true);
        player.enabled = true;
        player.GetComponent<InteractPlayer>().enabled = true;
        timeManager.startAll();
        player.startAll();
    }

    public void activeCanvas()
    {
        canvas.gameObject.SetActive(true);
    }

    public void ChangeScene(SCENES scene)
    {
        pantallaCarga.SetActive(true);
        Wait();
        actualScene = scene;
        SceneManager.LoadScene(scene.ToString());
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= timeManager.OnSceneLoaded;
        SceneManager.sceneLoaded -= tilemapController.OnSceneLoaded;
        SceneManager.sceneLoaded -= player.OnSceneLoaded;
        SceneManager.sceneLoaded -= canvas.OnSceneLoaded;
        SceneManager.sceneLoaded -= animalChange.OnSceneLoaded;
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public SCENES FromStringToScene(string scene)
    {
        if (scene == SCENES.Granja.ToString())
            return SCENES.Granja;
        else if (scene == SCENES.Map.ToString())
            return SCENES.Map;
        else if (scene == SCENES.City1.ToString())
            return SCENES.City1;
        else
            return SCENES.Granja;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            Debug.developerConsoleVisible = false;
        }
        if (Input.GetKeyDown(KeyCode.Insert))
        {
            Debug.developerConsoleVisible = true;
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        pantallaCarga.SetActive(false);
        player.GetComponent<InteractPlayer>().enabled = true;
        player.GetComponent<InputController>().EnableHand();
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.1f);
    }
}

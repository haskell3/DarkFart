using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftController : MonoBehaviour
{
    public static CraftController Instance { get; internal set; }
    [Header("GUI")]
    [SerializeField]
    private DisplayCraftBook m_DisplayCraftBook;
    [SerializeField]
    private DisplayCraftRecipe m_DisplayCraftRecipe;
    [Header("Craft")]
    [SerializeField]
    private CraftBook m_recetas;
    [SerializeField]
    private InputController inputController;


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        inputController = FindAnyObjectByType<InputController>();
    }

    public void loadDisplayCraftItem(int pos, bool isNewCraft)
    {
        //Debug.Log(pos);
        m_recetas.GetCraftSlot(pos).newCraft = isNewCraft;
        m_DisplayCraftRecipe.Load(m_recetas.GetCraftSlot(pos));
    }

    public void craftBookVisible()
    {
        m_DisplayCraftBook.gameObject.SetActive(!m_DisplayCraftBook.gameObject.activeInHierarchy);
        if (m_DisplayCraftBook.gameObject.activeSelf == true)
            inputController.DisableDefault();
        else
            inputController.EnableDefault();
    }

    public DisplayCraftBook getDisplayCraft()
    {
        return m_DisplayCraftBook;
    }

    public void setDisplayCraft(DisplayCraftBook displayCrafteos)
    {
        m_DisplayCraftBook = displayCrafteos;
    }

    public CraftBook getCraftBook()
    {
        return m_recetas;
    }

    public void Craft(Item item)
    {
        InventarioController.Instance.AddItem(item);
    }
}

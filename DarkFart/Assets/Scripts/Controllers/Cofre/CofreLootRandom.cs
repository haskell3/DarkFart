using System.Collections.Generic;
using UnityEngine;
using static Backpack;

public class CofreLootRandom : MonoBehaviour
{
    [Header("OpenInAnyTime?")]
    [SerializeField]
    private bool m_open = false;
    [Header("ScriptableObject")]
    [SerializeField]
    private LootRandom m_lootRandom;
    [Header("List and Quantity")]
    [Tooltip("Esta parte solo funciona si no tiene SO")]
    [SerializeField]
    private List<ItemSlot> m_loot = new List<ItemSlot>();
    [SerializeField]
    private int m_quantityLootSpawned;
    [SerializeField]
    private int m_minquantityLootSpawned;

    [SerializeField]
    private List<ItemSlot> lootGenerado = new List<ItemSlot>();

    /*
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            createLootRandom();
        }
    }*/

    public List<ItemSlot> createLootRandom()
    {
        if (!m_open)
        {
            m_open = true;
            if (m_lootRandom != null)
            {
                int random = Random.Range(m_lootRandom.minQuantityLootSpawned, m_lootRandom.QuantityLootSpawned);
                return lootRandom(m_lootRandom.Loot, random);
            }
            else
            {
                int random = Random.Range(m_minquantityLootSpawned, m_quantityLootSpawned);
                return lootRandom(m_loot, random);
            }
        }
        return null;
    }

    private List<ItemSlot> lootRandom(List<ItemSlot> items, int iterations)
    {
        lootGenerado.Clear();
        for (int i = 0; i < iterations; i++)
        {
            int random = Random.Range(0, items.Count);
            lootGenerado.Add(items[random]);
        }
        return lootGenerado;
    }
}

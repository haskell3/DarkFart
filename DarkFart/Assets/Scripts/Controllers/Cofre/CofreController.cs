using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XInput;
using static Backpack;

public class CofreController : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private ItemSlot[] m_actualInventory = new ItemSlot[36];
    [Header("GUI")]
    [SerializeField]
    private DisplayBackpack m_DisplayBackpack;
    [SerializeField]
    private DisplayCofre m_DisplayCofre;
    [SerializeField]
    private GameObject m_ObjectMove;
    [SerializeField]
    private GameObject m_cofreDisplay;
    [Header("Input")]
    [SerializeField]
    private InputController _controller;
    private TagDrop drop;

    public static CofreController Instance { get; internal set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        _controller = FindAnyObjectByType<InputController>();
    }

    public void Refresh()
    {
        if (m_DisplayBackpack.gameObject.activeSelf == true)
            m_DisplayBackpack.RefreshBackpack();
        if (m_DisplayCofre.gameObject.activeSelf == true)
            m_DisplayCofre.RefreshBackpack();
    }

    public ItemSlot[] getInventory()
    {
        return m_actualInventory;
    }

    public List<Object> getObjectInPos(int pos)
    {
        List<Object> displayItems = new List<Object>();
        foreach (Object item in FindObjectsOfType(typeof(DropAnotherInventory)))
        {
            if (item.GetComponent<DisplayItem>().getPosBackpack() == pos)
            {
                displayItems.Add(item);
            }
        }
        return displayItems;
    }

    public void changeCofreItem(int newPos, int oldPos)
    {
        /*
        if (m_ItemSlots2[newPos].Stack(m_ItemSlots2[oldPos]))
            return;
        */

        ItemSlot newSlot = m_actualInventory[newPos];
        m_actualInventory[newPos] = m_actualInventory[oldPos];
        if (newSlot != null)
            m_actualInventory[oldPos] = newSlot;
        else
            m_actualInventory[oldPos] = null;
    }

    public void setInventory(ItemSlot[] inventory)
    {
        m_actualInventory = inventory;
        Refresh();
    }

    public DisplayBackpack getDisplayBackpack()
    {
        return m_DisplayBackpack;
    }

    public void setDisplayBackpack(DisplayBackpack displayBackpack)
    {
        m_DisplayBackpack = displayBackpack;
    }

    public DisplayCofre getDisplayCofre()
    {
        return m_DisplayCofre;
    }

    public void setDisplayCofre(DisplayCofre displayHotbar)
    {
        m_DisplayCofre = displayHotbar;
    }

    public GameObject getObjectMove()
    {
        return m_ObjectMove;
    }

    public GameObject getHotbartMove()
    {
        return m_ObjectMove;
    }

    public Item selectedItem(int pos)
    {
        pos -= 1;
        DisplayItem[] hotbar = m_DisplayCofre.GetComponentsInChildren<DisplayItem>();
        Item itemSelected = null;
        foreach (DisplayItem item in hotbar)
        {
            if (pos == item.getPosBackpack())
            {
                item.getSelected().gameObject.SetActive(true);
                itemSelected = item.GetItem();
            }
            else
                item.getSelected().gameObject.SetActive(false);
        }
        return itemSelected;
    }

    public void cofreVisible()
    {
        if (drop == null)
            drop = FindAnyObjectByType<TagDrop>(FindObjectsInactive.Include);
        if (drop != null)
            drop.gameObject.SetActive(true);
        m_cofreDisplay.gameObject.SetActive(!m_cofreDisplay.gameObject.activeInHierarchy);
        if (m_cofreDisplay.gameObject.activeSelf == true)
        {
            _controller.DisableDefault();
            if (guiController.Instance != null)
                guiController.Instance.disableAll();
            
            drop.gameObject.SetActive(false);
        }
        else
        {
            _controller.EnableDefault();
            _controller.EnableHand();
            if (guiController.Instance != null)
                guiController.Instance.enableAll();
            if(drop != null)
                drop.gameObject.SetActive(true);
        }
    }
}

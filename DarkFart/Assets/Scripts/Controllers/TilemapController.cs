using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class TilemapController : MonoBehaviour
{
    [SerializeField] private Tilemap m_PlantasTilemap;
    private static Tilemap plantasTilemap;
    public static Tilemap PlantasTilemap { get { return plantasTilemap; } }
    [SerializeField] private GranjaInfo granjaInfo;
    private void Awake()
    {
        plantasTilemap = m_PlantasTilemap;

        //The next line of code it's to taste the first time each day because the pc's are frozen
        //granjaInfo.Save();
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        plantasTilemap = FindFirstObjectByType<TagFloor>()?.gameObject.GetComponent<Tilemap>();
    }
}

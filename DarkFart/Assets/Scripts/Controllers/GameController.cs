using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour { 
    public static GameController Instance { get; internal set; }
    [SerializeField]
    private GameObject m_FishingMinigame;
    [SerializeField]
    private InventarioController m_InventarioController;
    [SerializeField]
    private InputController m_InputController;
    [SerializeField]
    private Arma[] weapons;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }

        ActivateWeapons();
    }

    public void ActivateWeapons()
    {
        foreach (Arma weapon in weapons)
        {
            weapon.isAble = true;
        }
    }

    public void StartFishing(Pez p)
    {
        m_InputController.ChangeToPesca();
        m_FishingMinigame.SetActive(true);
        m_FishingMinigame.GetComponent<FishingMinigame>().FishInfo = p;
        m_FishingMinigame.GetComponent<FishingMinigame>().Ini();
    }

    public void StopFishing()
    {
        m_InputController.ChangeToDefault();
        m_FishingMinigame.GetComponent<FishingMinigame>().StopAllCoroutines();
        m_FishingMinigame.SetActive(false);
        InventarioController.Instance.Refresh();
    }

    public void StartDialogue(NPCController c)
    {
        m_InputController.DisableDefault();
        c.StopWalking();
    }

    public void StopDialogue(NPCController c)
    {
        m_InputController.EnableDefault();
        c.ChangeToRandomWalk();
    }
}

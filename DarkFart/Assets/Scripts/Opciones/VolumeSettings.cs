using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeSettings : MonoBehaviour
{
    [SerializeField] private AudioMixer myMixer;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider sfxSlider;

    public void SetMusicVolume()
    {
        float volume = musicSlider.value;
        myMixer.SetFloat("music", Mathf.Log10(volume) * 20);
    }
    public void SetSfxVolume()
    {
        float volume = sfxSlider.value;
        myMixer.SetFloat("sfx", Mathf.Log10(volume) * 20);
    }

    public void LoadMusicVolume(float volume)
    {
        myMixer.SetFloat("music", Mathf.Log10(volume) * 20);
    }
    public void LoadSfxVolume(float volume)
    {
        myMixer.SetFloat("sfx", Mathf.Log10(volume) * 20);
    }

    public void Sliders()
    {
        float music, sfx;
        myMixer.GetFloat("music", out music);
        myMixer.GetFloat("sfx", out sfx);
        musicSlider.value = music;
        sfxSlider.value = sfx;
    }
}

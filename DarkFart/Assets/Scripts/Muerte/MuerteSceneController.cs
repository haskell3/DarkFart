using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuerteSceneController : MonoBehaviour
{
    public void despertar()
    {
        GameObject player = FindAnyObjectByType<MovimentPlayer>().gameObject;
        player.GetComponent<HambreController>().resetHambre();
        player.GetComponent<EnergiaController>().resetEnergia();
        player.GetComponent<VidaController>().resetHp();
        TimeManager.Instance.startAll();
        SceneController.Instance.ChangeScene(SceneController.SCENES.Granja);
        SceneController.Instance.activeCanvas();
        player.transform.position = new Vector3(-7.5f, 130f, 0);
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Game is exiting");
    }
}

using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using static SaveData;
using static Backpack;
using Unity.Mathematics;
using UnityEngine.Tilemaps;

public class SaveData : MonoBehaviour
{
    private string saveFileNameFarm = Application.dataPath + "/Resources/farmInfo.json";
    private string saveFileNameMap = Application.dataPath + "/Resources/mapInfo.json";
    private string saveFileNameBackpack = Application.dataPath + "/Resources/backpack.json";
    private string saveFileNamePlayer = Application.dataPath + "/Resources/player.json";
    private string saveFileNameScene = Application.dataPath + "/Resources/scene.json";
    private string saveFileNameTime = Application.dataPath + "/Resources/timeManager.json";
    [SerializeField] private GranjaInfo GranjaInfo;
    [SerializeField] private MapInfo MapInfo;
    [SerializeField] private Backpack Backpack;
    [SerializeField] private GameObject playerObject;
    [SerializeField] private Item[] m_Everything;
    [SerializeField] private Animal[] m_Animals;
    [SerializeField] private Enemy[] m_Enemies;
    [SerializeField] private PersonalidadInfo[] m_Personalities;
    [SerializeField] private Sprite[] m_Sprites;
    [SerializeField] private Tile[] m_Tiles;
    private Dictionary<string, Item> m_Objects;
    private Dictionary<string, Animal> m_AnimalsDic;
    private Dictionary<string, Enemy> m_EnemiesDic;
    private Dictionary<string, PersonalidadInfo> m_PersonalitiesDic;
    private Dictionary<string, Sprite> m_SpritesDic;
    private Dictionary<string, Tile> m_TilesDic;
    private VidaController vida;
    private EnergiaController energiaController;
    private HambreController hambreController;
    public static SaveData Instance { get; internal set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        vida = playerObject.GetComponent<VidaController>();
        energiaController = playerObject.GetComponent<EnergiaController>();
        hambreController = playerObject.GetComponent<HambreController>();
        m_Objects = new Dictionary<string, Item>();
        m_AnimalsDic = new Dictionary<string, Animal>();
        m_EnemiesDic = new Dictionary<string, Enemy>();
        m_SpritesDic = new Dictionary<string, Sprite>();
        m_TilesDic = new Dictionary<string, Tile>();
        m_PersonalitiesDic = new Dictionary<string, PersonalidadInfo>();
        foreach(Item obj in m_Everything)
        {
            m_Objects.Add(obj.Name, obj);
        }
        foreach (Animal obj in m_Animals)
        {
            m_AnimalsDic.Add(obj.Name, obj);
        }
        foreach (Enemy obj in m_Enemies)
        {
            m_EnemiesDic.Add(obj.Name, obj);
        }
        foreach(Sprite sprite in m_Sprites)
        {
            m_SpritesDic.Add(sprite.name, sprite);
        }
        foreach (Tile tile in m_Tiles)
        {
            m_TilesDic.Add(tile.name, tile);
        }
        foreach (PersonalidadInfo personalidad in m_Personalities)
        {
            m_PersonalitiesDic.Add(personalidad.name, personalidad);
        }
    }
    public void Save()
    {
        string jsonDataFarm = JsonUtility.ToJson(new FarmInfoSaved(GranjaInfo), true);
        string jsonDataMap = JsonUtility.ToJson(new MapInfoSaved(MapInfo), true);
        string jsonDataBackpack = JsonUtility.ToJson(new BackPackInfo(Backpack));
        string jsonDataPlayer = JsonUtility.ToJson(new PlayerInfo(vida.Hp, vida.MaxHP, energiaController.Energy, energiaController.getMaxEnergia(), hambreController.Hambre, hambreController.MaxHambre, playerObject.transform.position));
        string jsonDataScene = JsonUtility.ToJson(new SceneInfo(SceneController.Instance.ActualScene.ToString()));
        string jsonDataTime = JsonUtility.ToJson(TimeManager.Instance);

        try
        {
            Debug.Log("Saving: ");
            Debug.Log(jsonDataFarm);
            Debug.Log(jsonDataMap);
            Debug.Log(jsonDataBackpack);
            Debug.Log(jsonDataPlayer);
            Debug.Log(jsonDataScene);
            Debug.Log(jsonDataTime);

            File.WriteAllText(saveFileNameFarm, jsonDataFarm);
            File.WriteAllText(saveFileNameMap, jsonDataMap);
            File.WriteAllText(saveFileNameBackpack, jsonDataBackpack);
            File.WriteAllText(saveFileNamePlayer, jsonDataPlayer);
            File.WriteAllText(saveFileNameScene, jsonDataScene);
            File.WriteAllText(saveFileNameTime, jsonDataTime);

        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to save {Path.Combine(Application.persistentDataPath, saveFileNameFarm)} with exception {e}");
        }

    }

    public string Load()
    {
        Debug.Log("Trying to load");
        try
        {
            string jsonDataFarm = File.ReadAllText(saveFileNameFarm);
            string jsonDataMap = File.ReadAllText(saveFileNameMap);
            string jsonDataBackpack = File.ReadAllText(saveFileNameBackpack);
            string jsonDataPlayer = File.ReadAllText(saveFileNamePlayer);
            string jsonDataScene = File.ReadAllText(saveFileNameScene);
            string jsonDataTime = File.ReadAllText(saveFileNameTime);

            PlayerInfo playerInfo = new PlayerInfo();
            BackPackInfo b = new BackPackInfo();
            MapInfoSaved m = new MapInfoSaved();
            FarmInfoSaved f = new FarmInfoSaved();
            SceneInfo sceneInfo = new SceneInfo();

            JsonUtility.FromJsonOverwrite(jsonDataFarm, f);
            JsonUtility.FromJsonOverwrite(jsonDataMap, m);
            JsonUtility.FromJsonOverwrite(jsonDataBackpack, b);
            JsonUtility.FromJsonOverwrite(jsonDataPlayer, playerInfo);
            JsonUtility.FromJsonOverwrite(jsonDataScene, sceneInfo);
            JsonUtility.FromJsonOverwrite(jsonDataTime, TimeManager.Instance);

            vida.MaxHP = playerInfo.maxVida;
            vida.Hp = playerInfo.vida;
            energiaController.Energy = playerInfo.energia;
            energiaController.SetMaxEnergy(playerInfo.maxEnergia);
            hambreController.Hambre = playerInfo.hambre;
            hambreController.MaxHambre = playerInfo.maxHambre;
            playerObject.transform.position = playerInfo.pos;

            ReconstructBackPack(b);
            ReconstructFarm(f);
            ReconstructMap(m);

            Debug.Log("Load completed");
            return sceneInfo.scene;        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to load {Path.Combine(Application.persistentDataPath, saveFileNameFarm)} with exception {e}");
        }
        return null;
    }

    private void ReconstructMap(MapInfoSaved m)
    {
        MapInfo.ClearInfo();
        MapInfo.seed = m.seed;
        foreach (ItemInfoSaved item in m.items)
        {
            ItemInfo i = new ItemInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            if (m_Objects.TryGetValue(item.itemName, out Item value))
                i.item = value;
            i.amount = item.amount;
            MapInfo.items.Add(i);
        }
        foreach (DropeableInfoSaved item in m.resources)
        {
            DropeableInfo i = new DropeableInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.vida = item.vida;
            i.maxVida = item.maxVida;

            foreach (string it in item.itemsName)
            {
                if (m_Objects.TryGetValue(it, out Item value))
                    i.items.Add(value);
            }

            MapInfo.resources.Add(i);
        }
        foreach (AnimalInfoSaved item in m.animals)
        {
            AnimalInfo i = new AnimalInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.vida = item.vida;
            i.maxVida = item.maxVida;
            if (m_AnimalsDic.TryGetValue(item.animalName, out Animal a))
                i.animal = a;
            foreach (string it in item.itemsName)
            {
                if (m_Objects.TryGetValue(it, out Item value))
                    i.items.Add(value);
            }
            for (int x = 0; x < item.spritesNames.Length; x++)
            {
                if (m_SpritesDic.TryGetValue(item.spritesNames[x], out Sprite value))
                    i.sprites[x] = value;
            }

            MapInfo.animals.Add(i);
        }
        foreach (EnemyInfoSaved item in m.enemies)
        {
            EnemyInfo i = new EnemyInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.vida = item.vida;
            i.maxVida = item.maxVida;
            if (m_EnemiesDic.TryGetValue(item.enemyName, out Enemy a))
                i.enemy = a;
            foreach (string it in item.itemsName)
            {
                if (m_Objects.TryGetValue(it, out Item value))
                    i.items.Add(value);
            }

            for (int x = 0; x < item.spritesNames.Length; x++)
            {
                if (m_SpritesDic.TryGetValue(item.spritesNames[x], out Sprite value))
                    i.sprites[x] = value;
            }

            MapInfo.enemies.Add(i);
        }
    }

    private void ReconstructFarm(FarmInfoSaved f)
    {
        GranjaInfo.ClearInfo();
        foreach (ItemInfoSaved item in f.items)
        {
            ItemInfo i = new ItemInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            if (m_Objects.TryGetValue(item.itemName, out Item value))
                i.item = value;
            i.amount = item.amount;
            GranjaInfo.items.Add(i);
        }
        foreach (DropeableInfoSaved item in f.resources)
        {
            DropeableInfo i = new DropeableInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.vida = item.vida;
            i.maxVida = item.maxVida;

            foreach(string it in item.itemsName)
            {
                if (m_Objects.TryGetValue(it, out Item value))
                    i.items.Add(value);
            }

            GranjaInfo.resources.Add(i);
        }
        foreach (AnimalInfoSaved item in f.animals)
        {
            AnimalInfo i = new AnimalInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.vida = item.vida;
            i.maxVida = item.maxVida;
            if(m_AnimalsDic.TryGetValue(item.animalName, out Animal a))
                i.animal = a;
            foreach (string it in item.itemsName)
            {
                if (m_Objects.TryGetValue(it, out Item value))
                    i.items.Add(value);
            }

            for (int x = 0; x < item.spritesNames.Length; x++)
            {
                if(m_SpritesDic.TryGetValue(item.spritesNames[x], out Sprite value))
                    i.sprites[x] = value;
            }

            GranjaInfo.animals.Add(i);
        }
        foreach (EnemyInfoSaved item in f.enemies)
        {
            EnemyInfo i = new EnemyInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.vida = item.vida;
            i.maxVida = item.maxVida;
            if(m_EnemiesDic.TryGetValue(item.enemyName, out Enemy a))
                i.enemy = a;
            foreach (string it in item.itemsName)
            {
                if (m_Objects.TryGetValue(it, out Item value))
                    i.items.Add(value);
            }

            for (int x = 0; x < item.spritesNames.Length; x++)
            {
                if (m_SpritesDic.TryGetValue(item.spritesNames[x], out Sprite value))
                    i.sprites[x] = value;
            }

            GranjaInfo.enemies.Add(i);
        }

        foreach (PlantInfoSaved item in f.plants)
        {
            PlantInfo i = new PlantInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            int i2 = 0;
            foreach(string s in item.spriteNames)
            {
                if(m_SpritesDic.TryGetValue(s, out Sprite sprite1))
                    i.sprites[i2] = sprite1;
                i2++;
            }
            if (m_Objects.TryGetValue(item.itemName, out Item value))
                i.item = value;
            i.isReady = item.isReady;
            i.isDead = item.isDead;
            i.currentDays = item.currentDays;
            i.days = item.days;

            GranjaInfo.plants.Add(i);
        }
        foreach (EstructuraInfoSaved item in f.estructures)
        {
            EstructuraInfo i = new (item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            if (m_Objects.TryGetValue(item.estructuraName, out Item value))
                i.estructura = (Estructura)value;
            i.typeEstructure = item.typeEstructure;

            GranjaInfo.estructures.Add(i);
        }
        foreach (BarreraInfoSaved item in f.barriers)
        {
            BarreraInfo i = new BarreraInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.vida = item.vida;
            i.maxVida = item.maxVida;

            GranjaInfo.barriers.Add(i);
        }
        foreach (CalderoInfoSaved item in f.caldiers)
        {
            CalderoInfo i = new CalderoInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            if (m_Objects.TryGetValue(item.woodInfo.Item, out Item value))
                i.wood = new ItemSlot(value, item.wood.Amount);
            else
                i.wood = new ItemSlot();
            i.water = item.water;
            i.maxWater = item.maxWater;

            GranjaInfo.caldiers.Add(i);
        }
        foreach (CofreInfoSaved item in f.chests)
        {
            CofreInfo i = new CofreInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if (m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.items = new ItemSlot[item.itemsInfo.Length];
            int i2 = 0;
            foreach(ItemSlotInfo slot in item.itemsInfo)
            {
                if (m_Objects.TryGetValue(slot.Item, out Item value))
                    i.items[i2] = new ItemSlot(value, slot.Amount);
                else
                    i.items[i2] = new ItemSlot();
                i2++;
            }

            GranjaInfo.chests.Add(i);
        }
        foreach (NPCInfoSaved item in f.NPCS)
        {
            NPCInfo i = new NPCInfo(item.layer, item.pos, item.sprite, item.type, item.id);
            if(m_SpritesDic.TryGetValue(item.spriteName, out Sprite sprite))
                i.sprite = sprite;
            i.nivelAmistad = item.nivelAmistad;
            i.script = item.script;
            i.name = item.name;
            i.hp = item.hp;
            i.MaxHP = item.MaxHP;
            i.speed = item.speed;
            if(m_PersonalitiesDic.TryGetValue(item.personalidadName, out PersonalidadInfo personalidadInfo))
                i.personalidad = personalidadInfo;
            for (int x = 0; x < item.spritesNames.Length; x++)
            {
                if (m_SpritesDic.TryGetValue(item.spritesNames[x], out Sprite value))
                    i.sprites[x] = value;
            }

            GranjaInfo.NPCS.Add(i);
        }

        Tilemap tilemap = TilemapController.PlantasTilemap;
        GranjaInfo.tiles.Clear();
        for (int i = 0; i < f.tiles.Count(); i++)
        {
            if(m_TilesDic.TryGetValue(f.tiles.GetTile(i), out Tile mapTile))
                GranjaInfo.tiles.Add(f.tiles.GetPos(i), mapTile);
        }
    }

    private void ReconstructBackPack(BackPackInfo b)
    {
        Backpack.ClearBackpack();
        int i = 0;
        foreach(ItemSlotInfo item in b.items)
        {
            if(m_Objects.TryGetValue(item.Item, out Item value))
                Backpack.AddByPos(i, new ItemSlot(value, item.Amount));
            i++;
        }
        if(m_Objects.TryGetValue(b.armadura.Item, out Item arm))
        {
            InventarioController.Instance.getBackpack().setArmadura((Armadura) arm);
            vida.Armadura = (Armadura) arm;
        }
        if(m_Objects.TryGetValue(b.secondhand.Item, out Item sh))
            InventarioController.Instance.getBackpack().SetSecondaryHand(sh);
        InventarioController.Instance.Refresh();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Y))
        {
            Load();
        }
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            Save();
        }
    }
}
[Serializable]
class PlayerInfo
{
    public float vida;
    public float maxVida;

    public float energia;
    public float maxEnergia;

    public float hambre;
    public float maxHambre;

    public Vector3 pos;

    public PlayerInfo()
    {

    }

    public PlayerInfo(float v, float mV, float e, float mE, float h, float mH, Vector3 pos)
    {
        vida = v;
        maxVida = mV;
        energia = e;
        maxEnergia = mE;
        hambre = h;
        maxHambre = mH;
        this.pos = pos;
    }
}

[Serializable]
class BackPackInfo
{
    public ItemSlotInfo[] items = new ItemSlotInfo[36];
    public ItemSlotInfo secondhand;
    public ItemSlotInfo armadura;
    
    public BackPackInfo(Backpack b)
    {
        int i = 0;
        foreach(ItemSlot item in b.ItemSlots2)
        {
            if(item.Item != null)
                items[i] = new ItemSlotInfo(item.Item.Name, item.Amount);
            i++;
        }
        armadura = new ItemSlotInfo(b.GetArmadura().Item?.Name, b.GetArmadura().Amount);
        secondhand = new ItemSlotInfo(b.GetSecondaryHand().Item?.Name, b.GetSecondaryHand().Amount);
    }

    public BackPackInfo() { }
}

[Serializable]
public class ItemSlotInfo
{
    [SerializeField]
    public string Item;
    [SerializeField]
    public int Amount;

    public ItemSlotInfo()
    {
        Item = "";
        Amount = 1;
    }

    public ItemSlotInfo(string obj)
    {
        Item = obj;
        Amount = 1;
    }
    public ItemSlotInfo(string obj, int amount)
    {
        Item = obj;
        Amount = amount;
    }

    public ItemSlotInfo(ItemSlot slot)
    {
        if (slot.Item != null)
            Item = slot.Item.Name;
        else
            Item = "";
        Amount = slot.Amount;
    }

}

class FarmInfoSaved
{
    public List<ItemInfoSaved> items = new List<ItemInfoSaved>();
    public List<DropeableInfoSaved> resources = new List<DropeableInfoSaved>();
    public List<AnimalInfoSaved> animals = new List<AnimalInfoSaved>();
    public List<EnemyInfoSaved> enemies = new List<EnemyInfoSaved>();
    public List<PlantInfoSaved> plants = new List<PlantInfoSaved>();
    public List<EstructuraInfoSaved> estructures = new List<EstructuraInfoSaved>();
    public List<BarreraInfoSaved> barriers = new List<BarreraInfoSaved>();
    public List<CalderoInfoSaved> caldiers = new List<CalderoInfoSaved>();
    public List<CofreInfoSaved> chests = new List<CofreInfoSaved>();
    public List<NPCInfoSaved> NPCS = new List<NPCInfoSaved>();
    public DictionarySerSaved tiles = new DictionarySerSaved();

    public FarmInfoSaved() { }
    public FarmInfoSaved(GranjaInfo saved) 
    {
        foreach (ItemInfo item in saved.items)
        {
            ItemInfoSaved i = new ItemInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            items.Add(i);
        }
        foreach (DropeableInfo item in saved.resources)
        {
            DropeableInfoSaved i = new DropeableInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            resources.Add(i);
        }
        foreach (AnimalInfo item in saved.animals)
        {
            AnimalInfoSaved i = new AnimalInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            animals.Add(i);
        }
        foreach (EnemyInfo item in saved.enemies)
        {
            EnemyInfoSaved i = new EnemyInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            enemies.Add(i);
        }

        foreach (PlantInfo item in saved.plants)
        {
            PlantInfoSaved i = new PlantInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            plants.Add(i);
        }
        foreach (EstructuraInfo item in saved.estructures)
        {
            EstructuraInfoSaved i = new EstructuraInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            estructures.Add(i);
        }
        foreach (BarreraInfo item in saved.barriers)
        {
            BarreraInfoSaved i = new BarreraInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            barriers.Add(i);
        }
        foreach (CalderoInfo item in saved.caldiers)
        {
            CalderoInfoSaved i = new CalderoInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            caldiers.Add(i);
        }
        foreach (CofreInfo item in saved.chests)
        {
            CofreInfoSaved i = new CofreInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            chests.Add(i);
        }
        foreach (NPCInfo item in saved.NPCS)
        {
            NPCInfoSaved i = new NPCInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            NPCS.Add(i);
        }
        for(int i = 0; i < saved.tiles.Count(); i++)
        {
            tiles.Add(saved.tiles.GetPos(i), saved.tiles.GetTile(i).name);
        }
    }

}

class MapInfoSaved
{
    public List<ItemInfoSaved> items = new List<ItemInfoSaved>();
    public List<DropeableInfoSaved> resources = new List<DropeableInfoSaved>();
    public List<AnimalInfoSaved> animals = new List<AnimalInfoSaved>();
    public List<EnemyInfoSaved> enemies = new List<EnemyInfoSaved>();
    public Vector2 seed;

    public MapInfoSaved() { }
    public MapInfoSaved(MapInfo saved)
    {
        foreach(ItemInfo item in saved.items)
        {
            ItemInfoSaved i = new ItemInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            items.Add(i);
        }
        foreach (DropeableInfo item in saved.resources)
        {
            DropeableInfoSaved i = new DropeableInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            resources.Add(i);
        }
        foreach (AnimalInfo item in saved.animals)
        {
            AnimalInfoSaved i = new AnimalInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            animals.Add(i);
        }
        foreach (EnemyInfo item in saved.enemies)
        {
            EnemyInfoSaved i = new EnemyInfoSaved(0, Vector3.zero, null, GameObjectsInfo.typeOfGameObject.Item, 0);
            i.Create(item);
            enemies.Add(i);
        }
        seed = saved.seed;
    }
}

[Serializable]
public class NPCInfoSaved : NPCInfo
{
    public string personalidadName;
    public string spriteName;
    public string[] spritesNames = new string[3];

    public NPCInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(NPCInfo npc)
    {
        layer = npc.layer;
        pos = npc.pos;
        type = npc.type;
        id = npc.id;
        nivelAmistad = npc.nivelAmistad;
        script = npc.script;
        name = npc.name;
        hp = npc.hp;
        MaxHP = npc.MaxHP;
        speed = npc.speed;
        personalidadName = npc.personalidad.name;
        spriteName = npc.sprite.name;
        for (int i = 0; i < spritesNames.Length; i++)
        {
            spritesNames[i] = npc.sprites[i].name;
        }
    }

}

[Serializable]
public class EstructuraInfoSaved : EstructuraInfo
{
    public string estructuraName;
    public string spriteName;

    public EstructuraInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(EstructuraInfo estructuraInfo)
    {
        layer = estructuraInfo.layer;
        pos = estructuraInfo.pos;
        type = estructuraInfo.type;
        id = estructuraInfo.id;

        estructuraName = estructuraInfo.estructura.name;
        typeEstructure = estructuraInfo.typeEstructure;
        spriteName = estructuraInfo.sprite.name;
    }
}

[Serializable]
public class BarreraInfoSaved : BarreraInfo
{
    public string spriteName;
    public BarreraInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(BarreraInfo barreraInfo)
    {
        layer = barreraInfo.layer;
        pos = barreraInfo.pos;
        type = barreraInfo.type;
        id = barreraInfo.id;

        vida = barreraInfo.vida;
        maxVida = barreraInfo.maxVida;
        spriteName = barreraInfo.sprite.name;
    }
}

[Serializable]
public class CofreInfoSaved : CofreInfo
{
    public ItemSlotInfo[] itemsInfo;
    public string spriteName;
    public CofreInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(CofreInfo cofreInfo) 
    {
        layer = cofreInfo.layer;
        pos = cofreInfo.pos;
        type = cofreInfo.type;
        id = cofreInfo.id;
        itemsInfo = new ItemSlotInfo[cofreInfo.items.Length];
        int i = 0;
        foreach(ItemSlot slot in cofreInfo.items)
        {
            itemsInfo[i] = new ItemSlotInfo(slot);
            i++;
        }
        spriteName = cofreInfo.sprite.name;
    }
}

[Serializable]
public class CalderoInfoSaved : CalderoInfo
{
    public ItemSlotInfo woodInfo;
    public string spriteName;
    public CalderoInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(CalderoInfo cofreInfo)
    {
        layer = cofreInfo.layer;
        pos = cofreInfo.pos;
        type = cofreInfo.type;
        id = cofreInfo.id;

        water = cofreInfo.water;
        maxWater = cofreInfo.maxWater;
        woodInfo = new ItemSlotInfo(cofreInfo.wood);
        spriteName = cofreInfo.sprite.name;
    }
}

[Serializable]
public class ItemInfoSaved : ItemInfo
{
    public string itemName;
    public string spriteName;

    public ItemInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(ItemInfo item) 
    {
        layer = item.layer;
        pos = item.pos;
        type = item.type;
        id = item.id;

        amount = item.amount;
        itemName = item.item.Name;
        spriteName = item.sprite.name;
    }
}
[Serializable]
public class PlantInfoSaved : PlantInfo
{
    public string[] spriteNames;
    public string spriteName;
    public string itemName;
    public PlantInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(PlantInfo plant) 
    {
        layer = plant.layer;
        pos = plant.pos;
        type = plant.type;
        id = plant.id;


        isReady = plant.isReady;
        isDead = plant.isDead;
        currentDays = plant.currentDays;
        days = plant.days;  
        itemName = plant.item.Name;
        spriteNames = new string[plant.sprites.Length];
        int i = 0;
        foreach(Sprite s in plant.sprites)
        {
            spriteNames[i] = s.name;
            i++;
        }
        spriteName = plant.sprite.name;
    }
}

[Serializable]
public class DropeableInfoSaved : DropeableInfo
{
    public List<string> itemsName = new List<string>();
    public string spriteName;

    public DropeableInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(DropeableInfo info)
    {
        layer = info.layer;
        pos = info.pos;
        type = info.type;
        id = info.id;

        vida = info.vida;
        maxVida = info.maxVida;
        foreach(Item i  in info.items)
        {
            itemsName.Add(i.name);
        }
        spriteName = info.sprite.name;
    }
}
[Serializable]
public class AnimalInfoSaved : AnimalInfo
{
    public string animalName;
    public List<string> itemsName = new List<string>();
    public string spriteName;
    public string[] spritesNames = new string[3];

    public AnimalInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(AnimalInfo info)
    {
        layer = info.layer;
        pos = info.pos;
        type= info.type;
        id = info.id;

        animalName = info.animal.Name;
        foreach (Item i in info.items)
        {
            itemsName.Add(i.name);
        }
        for (int i = 0; i < spritesNames.Length; i++)
        {
            spritesNames[i] = info.sprites[i].name;
        }
        spriteName = info.sprite.name;
    }
}
[Serializable]
public class EnemyInfoSaved : EnemyInfo
{
    public string enemyName;
    public List<string> itemsName = new List<string>();
    public string spriteName;
    public string[] spritesNames = new string[3];

    public EnemyInfoSaved(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }

    public void Create(EnemyInfo info)
    {
        layer = info.layer;
        pos = info.pos;
        type = info.type;
        id = info.id;

        enemyName = info.enemy.Name;
        foreach (Item i in info.items)
        {
            itemsName.Add(i.name);
        }
        for (int i = 0; i < spritesNames.Length; i++)
        {
            spritesNames[i] = info.sprites[i].name;
        }
        spriteName = info.sprite.name;
    }
}

[Serializable]
public class DictionarySerSaved
{
    [SerializeField] private List<string> tiles = new List<string>();
    [SerializeField] private List<Vector3> target = new List<Vector3>();

    public void Add(Vector3 v, string t)
    {
        tiles.Add(t);
        target.Add(v);
    }

    public string GetTile(int i)
    {
        if (tiles.Count <= i)
            Debug.LogError("Index Out Of Bounds");
        return tiles[i];
    }
    public Vector3 GetPos(int i)
    {
        if (i >= target.Count)
            Debug.LogError("Index Out Of Bounds");
        return target[i];
    }

    public int Count()
    {
        if (tiles.Count != target.Count)
            Debug.LogError("Ha fallado el diccionario");

        return tiles.Count;
    }

    public void Clear()
    {
        tiles.Clear();
        target.Clear();
    }
}

public class SceneInfo
{
    public string scene;

    public SceneInfo(string scene)
    {
        this.scene = scene;
    }

    public SceneInfo()
    {
        this.scene = null;
    }
}


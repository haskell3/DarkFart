using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.XR;
using static Llave;

public class ManoloController : MonoBehaviour, Interactuable
{
    [SerializeField] private bool firstTalk = true;
    [SerializeField] private TextAsset firstDialogue;
    [SerializeField] private List<TextAsset> helpDialogues;
    [SerializeField] private List<Llave> llaves = new List<Llave>();
    public List<Llave> Llaves { get { return llaves; } set { llaves = value; } }
    [SerializeField] private TextAsset carneDialogue;
    
    public void Interact()
    {
        if (firstTalk)
        {
            firstTalk = false;
            DialogueManager.Instance.EnterDialogueMode(firstDialogue, "Manolo");
        } else
        {
            int r = Random.Range(0, helpDialogues.Count);
            DialogueManager.Instance.EnterDialogueMode(helpDialogues[r], "Manolo");
        }
    }

    public void ActivarDialogoLlave(Llave l)
    {
        if (l.Tipo == Llave.TipoLlave.Carne)
        {
            DialogueManager.Instance.EnterDialogueMode(carneDialogue, "Manolo");
        }
    }
}

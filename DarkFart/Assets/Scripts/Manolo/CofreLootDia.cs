using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Backpack;

public class CofreLootDia : MonoBehaviour
{
    [SerializeField] private int daysForLoot;
    [SerializeField] private int daysForLootLeft;
    [SerializeField] private Item[] items;
    [SerializeField] private int quantity;
    [SerializeField] private ItemSlot[] cofreItems = new ItemSlot[36];
    
    public void CheckDay()
    {
        --daysForLootLeft;
        if (daysForLootLeft <= 0)
        {
            int random = Random.Range(0, items.Length);
            cofreItems = GetComponent<CofreBehaviour>().ItemSlots;
            AddItem(items[random]);
            GetComponent<CofreBehaviour>().ItemSlots = cofreItems;
            daysForLootLeft = daysForLoot;
            /*
            cofreItems = GetComponent<CofreBehaviour>().ItemSlots;
            int islot = GetSlot(cofreItems);
            cofreItems[islot].Item = item;
            cofreItems[islot].Amount += quantity;
            for (int i = islot; i < cofreItems.Length; i++)
            {
                if (cofreItems[i].Amount > 99)
                {
                    if (islot < cofreItems.Length - 1)
                    {
                        int sum = cofreItems[islot].Amount - 99;
                        cofreItems[islot + 1].Amount = sum;
                        cofreItems[islot + 1].Item = item;
                        cofreItems[islot].Amount = 99;
                    }
                }
                else break;
            }
            
            GetComponent<CofreBehaviour>().ItemSlots = cofreItems;
            */
        }
        
    }

    public int GetSlot(ItemSlot[] items)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].Amount < 99) return i;
        }
        return 0;
    }

    public void AddItem(Item item)
    {
        for (int i = 0; i < cofreItems.Length; i++)
        {
            if (cofreItems[i].Item == item)
            {
                if(cofreItems[i].Amount < (99 - quantity))
                {
                    cofreItems[i].Amount += quantity;
                    return;
                }
            }
            else if(cofreItems[i].Item == null)
            {
                cofreItems[i].Item = item;
                cofreItems[i].Amount = 1;
                return;
            }
        }
    }


}

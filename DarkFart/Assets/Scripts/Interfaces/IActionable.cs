using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public interface IActionable
{
    public void Action(Vector2 position, Vector2 direction, Tilemap t);
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomWalkEnemy : MonoBehaviour
{
    private NavMeshAgent m_NavMeshAgent;
    private float displacementDist = 2f;
    [SerializeField] private float wait;
    private void Awake()
    {
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
    }
    public void StartWalkRandomly()
    {
        StartCoroutine(WalkRandomly());
    }
    private IEnumerator WalkRandomly()
    {
        while (true)
        {
            int rx = Random.Range(-1, 2);
            int ry = Random.Range(-1, 2);

            Vector3 dir = new Vector3(rx, ry, 0).normalized;
            m_NavMeshAgent.SetDestination(transform.position - (dir * displacementDist));

            yield return new WaitForSeconds(wait);
        }
    }

    public void StopWalkRandomly()
    {
        StopAllCoroutines();
    }


}

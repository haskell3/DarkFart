using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeDetection : MonoBehaviour, IComparer<Collider2D>
{
    [SerializeField]
    private EnemyController controller;
    private EnemyDetection detection;
    private float radius;
    [SerializeField] private bool isAttacking;
    public bool IsAttacking { get { return isAttacking; } }
    [SerializeField] private float secondsToWait = 0.5f;
    [SerializeField] private LayerMask layerMask;
    private void Start()
    {
        radius = controller.EnemySO.AttackRange;
        GetComponent<CircleCollider2D>().radius = radius;
        detection = controller.gameObject.GetComponentInChildren<EnemyDetection>();
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 7 || collision.gameObject.layer == 8)
        {
            controller.ChangeToAttack();
        }else if(collision.gameObject.layer == 13)
        {
            Destroy(collision.gameObject);
            //TODO think what should do th enemy after eating the plant
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 7 || collision.gameObject.layer == 8)
        {
            controller.ChangeToChase(collision.transform);
        }
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isAttacking) return;
        if (collision.gameObject.layer == 7 || collision.gameObject.layer == 8 || collision.gameObject.layer == 13)
        {
            StartCoroutine(WhoToAttack());
        }
        if(collision.gameObject.layer == 12 && controller.AttackingBarreras)
        {
            controller.ChangeToAttack(collision.transform);
        }
    }

    private IEnumerator WhoToAttack()
    {
        isAttacking = true;
        while (true)
        {
            yield return new WaitForSeconds(secondsToWait);

            if (controller.AttackingBarreras)
            {
                isAttacking = false;
                StopAllCoroutines();
                break;
            }

            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius, layerMask);

            if (colliders.Length == 0)
            {
                isAttacking = false;
                detection.StopAttacking();
                StopAllCoroutines();
                break;
            }

            Array.Sort(colliders, this);

            if (colliders[0].gameObject.layer == 13)
            {
                Destroy(colliders[0].gameObject);
                continue;
            }

            controller.ChangeToAttack(colliders[0].transform);
        }
    }

    public int Compare(Collider2D x, Collider2D y)
    {
        return (int)((x.transform.position - transform.position).magnitude - (y.transform.position - transform.position).magnitude);
    }
}

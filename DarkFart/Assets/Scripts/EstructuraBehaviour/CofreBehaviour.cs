using System.Collections.Generic;
using UnityEngine;
using static Backpack;

public class CofreBehaviour : MonoBehaviour, Interactuable, EstructuraBehaviour
{
    [SerializeField] private CofreController m_CofreController;
    private Estructura estructura;
    public Estructura Estructura { get { return estructura; } set { estructura = value; } }

    [SerializeField]
    private ItemSlot[] m_ItemSlots = new ItemSlot[36];

    public ItemSlot[] ItemSlots { get { return m_ItemSlots; } set { m_ItemSlots = value; } }

    private void Start()
    {
        m_CofreController = CofreController.Instance;
    }

    public void Interact()
    {
        if (GetComponent<CofreLootRandom>())
        {
            List<ItemSlot> items = GetComponent<CofreLootRandom>().createLootRandom();
            if (items != null)
            {
                foreach (ItemSlot item in items)
                {
                    addItemAleatorySlot(item);
                }
            }
        }
        m_CofreController.cofreVisible();
        if (gameObject.activeSelf == true)
            m_CofreController.setInventory(m_ItemSlots);
    }

    private void addItemAleatorySlot(ItemSlot item)
    {
        int random = Random.Range(0, m_ItemSlots.Length);
        while (m_ItemSlots[random].Item != null)
        {
            random = Random.Range(0, m_ItemSlots.Length);
        }
        m_ItemSlots[random] = item;
    }
}

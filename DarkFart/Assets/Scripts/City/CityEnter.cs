using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityEnter : MonoBehaviour
{
    [SerializeField] private SceneController.SCENES sceneName;
    [SerializeField] private MapInfo MapInfo;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            transform.position = FindObjectOfType<MovimentPlayer>().transform.position + new Vector3(0, 3, 0);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        MapInfo.Save();
        collision.GetComponent<LastPositionPlayer>().LastPosition = new Vector3(this.transform.position.x, this.transform.position.y-2, 0);
        FindFirstObjectByType<MovimentPlayer>().transform.position = new Vector3(1, -18, 0);
        SceneController.Instance.ChangeScene(sceneName);
    }
}

using System;
using System.Collections.Generic;
using UnityEngine;

public class CityGenerator : MonoBehaviour
{
    [SerializeField]
    private CityInfo cityInfo;
    [SerializeField]
    private GameObject m_Enemy;
    [SerializeField]
    private GameObject m_ItemWorld;
    [SerializeField]
    private List<TascaGameObject> tascaGameObjects;
    // Start is called before the first frame update
    void Start()
    {
        SpawnEnemies();
        SpawnNPCs();
    }

    private void SpawnEnemies()
    {
        for (int i = 0; i < cityInfo.EnemySpawnInfos.Length; ++i)
        {
            EnemySpawnInfo e = cityInfo.EnemySpawnInfos[i];
            int r = UnityEngine.Random.Range(e.minAmountGroupEnemies, e.maxAmountGroupEnemies + 1);
            for (int j = 0; j < r; j++)
            {
                int ry = UnityEngine.Random.Range(cityInfo.CityPerimeterDownUp.x, cityInfo.CityPerimeterDownUp.y + 1);
                int rx = UnityEngine.Random.Range(cityInfo.CityPerimeterLeftRight.x, cityInfo.CityPerimeterLeftRight.y + 1);
                Vector3 pos = new Vector3(rx, ry, 0);
                for (int k = 0; k < e.groupAmount; ++k)
                {
                    GameObject g = Instantiate(m_Enemy);
                    g.GetComponent<EnemyController>().EnemySO = e.enemy;
                    g.transform.position = pos;
                }
                
            }
        }
        
    }

    private void SpawnItems()
    {
        for (int i = 0; i < cityInfo.ItemSpawnInfos.Length; ++i)
        {
            ItemSpawnInfo item = cityInfo.ItemSpawnInfos[i];
            int r = UnityEngine.Random.Range(item.minAmountItems, item.maxAmountItems + 1);
            for (int j = 0; j < r; j++)
            {
                if (UnityEngine.Random.Range(0, 101) >= item.spawnProbability) continue;
                int ry = UnityEngine.Random.Range(cityInfo.CityPerimeterDownUp.x, cityInfo.CityPerimeterDownUp.y + 1);
                int rx = UnityEngine.Random.Range(cityInfo.CityPerimeterLeftRight.x, cityInfo.CityPerimeterLeftRight.y + 1);
                Vector3 pos = new Vector3(rx, ry, 0);
                Item iSpawned = item.item;
                GameObject g = Instantiate(m_ItemWorld);
                int rq = UnityEngine.Random.Range(item.minQuantity, item.maxQuantity + 1);
                g.GetComponent<ItemInWorld>().Inicia(iSpawned, rq, pos);

            }
        }
    }
    private void SpawnNPCs()
    {
        for (int i = 0; i < cityInfo.NPCSpawnInfos.Length; ++i)
        {
            NPCSpawnInfo npc = cityInfo.NPCSpawnInfos[i];
            int r = UnityEngine.Random.Range(npc.minAmountNPCs, npc.maxAmountNPCs + 1);
            for (int j = 0; j < r; j++)
            {
                if (UnityEngine.Random.Range(0, 101) >= npc.spawnProbability) continue;
                int ry = UnityEngine.Random.Range(cityInfo.CityPerimeterDownUp.x, cityInfo.CityPerimeterDownUp.y + 1);
                int rx = UnityEngine.Random.Range(cityInfo.CityPerimeterLeftRight.x, cityInfo.CityPerimeterLeftRight.y + 1);
                Vector3 pos = new Vector3(rx, ry, 0);
                PersonalidadInfo personalidad = npc.personalidad;
                TascaInfo.Tasca t = npc.tasques[0].tasca;
                int erp = UnityEngine.Random.Range(1, 101);
                for (int k = 0; k < npc.tasques.Length; ++k)
                {
                    if (erp < npc.tasques[k].probability)
                    {
                        t = npc.tasques[k].tasca;
                        break;
                    }
                }
                GameObject g = Instantiate(FindGameObjectByTasca(t, tascaGameObjects));
                g.GetComponent<NPCController>().Personality = personalidad;
                g.GetComponent<NPCController>().Name = cityInfo.NpcNames[UnityEngine.Random.Range(0, cityInfo.NpcNames.Length)];
                g.transform.position = pos;

                //Esto es pa los sprites
                int rsprites = UnityEngine.Random.Range(0, personalidad.SpritesInfo.Count);
                g.GetComponent<SpriteRenderer>().sprite = personalidad.SpritesInfo[rsprites].Sprites[0];
                g.GetComponent<SpriteFrontBackSideAgent>().Sprites = personalidad.SpritesInfo[rsprites].Sprites;


            }
        }
    }

    public static GameObject FindGameObjectByTasca(TascaInfo.Tasca t, List<TascaGameObject> list)
    {
        for (int i = 0; i < list.Count; ++i)
        {
            if (t == list[i].tasca) return list[i].prefab;
        } 
        return null;
    }

    [Serializable]
    public class TascaGameObject
    {
        public TascaInfo.Tasca tasca;
        public GameObject prefab;

        public TascaGameObject()
        {
            tasca = TascaInfo.Tasca.NONE;
            prefab = null;
        }

        public TascaGameObject(TascaInfo.Tasca t, GameObject p)
        {
            tasca = t;
            prefab = p;
        }
        

    }


}

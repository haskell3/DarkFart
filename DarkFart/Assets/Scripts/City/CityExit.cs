using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityExit : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.transform.position = collision.GetComponent<LastPositionPlayer>().LastPosition;
        SceneController.Instance.ChangeScene(SceneController.SCENES.Map);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VidaControllerBoss))]
public class BossController : MonoBehaviour
{
    //Cuando hagamos mas bosses ya lo hare generico joder no me toqueis los cojones
    [SerializeField]
    private Sack sack;
    [SerializeField]
    private GameObject m_Door;
    [SerializeField]
    private AudioSource m_Music;
    [SerializeField]
    private AudioClip m_CityMusic;

    private void Awake()
    {
        sack = (Sack)FindAnyObjectByType(typeof(Sack));
    }
    public void fase1()
    {
        sack.ChangeFase(1);
    }

    public void fase2()
    {
        sack.ChangeFase(2);
    }

    public void fase3()
    {
        sack.ChangeFase(3);
    }

    public void Drop()
    {
        m_Music.clip = m_CityMusic;
        m_Music.Play();
        m_Door.SetActive(false);
    }
}

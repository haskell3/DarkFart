using System.Collections;
using UnityEngine;


[RequireComponent(typeof(IDieable))]
public class VidaController : MonoBehaviour
{
    [SerializeField] private ParticleSystem m_InstanceParticles;
    [SerializeField] protected float hp;
    public float Hp
    {
        get { return hp; }
        set { hp = value; }
    }
    [SerializeField] protected float maxHp;
    public float MaxHP
    {
        get { return maxHp; }
        set { maxHp = value; }
    }
    [SerializeField]
    private float timeEnergy = 0.5f;
    [SerializeField]
    private float timeHambre = 0.5f;
    [SerializeField] private Armadura m_armadura;
    [SerializeField] private VidaBarra GUI;
    public VidaBarra Gui { get { return GUI; } set { GUI = value; } }
    public Armadura Armadura
    {
        get { return m_armadura; }
        set { m_armadura = value; }
    }

    private Coroutine m_CoroutineEnergy;
    private Coroutine m_CoroutineStarving;
    public bool withoutEnergy = false;

    public void ExtraHp(Armadura armadura)
    {
        if (armadura != null)
        {
            if (m_armadura == null)
            {
                m_armadura = armadura;
                float y = hp / maxHp;
                maxHp += m_armadura.HpExtra;
                hp = maxHp * y;
            }
            else if (m_armadura.HpExtra != armadura.HpExtra)
            {
                m_armadura = armadura;
                float y = hp / maxHp;
                maxHp += m_armadura.HpExtra;
                hp = maxHp * y;
            }
        }
        else
        {
            if (m_armadura == null)
                return;
            float y = hp / maxHp;
            maxHp -= m_armadura.HpExtra;
            hp = maxHp * y;
            m_armadura = null;
        }
        RefreshGUI();
    }

    private void Awake()
    {
        hp = maxHp;
        m_InstanceParticles = GetComponent<ParticleSystem>();
        /*m_InstanceParticles = Instantiate(m_Particles).GetComponent<ParticleSystem>();
        ParticleSystem.MainModule settings = GetComponent<ParticleSystem>().main;
        settings.startColor = new ParticleSystem.MinMaxGradient(m_ParticleColor);*/
    }

    public void RecoverHP(float h)
    {
        hp += h;
        RefreshGUI();
        if (hp > maxHp) { hp = maxHp; }
    }
    public void LessHP(float h)
    {
        /*m_InstanceParticles.transform.position = transform.position;*/
        m_InstanceParticles.Play();
        hp -= h;
        RefreshGUI();
        if (hp <= 0)
        {
            hp = 0;
            gameObject.GetComponent<IDieable>().Die();
        }
    }

    public void WithoutEnergy()
    {
        withoutEnergy = true;
        m_CoroutineEnergy = StartCoroutine(DyingFromTime());
    }

    public void WithEnergy()
    {
        withoutEnergy = false;
        if (m_CoroutineEnergy != null)
            StopCoroutine(m_CoroutineEnergy);
    }

    public void Starving()
    {
        if(m_CoroutineStarving != null)
            StopCoroutine(m_CoroutineStarving);
        m_CoroutineStarving = StartCoroutine(DyingFromStarving());
    }

    public void StopStarving()
    {
        if (m_CoroutineStarving != null)
            StopCoroutine(m_CoroutineStarving);
    }

    private IEnumerator DyingFromTime()
    {
        while (hp > 0)
        {
            yield return new WaitForSeconds(timeEnergy);
            hp--;
            RefreshGUI();
        }
        hp = 0;
        gameObject.GetComponent<IDieable>().Die();
        StopAllCoroutines();
    }

    private IEnumerator DyingFromStarving()
    {
        while (hp > 0)
        {
            yield return new WaitForSeconds(timeHambre);
            hp--;
            RefreshGUI();
        }
        hp = 0;
        gameObject.GetComponent<IDieable>().Die();
        StopAllCoroutines();
    }

    public void RecoverFromSleep(int hours)
    {
        if (hours > 24) hours = 24;

        float par = maxHp / 24;

        hp += (par * hours);
        if (hp > maxHp) { hp = maxHp; }
        RefreshGUI();
    }

    public float mirarHp()
    {
        return hp;
    }

    public float mirarHpMax()
    {
        return maxHp;
    }

    private void RefreshGUI()
    {
        GUI?.mostrar();
    }

    public void SceneLoaded()
    {
        GUI = FindFirstObjectByType<VidaBarra>(FindObjectsInactive.Include);
        if (InventarioController.Instance.getBackpack().GetArmadura() != null)
            ExtraHp((Armadura)InventarioController.Instance.getBackpack().GetArmadura().Item);
    }

    public void resetHp()
    {
        hp = maxHp;
        RefreshGUI();
    }
}

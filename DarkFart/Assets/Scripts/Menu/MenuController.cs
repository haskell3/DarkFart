using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;

public class MenuController : MonoBehaviour
{
    private guiController canvas;
    private MovimentPlayer player;
    private GameController gameController;
    [SerializeField] private GranjaInfo granjaInfo;
    [SerializeField] private MapInfo mapInfo;
    [SerializeField] private Backpack backpack;

    private void Start()
    {
        canvas = FindFirstObjectByType<TagCanvas>(FindObjectsInactive.Include).gameObject.GetComponent<guiController>();
        gameController = FindFirstObjectByType<GameController>(FindObjectsInactive.Include);
        player = FindFirstObjectByType<MovimentPlayer>(FindObjectsInactive.Include);

        canvas.gameObject.SetActive(false);
        gameController.gameObject.SetActive(false);
        player.enabled = false;
        player.GetComponent<InteractPlayer>().enabled = false;
        player.GetComponent<InputController>().DisableDefault();
    }

    public void NewGame()
    {
        granjaInfo.ClearAllInfo();
        mapInfo.ClearAllInfo();
        backpack.ClearAllInfo();
        SceneController.Instance.ChangeScene(SceneController.SCENES.Granja);
        TimeManager.Instance.Reset(); 
        player.transform.position = new Vector2(-7.5f, 128.5f);
        startAll();
    }

    public void Load()
    {
        string scene = SaveData.Instance.Load();
        SceneController.Instance.ChangeScene(SceneController.Instance.FromStringToScene(scene));
        startAll();
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Game is exiting");
    }

    private void startAll()
    {
        SceneController.Instance.startAll();
        player.GetComponent<InventarioController>().Refresh();
        player.GetComponent<InputController>().EnableDefault();
        player.GetComponent<InputController>().EnableHand();
        player.GetComponent<HambreController>().resetHambre();
        player.GetComponent<VidaController>().resetHp();
        player.GetComponent<EnergiaController>().resetEnergia();
        gameController.gameObject.SetActive(true);
    }
}

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;

public class MenuGameController : MonoBehaviour
{
    private InputController _controller;
    [SerializeField] private GranjaInfo granjaInfo;
    [SerializeField] private MapInfo mapInfo;

    public void setController(InputController controller)
    {
        _controller = controller;
    }

    public void Save()
    {
        if(SceneController.Instance.ActualScene == SceneController.SCENES.Map) 
            mapInfo.Save();
        else if(SceneController.Instance.ActualScene == SceneController.SCENES.Granja)
            granjaInfo.Save();
        else
            SaveData.Instance.Save();
    }


    public void GoToMenu()
    {
        Save();
        FindFirstObjectByType<TagCanvas>().gameObject.GetComponent<guiController>().gameObject.SetActive(false);
        FindFirstObjectByType<GameController>().gameObject.SetActive(false);
        FindFirstObjectByType<MovimentPlayer>().enabled = false;
        CloseMenu();
        FindFirstObjectByType<InteractPlayer>().enabled = false;
        SceneController.Instance.ChangeScene(SceneController.SCENES.Menu);
    }


    public void Resume()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        _controller.EnableHand();
    }

    public void OpenMenu(InputAction.CallbackContext context)
    {
        bool menuIsOpen = false;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(!transform.GetChild(i).gameObject.activeInHierarchy);
            if (transform.GetChild(i).gameObject.activeSelf == true)
                menuIsOpen = true;
            else
                menuIsOpen = false;
        }
        _controller.EnableHand();
        if (menuIsOpen)
            _controller.DisableHand();
        else
            _controller.EnableHand();
    }

    private void CloseMenu()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(!transform.GetChild(i).gameObject.activeInHierarchy);
        }
        _controller.EnableHand();
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Game is exiting");
    }
}

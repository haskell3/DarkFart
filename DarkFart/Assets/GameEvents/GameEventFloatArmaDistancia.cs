using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEventFloatArmaDistancia", menuName = "GameEvent/GameEventFloatArmaDistancia")]
public class GameEventFloatArmaDistancia : GameEvent<float[], bool, Arma> { }
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEventInt", menuName = "GameEvent/GameEventInt")]
public class GameEventInt : GameEvent<int> { }

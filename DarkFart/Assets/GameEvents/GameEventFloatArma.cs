using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEventFloatArma", menuName = "GameEvent/GameEventFloatArma")]
public class GameEventFloatArma : GameEvent<float, float, float, Arma> { }
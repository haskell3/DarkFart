using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Manual", menuName = "Craft/Manual")]
public class Manual : Consumible, IConsume
{
    [SerializeField] private CraftBook m_craftBook;
    [SerializeField] private int m_PosDesbloquejar;

    public void Consume()
    {
        m_craftBook.recipesSlot[m_PosDesbloquejar].unlocked = true;
        m_craftBook.recipesSlot[m_PosDesbloquejar].newCraft = true;
    }
}

using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Galleda", menuName = "Item/Equipable/Herramientas/Galleda")]
public class Galleda : Equipable, IActionable
{
    [SerializeField] private Tile m_Water;
    [SerializeField] private GameEventFloat m_WasteEnergy;
    [SerializeField] private int MaxWater;
    public int maxWater
    {
        get
        {
            return MaxWater;
        }
        set
        {
            MaxWater = value;
        }
    }
    [SerializeField] private int water;
    public int Water
    {
        get
        {
            return water;
        }
        set
        {
            water = value;
        }
    }
    [Header("Functionality")]
    [SerializeField]
    private LayerMask layersUI;
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        Vector3Int target = t.WorldToCell(position + direction);
        if (t.GetTile<Tile>(target) == m_Water)
        {
            water = MaxWater;
        }
        else if (water > 0)
        {
            RaycastHit2D hit = Physics2D.Raycast(position + direction, Vector3.forward, Mathf.Infinity, layersUI);

            if (hit.rigidbody != null)
            {
                if (hit.collider.gameObject.GetComponent<CalderoBehaviour>() != null)
                {


                    CalderoBehaviour calderoBehaviour = hit.collider.gameObject.GetComponent<CalderoBehaviour>();
                    int quantity = calderoBehaviour.getWater();
                    int maxQuantity = calderoBehaviour.getMaxWater();
                    if (quantity < maxQuantity)
                    {
                        if ((quantity + water) < maxQuantity)
                        {
                            calderoBehaviour.sumWater(water);
                            water = 0;
                        }
                        else
                        {
                            int waterMax = maxQuantity - quantity;
                            calderoBehaviour.sumWater(waterMax);
                            water -= waterMax;
                        }
                        m_WasteEnergy.Raise(EnergiaQueGasta);
                    }
                }
            }
        }
    }
}

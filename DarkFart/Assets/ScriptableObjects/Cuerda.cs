using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Cuerda", menuName = "Item/Equipable/Herramientas/Cuerda")]
public class Cuerda : Equipable
{
    [SerializeField] private Animal animal;
    public Animal Animal
    {
        get
        {
            return animal;
        }
        set
        {
            animal = value;
        }
    }
    [SerializeField] private Color colorRope;
    public Color color
    {
        get
        {
            return colorRope;
        }
        set
        {
            colorRope = value;
        }
    }
    [SerializeField]
    private Material m_defaultMaterialRope;
    public Material material
    {
        get
        {
            return m_defaultMaterialRope;
        }
        set
        {
            m_defaultMaterialRope = value;
        }
    }

    [Header("Functionality")]
    [SerializeField]
    private LayerMask layersUI;

    private void Awake()
    {
        animal = null;
    }
    public GameObject Action(Vector2 position, Vector2 direction, Tilemap t, Rigidbody2D player, GameObject lastAnimal)
    {
        RaycastHit2D hit = Physics2D.Raycast(position + direction, Vector3.forward, Mathf.Infinity, layersUI);
        //Debug.Log("Cuelda");
        if (hit.rigidbody != null)
        {
            Debug.Log("Hit: " + hit.collider.name);
            if (animal != null)
            {
                Destroy(lastAnimal.GetComponent<DistanceJoint2D>());
                lastAnimal.GetComponent<AnimalController>().stopDraw();
                lastAnimal.GetComponent<AnimalController>().ChangeToRandomWalk();
                lastAnimal.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                animal = null;
            }
                
            if (hit.collider.gameObject.GetComponent<AnimalController>() != null && player != null)
            {
                lastAnimal = hit.collider.gameObject;
                animal = lastAnimal.GetComponent<AnimalController>().animal;
                lastAnimal.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                DistanceJoint2D joint = hit.collider.gameObject.AddComponent<DistanceJoint2D>();
                joint.connectedBody = player.gameObject.GetComponent<Rigidbody2D>();
                joint.autoConfigureDistance = false;
                joint.distance = 5;
                joint.maxDistanceOnly = true;
                LineRenderer lr = hit.collider.gameObject.AddComponent<LineRenderer>();
                lr.startColor = color;
                lr.endColor = color;
                lr.material = material;
                lr.SetWidth(0.1f, 0.1f);
                lr.sortingOrder = -1;
                lastAnimal.GetComponent<AnimalController>().startDraw(lr);
                hit.collider.gameObject.GetComponent<AnimalController>().ChangeToIdle();
                return hit.collider.gameObject;
            }
        }
        else
        {
            //Debug.Log("nada");
            if (lastAnimal != null && animal != null)
            {
                Destroy(lastAnimal.GetComponent<DistanceJoint2D>());
                lastAnimal.GetComponent<AnimalController>().stopDraw();
                lastAnimal.GetComponent<AnimalController>().ChangeToRandomWalk();
                lastAnimal.gameObject.transform.GetChild(0).gameObject.SetActive(true);
            }
            animal = null;
        }
        return null;
    }
}

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
[Serializable]
public class PersonalidadInfo : ScriptableObject
{
    [SerializeField] private List<SpritesInfo> spritesInfo;
    public List<SpritesInfo> SpritesInfo { get { return spritesInfo; } }
    [SerializeField]
    private List<FriendshipLevelInfo> levels;
    public List<FriendshipLevelInfo> Levels { get { return levels; } }
    [SerializeField] private DialogueInfo recruitmentDialogue;
    public DialogueInfo RecruitmentDialogue { get { return recruitmentDialogue; } }
}

[Serializable]
public struct FriendshipLevelInfo
{
    public float maxFriendship;
    public List<DialogueInfo> dialogues;
}

[Serializable]
public struct DialogueInfo
{
//    public DefaultAsset inkDialogueFile;
    public TextAsset dialogueTextAsset;
}

[Serializable]
public class SpritesInfo
{
    public Sprite[] Sprites = new Sprite[3];
}


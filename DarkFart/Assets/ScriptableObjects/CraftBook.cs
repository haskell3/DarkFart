using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using static Backpack;

[CreateAssetMenu(fileName = "CraftBook", menuName = "Craft/CraftBook")]
public class CraftBook : ScriptableObject
{
    [Serializable]
    public class CraftRecipe
    {
        [SerializeField]
        public bool unlocked;
        [SerializeField]
        public bool newCraft;
        [SerializeField]
        public Craft recipe;
    }

    [SerializeField]
    private CraftRecipe[] recipes;

    public ReadOnlyCollection<CraftRecipe> recipesSlot => new ReadOnlyCollection<CraftRecipe>(recipes);

    public CraftRecipe GetCraftSlot(int pos)
    {
        return recipes[pos];
    }
}

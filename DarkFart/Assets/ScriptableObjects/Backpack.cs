using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Backpack", menuName = "Player/Backpack")]
[Serializable]
public class Backpack : ScriptableObject
{
    [Serializable]
    public class ItemSlot
    {
        [SerializeField]
        public Item Item;
        [SerializeField]
        public int Amount;

        public ItemSlot()
        {
            Item = null;
            Amount = 1;
        }

        public ItemSlot(Item obj)
        {
            Item = obj;
            Amount = 1;
        }
        public ItemSlot(Item obj, int amount)
        {
            Item = obj;
            Amount = amount;
        }

        public bool Stack(ItemSlot slot)
        {
            //Esto seria si el item movido a X slot es el mismo y se puede stackear pues stackearlo
            if (Item == null)
            {
                Item = slot.Item;
                Amount = slot.Amount;
                return true;
            }
            else if (Item == slot.Item)
            {
                Amount += slot.Amount;
                return true;
            }

            return false;
        }
    }
    [Header("Backpack")]
    [SerializeField]
    private ItemSlot[] m_ItemSlots2 = new ItemSlot[36];
    public ReadOnlyCollection<ItemSlot> ItemSlots2 => new ReadOnlyCollection<ItemSlot>(m_ItemSlots2);

    [Header("Armor")]
    [SerializeField]
    private ItemSlot m_Armadura = new ItemSlot();

    [SerializeField]
    private int m_slotArmadura = 36;

    [Header("SecondHand")]
    [SerializeField]
    private ItemSlot m_SecondaryHand = new ItemSlot();

    [SerializeField]
    private int m_slotSecondaryHand = 37;

    public int CountOccupiedSlots()
    {
        int count = m_ItemSlots2.Count(slot => slot.Item != null);
        return count;
    }

    public bool CountOccupiedSlots(Item item, int amount)
    {
        if (item is not Equipable)
        {
            ItemSlot tryItem = GetItem(item);
            if (tryItem == null)
                return canAdd(item, amount);
            else
                return true;
        }
        if (CountOccupiedSlots() < 36)
            return true;
        return false;
    }

    public void Add(Item addItem)
    {
        for (int i = 0; i < m_ItemSlots2.Length; i++)
        {
            if (m_ItemSlots2[i].Item == null)
            {
                m_ItemSlots2[i] = new ItemSlot(addItem);
                break;
            }
        }
    }

    public bool canAdd(Item addItem, int amount)
    {
        if (CountOccupiedSlots() < 36)
        {
            for (int i = 0; i < m_ItemSlots2.Length; i++)
            {
                if (m_ItemSlots2[i].Item == null)
                {
                    return true;
                }
            }
        }
        return false;

    }
    public void AddByPos(int pos, ItemSlot item)
    {
        if (item.Item != null)
        {
            m_ItemSlots2[pos].Item = item.Item;
            m_ItemSlots2[pos].Amount = item.Amount;
        }
        else
        {
            m_ItemSlots2[pos].Item = null;
            m_ItemSlots2[pos].Amount = 0;
        }
    }


    public bool Add(Item addItem, int amount)
    {
        if (CountOccupiedSlots() < 36)
        {
            for (int i = 0; i < m_ItemSlots2.Length; i++)
            {
                if (m_ItemSlots2[i].Item == null)
                {
                    m_ItemSlots2[i] = new ItemSlot(addItem, amount);
                    return true;
                }
            }
        }
        return false;
    }

    public void AddItem(Item addedItem)
    {
        if (addedItem is Equipable)
            AddEquipable(addedItem);
        else if (addedItem is Materiales)
            AddMaterial(addedItem);
        else
            AddMaterial(addedItem);

    }

    public int AddItem(Item addedItem, int amount)
    {
        if (addedItem is Equipable)
            AddEquipable(addedItem);
        else if (addedItem is Materiales)
            return AddMaterial(addedItem, amount);
        else
            return AddMaterial(addedItem, amount);
        return 0;

    }


    //Funcion para items que se pueden stackear
    public void AddMaterial(Item addItem)
    {
        ItemSlot item = GetItem(addItem);
        if (item == null)
        {
            if (CountOccupiedSlots() < 36)
                Add(addItem);
        }
        else
        {
            if (item.Amount >= 99)
            {
                Add(addItem);
            }
            else
            {
                item.Amount++;
            }
        }
    }

    //Funcion para items que se pueden stackear
    public int AddMaterial(Item addItem, int amount)
    {
        ItemSlot item = GetItem(addItem);
        //Debug.Log("Amount: " + amount);
        if (item == null)
        {
            Add(addItem, amount);
        }
        else
        {
            if (item.Amount >= 99)
            {
                //Debug.Log("Item: " + item.Item);
                Add(addItem, amount);
            }
            else
            {
                //Debug.Log("Item3: " + item.Item);
                int quantity = 99 - item.Amount;
                if (quantity >= amount)
                {
                    item.Amount += amount;
                    amount = 0;
                }
                else if (quantity < amount)
                {
                    item.Amount += quantity;
                    amount -= quantity;
                }
                if (amount > 0)
                {
                    //Debug.Log("Item4: " + item.Item);
                    if (!Add(addItem, amount))
                        return amount;
                }
            }
        }
        return 0;
    }

    //Funcion para items que no se pueden stackear
    public void AddEquipable(Item addItem)
    {
        Add(addItem);
    }

    public void Remove(ItemSlot removeItem)
    {
        for (int i = 0; i < m_ItemSlots2.Length; i++)
        {
            if (m_ItemSlots2[i] == removeItem)
            {
                m_ItemSlots2[i] = null;
                break;
            }
        }
    }

    public void Remove(Item removeItem)
    {
        for (int i = 0; i < m_ItemSlots2.Length; i++)
        {
            if (m_ItemSlots2[i].Item == removeItem)
            {
                m_ItemSlots2[i].Item = null;
                m_ItemSlots2[i].Amount = 0;
                break;
            }
        }
    }

    public void RemoveItem(Item removeItem)
    {
        ItemSlot item = GetItem(removeItem);
        if (item == null)
            return;

        item.Amount--;
        if (item.Amount <= 0)
            Remove(item);
    }

    public void RemoveByPos(int pos)
    {
        if (pos == m_slotArmadura)
        {
            m_Armadura.Item = null;
            m_Armadura.Amount = 0;
            return;
        }
        else if (pos == m_slotSecondaryHand)
        {
            m_SecondaryHand.Amount--;
            if (m_SecondaryHand.Amount <= 0)
            {
                m_SecondaryHand.Item = null;
                m_SecondaryHand.Amount = 0;
            }
            return;
        }
        pos -= 1;
        m_ItemSlots2[pos].Amount--;
        if (m_ItemSlots2[pos].Amount <= 0)
        {
            m_ItemSlots2[pos].Item = null;
            m_ItemSlots2[pos].Amount = 0;
        }
    }

    public void RemoveItemByPos(int pos)
    {
        if (pos == m_slotArmadura)
        {
            m_Armadura.Item = null;
            m_Armadura.Amount = 0;
            return;
        }
        else if (pos == m_slotSecondaryHand)
        {
            m_SecondaryHand.Item = null;
            m_SecondaryHand.Amount = 0;
            return;
        }
        m_ItemSlots2[pos].Item = null;
        m_ItemSlots2[pos].Amount = 0;
    }

    public void RemoveAmountByPos(int pos, int amount)
    {
        m_ItemSlots2[pos].Amount -= amount;
        if (m_ItemSlots2[pos].Amount <= 0)
        {
            m_ItemSlots2[pos].Item = null;
            m_ItemSlots2[pos].Amount = 0;
        }

    }

    private ItemSlot GetItem(Item item)
    {
        //No funciona esto (Tendre que o arreglarlo o que recorra toda la backpack)
        List<ItemSlot> items = m_ItemSlots2.Where(slot => slot.Item == item).ToList();

        //Debug.Log(items.Count);

        if (item != null)
        {
            for (int i = 0; i < m_ItemSlots2.Length; i++)
            {
                if (m_ItemSlots2[i].Item == item && m_ItemSlots2[i].Amount < 99)
                {
                    //Debug.Log(i);
                    return m_ItemSlots2[i];
                }

            }
        }
        return null;
    }

    private ItemSlot GetItem(Item item, ItemSlot[] slots)
    {
        //No funciona esto (Tendre que o arreglarlo o que recorra toda la backpack)
        List<ItemSlot> items = slots.Where(slot => slot.Item == item).ToList();

        //Debug.Log(items.Count);

        if (item != null)
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (slots[i].Item == item && slots[i].Amount < 99)
                {
                    //Debug.Log(i);
                    return slots[i];
                }

            }
        }
        return null;
    }

    public ItemSlot GetItemSlot(int pos)
    {
        if (pos == m_slotArmadura)
            return m_Armadura;
        else if (pos == m_slotSecondaryHand)
            return m_SecondaryHand;
        return m_ItemSlots2[pos];
    }

    public ItemSlot GetArmadura()
    {
        return m_Armadura;
    }

    public ItemSlot GetSecondaryHand()
    {
        return m_SecondaryHand;
    }

    public void setArmadura(Armadura armadura)
    {
        m_Armadura = new ItemSlot(armadura);
    }

    public void SetSecondaryHand(Item item)
    {
        m_SecondaryHand = new ItemSlot(item);
    }

    public void SetSecondaryHand(int pos)
    {
        pos -= 1;
        ItemSlot secundaryHand = m_SecondaryHand;
        m_SecondaryHand = m_ItemSlots2[pos];
        if (secundaryHand != null)
            m_ItemSlots2[pos] = secundaryHand;
        else
            m_ItemSlots2[pos] = null;
    }

    public int GetPosSlotArmadura()
    {
        return m_slotArmadura;
    }

    public int GetPosSlotSecondaryHand()
    {
        return m_slotSecondaryHand;
    }

    public void changeBackpackItem(int newPos, int oldPos)
    {
        /*
        if (m_ItemSlots2[newPos].Stack(m_ItemSlots2[oldPos]))
            return;
        */

        ItemSlot newSlot = null;
        if (newPos == m_slotArmadura)
        {
            //Debug.Log("1");
            newSlot = m_Armadura;
            if (m_ItemSlots2[oldPos].Item is Armadura)
            {
                m_Armadura = m_ItemSlots2[oldPos];
                if (newSlot != null)
                    m_ItemSlots2[oldPos] = newSlot;
                else
                    m_ItemSlots2[oldPos] = null;
            }
            else
                return;
        }
        else if (oldPos == m_slotArmadura)
        {
            //Debug.Log("2");
            newSlot = m_ItemSlots2[newPos];
            m_ItemSlots2[newPos] = m_Armadura;
            if (newSlot.Item is Armadura && newSlot != null)
                m_Armadura = newSlot;
            else
                m_Armadura = new ItemSlot();
        }
        else if (newPos == m_slotSecondaryHand)
        {
            //Debug.Log("3");
            if (oldPos != newPos)
            {
                newSlot = m_SecondaryHand;
                m_SecondaryHand = m_ItemSlots2[oldPos];
                if (newSlot != null)
                    m_ItemSlots2[oldPos] = newSlot;
                else
                    m_ItemSlots2[oldPos] = null;
            }
        }
        else if (oldPos == m_slotSecondaryHand)
        {
            //Debug.Log("4");
            newSlot = m_ItemSlots2[newPos];
            m_ItemSlots2[newPos] = m_SecondaryHand;
            if (newSlot != null)
                m_SecondaryHand = newSlot;
            else
                m_SecondaryHand = new ItemSlot();
        }
        else
        {
            //Debug.Log("5");
            ItemSlot newItem = m_ItemSlots2[newPos];
            ItemSlot oldItem = m_ItemSlots2[oldPos];
            if (newItem != null && oldItem != null && oldPos != newPos)
            {
                if (oldItem.Item == newItem.Item)
                {
                    int amount = 99 - oldItem.Amount;
                    if (amount >= newItem.Amount)
                    {
                        //Debug.Log("1");
                        oldItem.Amount += newItem.Amount;
                        newItem = new ItemSlot();
                    }
                    else if (amount < newItem.Amount)
                    {
                        //Debug.Log("2");
                        oldItem.Amount += amount;
                        newItem.Amount -= amount;
                    }
                }
            }
            if (newItem != null)
                m_ItemSlots2[oldPos] = newItem;
            else
                m_ItemSlots2[oldPos] = null;

            if (oldItem != null)
                m_ItemSlots2[newPos] = oldItem;
            else
                m_ItemSlots2[newPos] = null;
        }
    }

    public void changeToAnotherBackpack(int newPos, int oldPos, ItemSlot[] cofre)
    {
        /*
        if (cofre[newPos].Stack(m_ItemSlots2[oldPos]))
            return;
        */
        //Stack(newPos, oldPos, cofre);

        ItemSlot newItem = cofre[newPos];
        ItemSlot oldItem = m_ItemSlots2[oldPos];


        if (newItem != null && oldItem != null && oldPos != newPos)
        {
            if (oldItem.Item == newItem.Item)
            {
                int amount = 99 - oldItem.Amount;
                if (amount >= newItem.Amount)
                {
                    //Debug.Log("1");
                    oldItem.Amount += newItem.Amount;
                    newItem = new ItemSlot();
                }
                else if (amount < newItem.Amount)
                {
                    //Debug.Log("2");
                    oldItem.Amount += amount;
                    newItem.Amount -= amount;
                }
            }
        }

        if (newItem != null)
            m_ItemSlots2[oldPos] = newItem;
        else
            m_ItemSlots2[oldPos] = null;

        if (oldItem != null)
            cofre[newPos] = oldItem;
        else
            cofre[newPos] = null;
    }

    public void ClearBackpack()
    {
        m_ItemSlots2 = new ItemSlot[36];
        for (int i = 0; i < m_ItemSlots2.Length; i++)
        {
            m_ItemSlots2[i] = new ItemSlot(null, 0);
        }
    }

    public void ClearAllInfo()
    {
        ClearBackpack();
        m_Armadura = new ItemSlot();
        m_SecondaryHand = new ItemSlot();
    }

}

using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Azada", menuName = "Item/Equipable/Herramientas/Azada")]
public class Azada : Equipable, IActionable
{
    [SerializeField] private Tile m_TileTarget;
    [SerializeField] private Tile m_TileNew;
    [SerializeField] private GameEventFloat m_WasteEnergy;
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        Vector3Int target = t.WorldToCell(position + direction);

        if (t.GetTile<Tile>(target) == m_TileTarget)
        {
            m_WasteEnergy.Raise(EnergiaQueGasta);
            t.SetTile(target, m_TileNew);
        }
    }
}

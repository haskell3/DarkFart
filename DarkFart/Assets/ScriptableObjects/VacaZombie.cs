using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VacaZombie", menuName = "Animal/VacaZombie")]
public class VacaZombie : Animal
{
    [SerializeField]
    private float meatTime;
    public float MeatTime
    {
        get
        {
            return meatTime;
        }
        set
        {
            meatTime = value;
        }
    }
    [SerializeField]
    private Alimento carneZombie;
    public Alimento Carne
    {
        get
        {
            return carneZombie;
        }
        set
        {
            carneZombie = value;
        }
    }
}

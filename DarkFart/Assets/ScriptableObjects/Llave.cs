using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "LLave", menuName = "Item/Equipable/Herramientas/Llave")]
public class Llave : Equipable, IActionable
{
    [SerializeField] private LayerMask m_EstructuraMask;
    public enum TipoLlave { Carne };
    [SerializeField]
    private TipoLlave m_TipoLlave;
    public TipoLlave Tipo { get { return m_TipoLlave; } }
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        Debug.Log("Llave usada");
        Vector3 target = position + direction;

        Vector3 origin = target;
        origin.z = -10;

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward, 20, m_EstructuraMask);

        if (hit.transform == null) return;

        if (hit.transform.gameObject.TryGetComponent<PuertaController>(out PuertaController puertaController))
        {
            puertaController.AbrirPuerta(m_TipoLlave);
        }

        if (hit.transform.gameObject.TryGetComponent<ManoloController>(out ManoloController manoloController))
        {
            Debug.Log("Manolo encontrao");
            manoloController.Llaves.Add(this);
            manoloController.ActivarDialogoLlave(this);
            InventarioController.Instance.getBackpack().Remove(this);
            InventarioController.Instance.Refresh();
        }
    }
}

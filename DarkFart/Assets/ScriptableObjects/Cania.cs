using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Cania", menuName = "Item/Equipable/Herramientas/Cania")]
public class Cania : Equipable, IActionable
{
    [SerializeField] private float m_WaitingTime;
    public float WaitingTime
    {
        get { return m_WaitingTime; }
        set { m_WaitingTime = value; }
    }
    private bool exclamation = false;
    [SerializeField] private FishingSpotInfo m_FishingSpotInfo;
    [SerializeField] private GameEventFloat m_WasteEnergy;

    private List<Pez> m_Peces = null;
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {

        Vector3Int target = t.WorldToCell(position + direction);

        if (ContainsTile(t.GetTile<Tile>(target)))
        {
            if (!PescaPlayer.Instance.started)
            {
                //Debug.Log("1");
                List<Pez> peces = GetPoolTile(t.GetTile<Tile>(target));
                m_Peces = peces;
                m_WasteEnergy.Raise(EnergiaQueGasta);
                PescaPlayer.Instance.StartFishing(m_WaitingTime);
            }
            else if (!PescaPlayer.Instance.exclamacion)
            {
                //Debug.Log("2");
                PescaPlayer.Instance.StopAll();
            }
            else
            {
                //Debug.Log("3");
                PescaPlayer.Instance.StopAll();
                Fishing();
            }
        }
    }

    private void Fishing()
    {
        int r = Random.Range(0, m_Peces.Count);
        GameController.Instance.StartFishing(m_Peces[r]);
    }

    private bool ContainsTile(Tile tile)
    {
        foreach (FishingSpotInfo.FishingSpot t in m_FishingSpotInfo.fishingTilesInfo)
        {
            if (t.tile == tile) return true;
        }
        return false;
    }

    private List<Pez> GetPoolTile(Tile tile)
    {
        foreach (FishingSpotInfo.FishingSpot t in m_FishingSpotInfo.fishingTilesInfo)
        {
            if (t.tile == tile) return t.fishPool;
        }
        return null;
    }


}

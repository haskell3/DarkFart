using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "HachaPico", menuName = "Item/Equipable/Herramientas/HachaPico")]
public class HachaPico : Equipable, IActionable
{
    [SerializeField] private GameEventFloat m_GameEvent;
    [SerializeField] private GameEventFloat m_WasteEnergy;
    [SerializeField] private float da�o;
    public float Dmg
    {
        get
        {
            return da�o;
        }
        set
        {
            da�o = value;
        }
    }
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        m_GameEvent.Raise(da�o);
        m_WasteEnergy.Raise(EnergiaQueGasta);
    }
}

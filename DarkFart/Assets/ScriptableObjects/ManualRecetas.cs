using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Manual", menuName = "Cocina/Manual")]
public class ManualRecetas : Consumible, IConsume
{
    [SerializeField] private CookBook m_cookBook;
    [SerializeField] private int m_PosDesbloquejar;

    public void Consume()
    {
        m_cookBook.recipesSlot[m_PosDesbloquejar].unlocked = true;
    }
}

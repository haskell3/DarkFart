using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Estructura", menuName = "Item/Estructura/Estructura")]
public class Estructura : Item, IActionable
{
    [SerializeField] protected GameObject m_Prefab;
    [SerializeField] protected Tile[] m_AbleTiles;
    [SerializeField] protected LayerMask m_EstructureMask;

    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        if (!isAble(position, direction, t)) return;
        GameObject g = Instantiate(m_Prefab);

        g.transform.position = t.GetCellCenterWorld(t.WorldToCell(position + direction));
        g.GetComponent<SpriteRenderer>().sprite = Sprite;
        g.GetComponent<EstructuraBehaviour>().Estructura = this;
    }

    public bool isAble(Vector2 position, Vector2 direction, Tilemap t)
    {
        Vector3Int target = t.WorldToCell(position + direction);

        if (!m_AbleTiles.Contains(t.GetTile<Tile>(target))) return false;

        Vector3 origin = position + direction;
        origin.z = -10;

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward, 20, m_EstructureMask);

        return hit.transform == null;
    }
}

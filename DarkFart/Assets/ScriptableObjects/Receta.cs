using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Receta", menuName = "Cocina/Receta")]
public class Receta : ScriptableObject
{
    [Serializable]
    public class Ingredients
    {
        [SerializeField]
        public Item ingredient;
        [SerializeField]
        public int quantity;
    }

    [SerializeField]
    private string description;
    public string Description
    {
        get
        {
            return description;
        }
        set
        {
            description = value;
        }
    }
    [SerializeField]
    private List<Ingredients> ingredients;
    public List<Ingredients> IngredientsCraft
    {
        get
        {
            return ingredients;
        }
        set
        {
            ingredients = value;
        }
    }

    [SerializeField]
    private int water;
    public int Water
    {
        get
        {
            return water;
        }
        set
        {
            water = value;
        }
    }

    [SerializeField]
    private Alimento result;
    public Alimento Result
    {
        get
        {
            return result;
        }
        set
        {
            result = value;
        }
    }
}

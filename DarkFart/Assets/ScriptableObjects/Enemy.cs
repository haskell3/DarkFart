using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
[Serializable]
public class Enemy : ScriptableObject
{
    [SerializeField]
    private string name;
    public string Name { get { return name; } set { name = value; } }
    [SerializeField] private Sprite[] sprites;
    public Sprite[] Sprites { get {  return sprites; } set {  sprites = value; } }
    [SerializeField]
    private float hp;
    public float Hp { get { return hp; } set {  hp = value; } }
    [SerializeField]
    private float speed;
    public float Speed { get { return speed; } set { speed = value; } }
    [SerializeField]
    private float detectionRange;
    public float DetectionRange { get { return detectionRange; } set { detectionRange = value; } }
    [SerializeField]
    private float attackRange;
    public float AttackRange { get { return attackRange; } set { attackRange = value; } }
    [SerializeField]
    private float damage;
    public float Damage { get { return damage; } set { damage = value; } }
    [SerializeField]
    private Vector2 attackArea;
    public Vector2 AttackArea { get { return attackArea; } set { attackArea = value; } }
    [SerializeField]
    private float attackCooldown;
    public float AttackCooldown { get { return attackCooldown; } set { attackCooldown = value; } }
    [SerializeField]
    private int criticProbability;
    public int CriticProbability { get { return criticProbability; } set { criticProbability = value; } }
    [SerializeField]
    private List<Item> drops;
    public List<Item> Drops { get {  return drops; } set {  drops = value; } }
}

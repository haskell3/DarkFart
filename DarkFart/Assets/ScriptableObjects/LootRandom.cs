using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using static Backpack;

[CreateAssetMenu(fileName = "LootRandom", menuName = "LootRandom")]
public class LootRandom : ScriptableObject
{
    [SerializeField]
    private List<ItemSlot> loot = new List<ItemSlot>();
    public List<ItemSlot> Loot
    {
        get
        {
            return loot;
        }
        set
        {
            loot = value;
        }
    }
    [SerializeField]
    private int quantityLootSpawned;
    public int QuantityLootSpawned
    {
        get
        {
            return quantityLootSpawned;
        }
        set
        {
            quantityLootSpawned = value;
        }
    }
    [SerializeField]
    private int minquantityLootSpawned;
    public int minQuantityLootSpawned
    {
        get
        {
            return minquantityLootSpawned;
        }
        set
        {
            minquantityLootSpawned = value;
        }
    }
}

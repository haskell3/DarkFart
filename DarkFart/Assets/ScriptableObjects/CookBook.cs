using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

[CreateAssetMenu(fileName = "Receta", menuName = "Cocina/CookBook")]
public class CookBook : ScriptableObject
{
    [Serializable]
    public class CookRecipe
    {
        [SerializeField]
        public bool unlocked;
        [SerializeField]
        public Receta recipe;
    }

    [SerializeField]
    private CookRecipe[] recipes;

    public ReadOnlyCollection<CookRecipe> recipesSlot => new ReadOnlyCollection<CookRecipe>(recipes);

    public CookRecipe GetCookRecipeSlot(int pos)
    {
        return recipes[pos];
    }
}

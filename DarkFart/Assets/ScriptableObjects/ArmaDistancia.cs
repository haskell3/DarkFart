using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "ArmaDistancia", menuName = "Item/Equipable/Arma/ArmaDistancia")]
public class ArmaDistancia : Arma, IActionable
{
    [SerializeField]
    private float velocidadPro;
    public float VelocidadPro
    {
        get
        {
            return velocidadPro;
        }
        set
        {
            velocidadPro = value;
        }
    }
    [SerializeField]
    private float distanciaPro;
    public float DistanciaPro
    {
        get
        {
            return distanciaPro;
        }
        set
        {
            distanciaPro = value;
        }
    }
    [SerializeField]
    private bool isAbleFollow;
    public bool IsAbleFollow
    {
        get
        {
            return isAbleFollow;
        }
        set
        {
            isAbleFollow = value;
        }
    }
    [SerializeField]
    private float gradoSeguimiento;
    public float GradoSeguimiento
    {
        get
        {
            return gradoSeguimiento;
        }
        set
        {
            gradoSeguimiento = value;
        }
    }
    private float[] send = new float[6];
    [SerializeField] private GameEventFloatArmaDistancia m_GameEventD;
    public new void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        if (!isAble) return;

        isAble = false;

        send[1] = cooldown;
        send[2] = areaAttack;
        send[3] = velocidadPro;
        send[4] = distanciaPro;
        send[5] = gradoSeguimiento;

        if (Random.Range(0, 101) < probCritico)
            send[0] = dmg * 2;
        else
            send[0] = dmg;
        m_GameEventD.Raise(send, IsAbleFollow, this);
        m_WasteEnergy.Raise(EnergiaQueGasta);
    }

}

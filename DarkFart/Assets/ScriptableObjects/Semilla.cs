using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Semilla", menuName = "Item/Materiales/Semilla")]
public class Semilla : Materiales, IActionable
{
    [SerializeField] private GameObject m_Prefab;
    [SerializeField] private Tile[] m_Tiles;
    [SerializeField]
    private LayerMask m_LayerMask;

    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        if (m_Tiles.Contains(t.GetTile<Tile>(t.WorldToCell(position + direction))))
        {
            Vector3 vector3 = position + direction;
            vector3.z = -10;
            RaycastHit2D hit = Physics2D.Raycast(vector3, Vector3.forward, 30, m_LayerMask);
            if (hit.rigidbody == null)
            {
                GameObject g = Instantiate(m_Prefab);
                g.transform.position = t.GetCellCenterWorld(t.WorldToCell(position + direction));
            }
        }
    }
}

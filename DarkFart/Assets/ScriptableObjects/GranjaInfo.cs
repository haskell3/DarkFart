using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;
using UnityEngine.Tilemaps;
using static UnityEngine.RuleTile.TilingRuleOutput;

[CreateAssetMenu]
[Serializable]
public class GranjaInfo : ScriptableObject
{
    [SerializeField] public List<ItemInfo> items = new List<ItemInfo>();
    [SerializeField] public List<DropeableInfo> resources = new List<DropeableInfo>();
    [SerializeField] public List<AnimalInfo> animals = new List<AnimalInfo>();
    [SerializeField] public List<EnemyInfo> enemies = new List<EnemyInfo>();
    [SerializeField] public List<PlantInfo> plants = new List<PlantInfo>();
    [SerializeField] public List<EstructuraInfo> estructures = new List<EstructuraInfo>();
    [SerializeField] public List<BarreraInfo> barriers = new List<BarreraInfo>();
    [SerializeField] public List<CalderoInfo> caldiers = new List<CalderoInfo>();
    [SerializeField] public List<CofreInfo> chests = new List<CofreInfo>();
    [SerializeField] public List<NPCInfo> NPCS = new List<NPCInfo>();
    [SerializeField] public DictionarySer tiles = new DictionarySer();
    [SerializeField] private Vector3 Init;
    public Vector3 init { get { return Init; } }
    [SerializeField] private Vector3 Finish;
    public Vector3 finish { get { return Finish; } }
    public void Save()
    {
        SaveTiles();
        SaveGameObjects();

        SaveData.Instance.Save();
    }

    private void SaveTiles()
    {
        tiles.Clear();
        Vector3 target = Vector3.zero;
        Tilemap t = TilemapController.PlantasTilemap;
        for(float i = Init.x; i < Finish.x; ++i)
        {
            for(float j = Init.y; j < Finish.y; ++j)
            {
                target.x = i;
                target.y = j;
                tiles.Add(target, t.GetTile<Tile>(t.WorldToCell(target)));
            }
        }
    }

    private void SaveGameObjects()
    {
        IdInfo[] infos = FindObjectsByType<IdInfo>(FindObjectsSortMode.None);
        ClearInfo();
        foreach (IdInfo info in infos)
        {
            info.SaveInfo();
        }
    }

    public void ClearInfo()
    {
        items.Clear();
        resources.Clear();
        enemies.Clear();
        animals.Clear();
        estructures.Clear();
        plants.Clear();
        barriers.Clear();
        caldiers.Clear();
        chests.Clear();
        NPCS.Clear();
    }

    public void ClearAllInfo()
    {
        ClearInfo();
        tiles.Clear();
    }
}
[Serializable]
public class DictionarySer
{
    [SerializeField] private List<Tile> tiles;
    [SerializeField] private List<Vector3> target;

    public void Add(Vector3 v, Tile t)
    {
        tiles.Add(t);
        target.Add(v);
    }

    public Tile GetTile(int i)
    {
        if(tiles.Count <= i)
            Debug.LogError("Index Out Of Bounds");
        return tiles[i];
    }
    public Vector3 GetPos(int i)
    {
        if (i >= target.Count)
            Debug.LogError("Index Out Of Bounds");
        return target[i];
    }

    public int Count()
    {
        if (tiles.Count != target.Count)
            Debug.LogError("Ha fallado el diccionario");

        return tiles.Count;
    }

    public void Clear()
    {
        tiles.Clear();
        target.Clear();
    }
}


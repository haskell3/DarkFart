using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class BiomaInfo : ScriptableObject
{
    public enum TypeAnimal { NONE, NARIZ_COSA, VACA_ZOMBIE }
    
    [Serializable]
    public struct ItemSpawn
    {
        public Item item;
        public float spawnProbability;
    }

    [Serializable]
    public struct NPCSpawn
    {
        public PersonalidadInfo personalidad;
        public float spawnProbability;
        public TascaInfo[] tasques;
    }

    [Serializable]
    public struct AnimalSpawn
    {
        public TypeAnimal typeAnimal;
        public float spawnProbability;
    }

    [Serializable]
    public struct ResourceSpawn
    {
        public GameObject resource;
        public float spawnProbability;
    }

    [Serializable]
    public struct WaterSpawn
    {
        public Tile waterTile;
        public float waterProbability;
    }
    [Serializable]
    public struct EnemySpawn
    {
        public Enemy enemy;
        public float enemyProbability;
        public int enemyAmount;
    }
    [Serializable]
    public struct BiomaTileInfo
    {
        public string name;
        public Tile tile;
        public float probability;

        public float itemProbability; 
        public ItemSpawn[] items;
        
        
        public float NPCProbability;
        public NPCSpawn[] npcs;

        public float animalProbability;
        public AnimalSpawn[] animals;

        public float resourceProbability;
        public ResourceSpawn[] resources;

    }
    public BiomaTileInfo[] tiles;
    public WaterSpawn water;
    public float enemyProbability;
    public EnemySpawn[] enemies;
    public string[] NpcNames;
}
VAR tpToGranja = false
VAR friendship = 0

-> main

=== main ===
¡Buenas granjero! Veo que ya has despertado de tu largo sueño.
Pensé que habrias muerto al igual que el 90% de los seres humanos.
    *[¿Quién eres?]
        Quien sea yo no es importante.
        -> continue1
    *[¿Qué ha pasado?]
        Pues verás... es una historia graciosa.
        -> continue1

=== continue1 ===
Te has pasado los últimos 10 años "durmiendo" dentro de esa cabaña.
Aunque no pareces nada cambiado...
Debe de ser algún efecto secundario causado por el Dark Fart.
¿Recuerdas el Dark Fart... verdad?
    *[Sí]
        Por supuesto ¿cómo olvidarlo?
        -> continue2
    *[No]
        El Dark Fart fue el fatídico acontecimiento que cambió nuestro mundo para siempre.
        Hace ya 10 años, el mundo se sumió en una oscura neblina.
        Nadie sabe realmente de donde salió o quién provocó tan cruel suceso.
        Lo único que se sabe es que solo algunas personas sobrevivieron a tal masacre y a todo lo que conllevó.
        Los animales en cambio están irreconocibles, yo de ti no me acercaría a ninguno de ellos.
        Algunos pueden ser muy agresivos.
        ->continue2
    
=== continue2 ===
Aunque es curioso... tú... pareces distinto.
Sí, en definitiva eres distinto al resto, noto un aura de inmortalidad en tí.
Además, no has envejecido nada, eso seguro que significa algo.
    *[¿Inmortalidad?]
        Eso mismo he dicho, seguramente te des cuenta por tí mismo.
        La gente temeraria como tú siempre acaban yendo de frente contra el enemigo.
        Así que seguramente acabes de descubrirlo por tí solo.
        -> continue3
    *[Siempre supe que era especial]
        Jajaja, es una buena manera de verlo.
        ->continue3

=== continue3 ===
¡Ah por cierto! Una cosa más.
Si llegas a encontrar alguna especie de llave traemela.
Me hará recordar más cosas sobre el Dark Fart.
Ha pasado tanto tiempo que hasta yo me he olvidado de cosas.
    *[Vale, gracias]
        ->continue4
    *[Ya me jodería]
        Bueno, los fantasmas somos eternos y nos olvidamos de las cosas.
        A duras penas me acuerdo de mi nombre.
        ->continue4


=== continue4 ===
¡ESPERA! ¡Una última cosa!
    *[¿Qué pasa?]
        ->final
    *[Eres más pesado que una vaca en brazos]
        Perdona, pero es muy importante.
        ->final
        
=== final ===
Si te faltan semillas, yo iré reponiendo el cofre que tengo a mi lado cada día.
Pero ten cuidado porque a mí me entra hambre por las noches y como me dejes algo ahí, me lo podría comer.
En fin... ¡Buena suerte, granjero!
Y no olvides consultarme de vez en cuando para que te dé unos consejos.
->END

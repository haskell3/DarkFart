VAR tpToGranja = false
VAR friendship = 0

-> main

=== main ===
¿Qué te pasa con la forma en la que levantas esa sword? 
Si no haces bien las repeticiones, solo te vas a lesionar y no vas a ver los resultados de tu grindeo.
    *[Gracias por el consejo]
        ~ friendship = 10
        No hay de qué camarada.
        Siempre es un placer ayudar a otros en su grindeo.
        -> END
    *[Me da igual]
        ~ friendship = -10
        Así nunca vas a improvear tu fuerza y músculos.
        -> END
    *[La próxima vez lo haré mejor]
        Nunca dejes para mañana lo que puedas hacer hoy.
        -> END
    
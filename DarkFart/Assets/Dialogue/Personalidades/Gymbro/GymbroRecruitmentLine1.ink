VAR tpToGranja = false
VAR friendship = 0

-> main

=== main ===
¡Oye tú! ¿Necesitas a alguien fuerte para que te ayude con tu granja?
No creo que tengas musculos como los mios.
    *[Ni un cerebro como el tuyo]
        Jaja muy gracioso, camarada.
        -> afirmative
    *[La verdad es que sí]
        -> afirmative
    *[No, gracias]
        Pues tú te lo pierdes.
        -> END

=== afirmative ===
¡Pues estás de suerte!
A cambio del modico precio de un lugar de un refugio y comida puedes precisar de mis musculos para el trabajo que no quieras hacer.
¿Qué te parece eh?
    *[Vale]
        ~ tpToGranja = true
        ¡Genial! Pues te espero ahí.
        -> END
    *[No lo necesito]
        Pues tú te lo pierdes.
        -> END
    
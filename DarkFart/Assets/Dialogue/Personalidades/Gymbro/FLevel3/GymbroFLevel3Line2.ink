VAR tpToGranja = false
VAR friendship = 0

-> main

=== main ===
Últimamente he estado pensando que deberíamos entrenar algún día de estos.
¿Qué te apetecería entrenar primero?
    *[Pierna]
        ~ friendship = 5
        ¡Tú sí le sabes!
        ¡Día de pierna, el mejor día!
        -> END
    *[Cardio]
        ~ friendship = 10
        ¡Cardio, genial! 
        Empezamos con una buena sesión de cardio para calentar. 
        Después, podemos meterle un poco de fuerza, ¿te parece bien?
        -> END
    *[No me apetece entrenar nada]
        ~ friendship = -20
        Oh... bueno...
        -> END
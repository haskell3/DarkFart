VAR tpToGranja = false
VAR friendship = 0

-> main

=== main ===
Ser un lobo solitario es un don que pocos comprenden.
    *[Yo lo comprendo]
        No creo que tú comprendas el dolor que reside en mi alma.
        -> END
    *[¡Auuu!]
        ~ friendship = -10
        Mi cultura no es tu disfraz.
        -> END
    *[¿Estás bien?]
        ~ friendship = 10
        ... No.
        -> END

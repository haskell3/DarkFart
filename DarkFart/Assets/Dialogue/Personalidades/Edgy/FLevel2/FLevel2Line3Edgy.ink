VAR tpToGranja = false
VAR friendship = 0

-> main

=== main ===
He estado observando tu habilidad en combate.
Puedo ver tus fortalezas, pero todavía te queda mucho para estar a mi nivel.
    *[Lo dudo, eres fuerte]
     ~ friendship = -5
        No desconfies tanto en tí mismo.
        Eso tan solo muestra tu debilidad.
        -> END
    *[Lo dudo, eres débil]
        ~ friendship = -10
        El verdadero poder reside en la oscuridad que cada uno lleva dentro.
        Y mi alma está hecha de sombras que nadie puede iluminar.
        -> END
    *[Enseñame maestro]
        ~ friendship = 10
        Ja... me alegro que digas eso.
        Algún dia te enseñaré el camino del lobo solitario si así lo deseas.
        -> END
        
    
VAR tpToGranja = false
VAR friendship = 0

-> main

=== main ===
No hay nada mas frio que mi corazón.
Sin embargo últimamente las noches a la intemperie son realmente duras.
    *[La verdad es que sí]
        -> insistencia
    *[¿Te quieres venir a mi granja?]
        ~ tpToGranja = true
        ¿Qué? ¿Insinuas que necesito ayuda de alguien?
        Sin embargo, ya que insistes tanto iré.
        -> END
        
=== insistencia ===
Verdaderamente hace mucho frío.
Si no fuese porque estoy acostumbrado a la fría y solitaria noche no lo contaría.
    * [... ¿Necesitas un refugio?]
        ~ tpToGranja = true
        ¿Necesitar? Yo solo me necesito a mi mismo.
        Pero si tanto insistes te asistiré con mis habilidades.
        -> END
    * [Pues muy bien]
        ... suspiro
        -> END
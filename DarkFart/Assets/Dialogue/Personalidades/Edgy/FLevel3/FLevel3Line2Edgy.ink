VAR tpToGranja = false
VAR friendship = 0

-> main

=== main ===
Últimamente he estado pensando mucho en la muerte.
Aunque yo no temo a la muerte, ella teme conocerme.
¿Tú le temes a la muerte granjero?
    *[Sí]
        ~ friendship = -10
        No deberías temerle si tu deber es protegernos.
        -> END
    *[No]
        Dudo que estés siendo sincero.
        No hay ningún ser humano normal que no le tema a la muerte.
        Sin embargo, yo no soy un humano normal.
        -> END
    *[A veces]
        ~ friendship = 10
        Eso es lo más correcto.
        La muerte es un destino del que nadie puede escapar.
        Es natural tenerle miedo.
        -> END